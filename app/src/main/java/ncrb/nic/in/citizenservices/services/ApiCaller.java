package ncrb.nic.in.citizenservices.services;


import java.util.HashMap;
import java.util.List;


import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services_params.ResponseGetNearByPoliceStation;
import ncrb.nic.in.citizenservices.services_params.SubmitFormModel;
import ncrb.nic.in.citizenservices.services_params.WSPCountryConnect;
import ncrb.nic.in.citizenservices.services_params.WSPDistrictOfficeConnect;
import ncrb.nic.in.citizenservices.services_params.WSPGetFIR;
import ncrb.nic.in.citizenservices.services_params.WSPLoginConnect;
import ncrb.nic.in.citizenservices.services_params.WSPNatureOfComplaintConnect;
import ncrb.nic.in.citizenservices.services_params.WSPPoliceContact;
import ncrb.nic.in.citizenservices.services_params.WSPReqSedition;
import ncrb.nic.in.citizenservices.services_params.WSPSaveCitizenTip;
import ncrb.nic.in.citizenservices.services_params.WSPSearchRecordConnect;
import ncrb.nic.in.citizenservices.services_params.WSPStateConnect;
import ncrb.nic.in.citizenservices.services_params.WSPStateOfficeConnect;
import ncrb.nic.in.citizenservices.services_params.WSPSubmitComplaintConnect;
import ncrb.nic.in.citizenservices.services_params.WSPvpn;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;
import ncrb.nic.in.citizenservices.services_params.SyncableDataGSON;
import retrofit.http.Body;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.Query;


/**
 * Created by sez1 on 18/11/15.
 */
public interface ApiCaller {


    // -------------
    // MICROSOFT STACK -  START
    // -------------

//    @Headers("Content-Type: application/json")
//    @POST("/mDistrictConnect")
//    public void mDistrictConnect(@Body JSONPostParams jsonPostParams,
//                                 retrofit.Callback<wspDistrictConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mDistrictConnectWithoutPS")
//    public void mDistrictConnectWithoutPS(@Body JSONPostParams jsonPostParams,
//                                 retrofit.Callback<wspDistrictConnect> callback);
//
//
//
//    @Headers("Content-Type: application/json")
//    @POST("/mPoliceStationContact")
//    public void mPoliceStationContact(@Body JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPPoliceContact> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @POST("/mStateOfficeConnect")
//    public void mStateOfficeConnect(@Body JSONPostParams jsonPostParams,
//                                    retrofit.Callback<WSPStateOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @POST("/mDistrictOfficeConnect")
//    public void mDistrictOfficeConnect(@Body JSONPostParams jsonPostParams,
//                                       retrofit.Callback<WSPDistrictOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @POST("/mGetCitizenLoginDetailsConnect")
//    public void mGetCitizenLoginDetailsConnect(@Body JSONPostParams jsonPostParams,
//                                               retrofit.Callback<WSPLoginConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mGetFIRStatus")
//    public void mGetFIRStatus(@Body JSONPostParams jsonPostParams,
//                                               retrofit.Callback<WSPGetFIR> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mReqSedition")
//    public void mReqSedition(@Body JSONPostParams jsonPostParams,
//                                               retrofit.Callback<WSPReqSedition> callback);
//
//    /*Login for java application*/
//    @FormUrlEncoded
//    @POST("/mGetCitizenLoginDetailsConnect")
//    public void mGetCitizenLoginDetailsConnectJava(@FieldMap HashMap<String, String> arr,
//                                      retrofit.Callback<WSPLoginConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mRegisterComplaintConnect")
//    public void mRegisterComplaintConnect(@Body JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPSubmitComplaintConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @POST("/mGetServiceStatus")
//    public void mGetServiceStatus(@Body JSONPostParams jsonPostParams,
//                                  retrofit.Callback<WSPSearchRecordConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mNatureOfComplaintConnect")
//    public void mNatureOfComplaintConnect(@Body JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPNatureOfComplaintConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mCountryConnect")
//    public void mCountryConnect(@Body JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPCountryConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @POST("/mStateConnect")
//    public void mStateConnect(@Body JSONPostParams jsonPostParams,
//                                    retrofit.Callback<WSPStateConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @POST("/mSaveCitizenTip")
//    public void mSaveCitizenTip(@Body JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPSaveCitizenTip> callback);



    // -------------
    // MICROSOFT STACK -  END
    // -------------


    // -------------
    // JAVA STACK -  START
    // -------------


    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mDistrictConnect(@Body JSONPostParams jsonPostParams,
                                 retrofit.Callback<wspDistrictConnect> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mDistrictConnectWithoutPS(@Body JSONPostParams jsonPostParams,
                                          retrofit.Callback<wspDistrictConnect> callback);



    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mPoliceStationContact(@Body JSONPostParams jsonPostParams,
                                      retrofit.Callback<WSPPoliceContact> callback);


    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mStateOfficeConnect(@Body JSONPostParams jsonPostParams,
                                    retrofit.Callback<WSPStateOfficeConnect> callback);


    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mDistrictOfficeConnect(@Body JSONPostParams jsonPostParams,
                                       retrofit.Callback<WSPDistrictOfficeConnect> callback);


    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mGetCitizenLoginDetailsConnect(@Body JSONPostParams jsonPostParams,
                                               retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mGetFIRStatus(@Body JSONPostParams jsonPostParams,
                              retrofit.Callback<WSPGetFIR> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mReqSedition(@Body JSONPostParams jsonPostParams,
                             retrofit.Callback<WSPReqSedition> callback);

    /*Login for java application*/
    @FormUrlEncoded
    @POST("/mWebService")
    public void mGetCitizenLoginDetailsConnectJava(@FieldMap HashMap<String, String> arr,
                                                   retrofit.Callback<WSPLoginConnect> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mRegisterComplaintConnect(@Body JSONPostParams jsonPostParams,
                                          retrofit.Callback<WSPSubmitComplaintConnect> callback);


    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mGetServiceStatus(@Body JSONPostParams jsonPostParams,
                                  retrofit.Callback<WSPSearchRecordConnect> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mNatureOfComplaintConnect(@Body JSONPostParams jsonPostParams,
                                          retrofit.Callback<WSPNatureOfComplaintConnect> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mCountryConnect(@Body JSONPostParams jsonPostParams,
                                retrofit.Callback<WSPCountryConnect> callback);


    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mStateConnect(@Body JSONPostParams jsonPostParams,
                              retrofit.Callback<WSPStateConnect> callback);

    @Headers("Content-Type: application/json")
    @POST("/mWebService")
    public void mSaveCitizenTip(@Body JSONPostParams jsonPostParams,
                                retrofit.Callback<WSPSaveCitizenTip> callback);



    // -------------
    // JAVA STACK -  END
    // -------------



    // -------------
    // start - todo remove this after demo
    // https://www.jasonbase.com/things/gXdp.json
    // -------------

//    @FormUrlEncoded
//    @GET("/rest/create_complaint")
//    public void complaintRegistration(@FieldMap HashMap<String, String> arr,
//                                      retrofit.Callback<SubmitFormModel> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/gXdp.json")
//    public void mDistrictConnect(@Query("q") JSONPostParams jsonPostParams,
//                                 retrofit.Callback<wspDistrictConnect> callback);
//
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/LVxb.json")
//    public void mDistrictConnectWithoutPS(@Query("q") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<wspDistrictConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/bQ87.json")
//    public void mPoliceStationContact(@Query("q") JSONPostParams abc,
//                                      retrofit.Callback<WSPPoliceContact> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/dRbG.json")
//    public void mStateOfficeConnect(@Query("q") JSONPostParams jsonPostParams,
//                                    retrofit.Callback<WSPStateOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/B83m.json")
//    public void mDistrictOfficeConnect(@Query("q") JSONPostParams jsonPostParams,
//                                       retrofit.Callback<WSPDistrictOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    //@GET("/0dPa.json")
//    @GET("/JodA.json")
//    public void mGetCitizenLoginDetailsConnect(@Query("q") JSONPostParams jsonPostParams,
//                                               retrofit.Callback<WSPLoginConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/yKjx.json")
//    public void mRegisterComplaintConnect(@Query("q") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPSubmitComplaintConnect> callback);
//
//    @FormUrlEncoded
//    @GET("/rest/nearby_location")
//    public void getNearByPoliceStation(@FieldMap HashMap<String, String> arr,
//                                       retrofit.Callback<ResponseGetNearByPoliceStation> callback);
//
//
//    @FormUrlEncoded
//    @GET("/rest/sync_sql")
//    public void getSyncableDatabase(@FieldMap HashMap<String, String> arr,
//                                    retrofit.Callback<SyncableDataGSON> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/RPjX.json")
//    public void mGetServiceStatus(@Query("q") JSONPostParams jsonPostParams,
//                                  retrofit.Callback<WSPSearchRecordConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/8WBb.json")
//    public void mNatureOfComplaintConnect(@Query("q") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPNatureOfComplaintConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/Y7zb.json")
//    public void mCountryConnect(@Query("q") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPCountryConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/AqeD.json")
//    public void mStateConnect(@Query("q") JSONPostParams jsonPostParams,
//                              retrofit.Callback<WSPStateConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/emyk.json")
//    public void mSaveCitizenTip(@Query("q") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPSaveCitizenTip> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/lgRa.json")
//    public void mGetFIRStatus(@Query("q") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPGetFIR> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/kL02.json")
//    public void mReqSedition(@Query("q") JSONPostParams jsonPostParams,
//                              retrofit.Callback<WSPReqSedition> callback);
//
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/red")
//    public void mVPNChecking(@Query("q") JSONPostParams jsonPostParams,
//                             retrofit.Callback<List<WSPvpn>> callback);




    // -------------
    // end todo remove this after demo\
    // https://www.jasonbase.com/things/gXdp.json
    // -------------



    // -------------
    // start - todo remove this after demo
    // -------------

//    @FormUrlEncoded
//    @GET("/rest/create_complaint")
//    public void complaintRegistration(@FieldMap HashMap<String, String> arr,
//                                      retrofit.Callback<SubmitFormModel> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/kdych")
//    public void mDistrictConnect(@Query("q") JSONPostParams jsonPostParams,
//                                 retrofit.Callback<wspDistrictConnect> callback);
//
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/13ftpd")
//    public void mDistrictConnectWithoutPS(@Query("q") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<wspDistrictConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/12b3nl")
//    public void mPoliceStationContact(@Query("q") JSONPostParams abc,
//                                      retrofit.Callback<WSPPoliceContact> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/1gli69")
//    public void mStateOfficeConnect(@Query("q") JSONPostParams jsonPostParams,
//                                    retrofit.Callback<WSPStateOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/1gjd0h")
//    public void mDistrictOfficeConnect(@Query("q") JSONPostParams jsonPostParams,
//                                       retrofit.Callback<WSPDistrictOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/1h4sm9")
//    public void mGetCitizenLoginDetailsConnect(@Query("q") JSONPostParams jsonPostParams,
//                                               retrofit.Callback<WSPLoginConnect> callback);
//
//    /*Login for java application*/
//    @FormUrlEncoded
//    @GET("/1h4sm9")
//    public void mGetCitizenLoginDetailsConnectJava(@FieldMap HashMap<String, String> arr,
//                                                   retrofit.Callback<WSPLoginConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/1femyp")
//    public void mRegisterComplaintConnect(@Query("q") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPSubmitComplaintConnect> callback);
//
//    @FormUrlEncoded
//    @GET("/rest/nearby_location")
//    public void getNearByPoliceStation(@FieldMap HashMap<String, String> arr,
//                                       retrofit.Callback<ResponseGetNearByPoliceStation> callback);
//
//
//    @FormUrlEncoded
//    @GET("/rest/sync_sql")
//    public void getSyncableDatabase(@FieldMap HashMap<String, String> arr,
//                                    retrofit.Callback<SyncableDataGSON> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/zxd8h")
//    public void mGetServiceStatus(@Query("q") JSONPostParams jsonPostParams,
//                                  retrofit.Callback<WSPSearchRecordConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/ukgsh")
//    public void mNatureOfComplaintConnect(@Query("q") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPNatureOfComplaintConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/92pup")
//    public void mCountryConnect(@Query("q") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPCountryConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/abq81")
//    public void mStateConnect(@Query("q") JSONPostParams jsonPostParams,
//                              retrofit.Callback<WSPStateConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/93qyt")
//    public void mSaveCitizenTip(@Query("q") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPSaveCitizenTip> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/93qyt")
//    public void mVPNChecking(@Query("q") JSONPostParams jsonPostParams,
//                             retrofit.Callback<WSPSaveCitizenTip> callback);


    // -------------
    // end todo remove this after demo
    // -------------


    // start localhost

//    @FormUrlEncoded
//    @GET("/rest/create_complaint")
//    public void complaintRegistration(@FieldMap HashMap<String, String> arr,
//                                      retrofit.Callback<SubmitFormModel> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mDistrictConnect")
//    public void mDistrictConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                 retrofit.Callback<wspDistrictConnect> callback);
//
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mDistrictConnectWithoutPS")
//    public void mDistrictConnectWithoutPS(@Query("qq") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<wspDistrictConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mPoliceStationContact")
//    public void mPoliceStationContact(@Query("qq") JSONPostParams abc,
//                                      retrofit.Callback<WSPPoliceContact> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mStateOfficeConnect")
//    public void mStateOfficeConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                    retrofit.Callback<WSPStateOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mDistrictOfficeConnect")
//    public void mDistrictOfficeConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                       retrofit.Callback<WSPDistrictOfficeConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mGetCitizenLoginDetailsConnect")
//    public void mGetCitizenLoginDetailsConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                               retrofit.Callback<WSPLoginConnect> callback);
//
//    /*Login for java application*/
//    @FormUrlEncoded
//    @GET("/ncrbweb.php?qparams=mGetCitizenLoginDetailsConnect")
//    public void mGetCitizenLoginDetailsConnectJava(@FieldMap HashMap<String, String> arr,
//                                                   retrofit.Callback<WSPLoginConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mRegisterComplaintConnect")
//    public void mRegisterComplaintConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPSubmitComplaintConnect> callback);
//
//    @FormUrlEncoded
//    @GET("/rest/nearby_location")
//    public void getNearByPoliceStation(@FieldMap HashMap<String, String> arr,
//                                       retrofit.Callback<ResponseGetNearByPoliceStation> callback);
//
//
//    @FormUrlEncoded
//    @GET("/rest/sync_sql")
//    public void getSyncableDatabase(@FieldMap HashMap<String, String> arr,
//                                    retrofit.Callback<SyncableDataGSON> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mGetServiceStatus")
//    public void mGetServiceStatus(@Query("qq") JSONPostParams jsonPostParams,
//                                  retrofit.Callback<WSPSearchRecordConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mNatureOfComplaintConnect")
//    public void mNatureOfComplaintConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                          retrofit.Callback<WSPNatureOfComplaintConnect> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mCountryConnect")
//    public void mCountryConnect(@Query("qq") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPCountryConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mStateConnect")
//    public void mStateConnect(@Query("qq") JSONPostParams jsonPostParams,
//                              retrofit.Callback<WSPStateConnect> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mSaveCitizenTip")
//    public void mSaveCitizenTip(@Query("qq") JSONPostParams jsonPostParams,
//                                retrofit.Callback<WSPSaveCitizenTip> callback);
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mGetFIRStatus")
//    public void mGetFIRStatus(@Query("q") JSONPostParams jsonPostParams,
//                              retrofit.Callback<WSPGetFIR> callback);
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/ncrbweb.php?qparams=mReqSedition")
//    public void mReqSedition(@Query("q") JSONPostParams jsonPostParams,
//                             retrofit.Callback<WSPReqSedition> callback);
//
//
//
//    @Headers("Content-Type: application/json")
//    @GET("/red")
//    public void mVPNChecking(@Query("q") JSONPostParams jsonPostParams,
//                             retrofit.Callback<List<WSPvpn>> callback);
//

    // end localhost

}// end api caller

/*
*
*
* demo URL to access JSON values
*
mCountryConnect = https://api.myjson.com/bins/92pup
https://www.jasonbase.com/things/Y7zb.json

mDistrictConnect = https://api.myjson.com/bins/kdych
https://www.jasonbase.com/things/gXdp.json

mDistrictConnectWithoutPS = https://api.myjson.com/bins/13ftpd
https://www.jasonbase.com/things/LVxb.json

mDistrictOfficeConnect = https://api.myjson.com/bins/1gjd0h
https://www.jasonbase.com/things/B83m.json

mGetCitizenLoginDetailsConnect = https://api.myjson.com/bins/1h4sm9
https://www.jasonbase.com/things/0dPa.json

mGetServiceStatus =  https://api.myjson.com/bins/zxd8h
https://www.jasonbase.com/things/RPjX.json

mNatureOfComplaintConnect = https://api.myjson.com/bins/ukgsh
https://www.jasonbase.com/things/8WBb.json

mPSConnect = https://api.myjson.com/bins/1dmc5d
https://www.jasonbase.com/things/JBml.json

mPoliceStationConnect = error

mPoliceStationContact = https://api.myjson.com/bins/12b3nl
https://www.jasonbase.com/things/bQ87.json

mRegisterComplaintConnect = https://api.myjson.com/bins/1femyp
https://www.jasonbase.com/things/yKjx.json

mStateConnect =  https://api.myjson.com/bins/abq81
https://www.jasonbase.com/things/AqeD.json

mStateOfficeConnect = https://api.myjson.com/bins/1gli69
https://www.jasonbase.com/things/dRbG.json

mSaveCitizenTip = https://api.myjson.com/bins/93qyt
https://www.jasonbase.com/things/emyk.json

mReqSedition
https://www.jasonbase.com/things/kL02.json

mGetFIRStatus
https://www.jasonbase.com/things/lgRa.json


*/