/**
 * 
 */
package ncrb.nic.in.citizenservices.json.objects;

import android.support.annotation.Keep;

import java.util.Map;

/**
 * Class to represent a category in app.
 * @author
 */
public class JSONPostParams {

	String state_cd;
	String district_cd;
	String username;
	String password;

	String PersonCode;
	String PersonTypeCD;
	String UidNum;
	String FirstName;
	String MiddleName;
	String LastName;
	String RelativeType;
	String RelativeName;
	String Dob;
	String Mobile1;
	String Mobile2;
	String Telephone;
	String Email;
	String RecordStatus;
	String RecordCreatedOn;
	String RecordCreatedBy;
	String OriginalRecord;
	String ComplRegNum;
	String LangCd;
	String ComplSrno;
	String ComplYear;
	String ComplRegDt;
	String RequisitionType;
	String ComplaintNatureCd;
	String ReceiptModeCd;
	String ComplDesc;
	String ComplaintRemarks;
	String IncidentPlc;
	String IncidentType;
	String IncidentFromDt;
	String IncidentToDt;
	String IsPsKnown;
	String IsDistrictKnown;
	String IsOfficeKnown;
	String SubmitDistrictCd;
	String SubmitPsCd;
	String SubmitOfficeCd;
	String InformationTypeCD;
	String StateCD;
	String strComplainantPresentAddressXml;
	String strComplainantPermanentAddressXml;
	String strComplaintFilesXml;
	String strPersonNationalityCitizenXml;
	String NationalityCd;
	String YOB;
	String Age;
	String AgeMonths;
	String AgeFromYrs;
	String AgeToYrs;
	String strAccusedPersonInfoXml;
	String strComplaintFilesXmlComplaintDescription;
	String COMPUNIQCODE;

	// search record
	String m_login_id;
	String m_lang_cd;
	String o_year;
	String m_service_id_no;
	String m_service_type_cd;
	String m_first_name;

	// present address params
	String addressline1;
	String addressline2;
	String addressline3;
	String village;
	String tehsil;
	String countrycd;
	String add_statecd;
	String add_districtcd;
	String add_pscd;
	String pincode;

	//save citizen tip
	String int_LangCd;
	String nvc_InformationDesc;
	String int_InformationModeCd;
	String nvc_IsPsKnown;
	String nvc_IsDistrictKnown;
	String nvc_RecordCreatedBy;
	String int_AssignedStateCd;
	String int_AssignedDistrictCd;
	String int_AssignedPsCd;
	String int_AssignedOfficeCd;
	String nvc_AnyDocUploaded;
	String nvc_RecordStatus;
	String int_OriginalRecords;
	String int_NatureOfInformation;
	String XMLDOC_CITIZENTIP_UPLOAD_DOCUMENTS;
	String XMLDOC_CITIZENTIP_INFORMER_DETAILS;
	String XMLDOC_CITIZENTIP_INFORMER_ADDRESS;
	String XMLDOC_CITIZENTIP_SUSPECT_DETAILS;
	String XMLDOC_CITIZENTIP_SUSPECT_ADDRESS;

	@Keep
	String seed;

	@Keep
	String coco;

	public JSONPostParams(String state_cd) {
		this.state_cd = state_cd;
	}

	public JSONPostParams(String state_cd, String district_cd) {
		this.state_cd = state_cd;
		this.district_cd = district_cd;
	}

	public JSONPostParams(String method_name, String username, String password) {

		if(method_name.toString().equals("LOGIN")) {
			this.username = username;
			this.password = password;
		}// end if login

	}// end constructor

	///Map Message = new HashMap();	Message.put("title", "Test message");Message.get("title")
	public JSONPostParams(String method_name, Map mapParams) {


		// @logic may in future we need to add more parameters
		if (method_name == "LOGIN") {

			this.seed = mapParams.get("seed").toString();

		}

		// @logic may in future we need to add more parameters
		if (method_name == "mStateConnect") {

			this.seed = mapParams.get("seed").toString();

		}

		// @logic may in future we need to add more parameters
		if (method_name == "mDistrictConnect") {

			this.seed = mapParams.get("seed").toString();

		}

		// @logic may in future we need to add more parameters
		if (method_name == "mCountryConnect") {

			this.seed = mapParams.get("seed").toString();

		}

		// @logic may in future we need to add more parameters
		if (method_name == "mRegisterComplaintConnect") {

			this.seed = mapParams.get("seed").toString();
			this.coco = mapParams.get("coco").toString();

		}


		// @logic may in future we need to add more parameters
		if (method_name == "mSaveCitizenTip") {

			this.seed = mapParams.get("seed").toString();

		}

        // @logic may in future we need to add more parameters
        if (method_name == "mDistrictOfficeConnect") {

            this.seed = mapParams.get("seed").toString();

        }

        // @logic may in future we need to add more parameters
        if (method_name == "mStateOfficeConnect") {

            this.seed = mapParams.get("seed").toString();

        }

		// @logic may in future we need to add more parameters
		if (method_name == "mGetServiceStatus") {

			this.seed = mapParams.get("seed").toString();

		}


		if (method_name == "SUBMIT_COMPLAINT") {
			this.PersonCode = 	mapParams.get("PersonCode").toString();
			this.PersonTypeCD = 	mapParams.get("PersonTypeCD").toString();
			this.UidNum = 	mapParams.get("UidNum").toString();
			this.FirstName = 	mapParams.get("FirstName").toString();
			this.MiddleName = 	mapParams.get("MiddleName").toString();
			this.LastName = 	mapParams.get("LastName").toString();
			this.RelativeType = 	mapParams.get("RelativeType").toString();
			this.RelativeName = 	mapParams.get("RelativeName").toString();
			this.Dob = 	mapParams.get("Dob").toString();
			this.Mobile1 = 	mapParams.get("Mobile1").toString();
			this.Mobile2 = 	mapParams.get("Mobile2").toString();
			this.Telephone = 	mapParams.get("Telephone").toString();
			this.Email = 	mapParams.get("Email").toString();
			this.RecordStatus = 	mapParams.get("RecordStatus").toString();
			this.RecordCreatedOn = 	mapParams.get("RecordCreatedOn").toString();
			this.RecordCreatedBy = 	mapParams.get("RecordCreatedBy").toString();
			this.OriginalRecord = 	mapParams.get("OriginalRecord").toString();
			this.ComplRegNum = 	mapParams.get("ComplRegNum").toString();
			this.LangCd = 	mapParams.get("LangCd").toString();
			this.ComplSrno = 	mapParams.get("ComplSrno").toString();
			this.ComplYear = 	mapParams.get("ComplYear").toString();
			this.ComplRegDt = 	mapParams.get("ComplRegDt").toString();
			this.RequisitionType = 	mapParams.get("RequisitionType").toString();
			this.ComplaintNatureCd = 	mapParams.get("ComplaintNatureCd").toString();
			this.ReceiptModeCd = 	mapParams.get("ReceiptModeCd").toString();
			this.ComplDesc = 	mapParams.get("ComplDesc").toString();
			this.ComplaintRemarks = 	mapParams.get("ComplaintRemarks").toString();
			this.IncidentPlc = 	mapParams.get("IncidentPlc").toString();
			this.IncidentType = 	mapParams.get("IncidentType").toString();
			this.IncidentFromDt = 	mapParams.get("IncidentFromDt").toString();
			this.IncidentToDt = 	mapParams.get("IncidentToDt").toString();
			this.IsPsKnown = 	mapParams.get("IsPsKnown").toString();
			this.IsDistrictKnown = 	mapParams.get("IsDistrictKnown").toString();
			this.IsOfficeKnown = 	mapParams.get("IsOfficeKnown").toString();
			this.SubmitDistrictCd = 	mapParams.get("SubmitDistrictCd").toString();
			this.SubmitPsCd = 	mapParams.get("SubmitPsCd").toString();
			this.SubmitOfficeCd = 	mapParams.get("SubmitOfficeCd").toString();
			this.InformationTypeCD = 	mapParams.get("InformationTypeCD").toString();
			this.StateCD = 	mapParams.get("StateCD").toString();
			this.strComplainantPresentAddressXml = 	mapParams.get("strComplainantPresentAddressXml").toString();
			this.strComplainantPermanentAddressXml = 	mapParams.get("strComplainantPermanentAddressXml").toString();
			this.strComplaintFilesXml = 	mapParams.get("strComplaintFilesXml").toString();
			this.strPersonNationalityCitizenXml = 	mapParams.get("strPersonNationalityCitizenXml").toString();
			this.NationalityCd = 	mapParams.get("NationalityCd").toString();
			this.YOB = 	mapParams.get("YOB").toString();
			this.Age = 	mapParams.get("Age").toString();
			this.AgeMonths = 	mapParams.get("AgeMonths").toString();
			this.AgeFromYrs = 	mapParams.get("AgeFromYrs").toString();
			this.AgeToYrs = 	mapParams.get("AgeToYrs").toString();
			this.strAccusedPersonInfoXml = 	mapParams.get("strAccusedPersonInfoXml").toString();
			this.strComplaintFilesXmlComplaintDescription = 	mapParams.get("strComplaintFilesXmlComplaintDescription").toString();
			this.COMPUNIQCODE = 	mapParams.get("COMPUNIQCODE").toString();

			this.addressline1 = 	mapParams.get("addressline1").toString();
			this.addressline2 = 	mapParams.get("addressline2").toString();
			this.addressline3 = 	mapParams.get("addressline3").toString();
			this.village = 	mapParams.get("village").toString();
			this.tehsil = 	mapParams.get("tehsil").toString();
			this.countrycd = 	mapParams.get("countrycd").toString();
			this.add_statecd = 	mapParams.get("add_statecd").toString();
			this.add_districtcd = 	mapParams.get("add_districtcd").toString();
			this.add_pscd = 	mapParams.get("add_pscd").toString();
			this.pincode = 	mapParams.get("pincode").toString();

			this.pincode = 	mapParams.get("USER_LOGIN_ID").toString();
			this.pincode = 	mapParams.get("USER_APPLICANT_NUM").toString();


		}// end if


		if (method_name == "SEARCH_RECORD") {

			this.m_login_id = 	mapParams.get("m_login_id").toString();
			this.m_lang_cd = 	mapParams.get("m_lang_cd").toString();
			this.o_year = 	mapParams.get("o_year").toString();
			this.m_service_id_no = 	mapParams.get("m_service_id_no").toString();
			this.m_service_type_cd = 	mapParams.get("m_service_type_cd").toString();
			this.m_first_name = 	mapParams.get("m_first_name").toString();


		}

		if (method_name == "SAVE_CITIZEN_TIP") {

			this.int_LangCd =	mapParams.get("int_LangCd").toString();
			this.nvc_InformationDesc =	mapParams.get("nvc_InformationDesc").toString();
			this.int_InformationModeCd =	mapParams.get("int_InformationModeCd").toString();
			this.nvc_IsPsKnown =	mapParams.get("nvc_IsPsKnown").toString();
			this.nvc_IsDistrictKnown =	mapParams.get("nvc_IsDistrictKnown").toString();
			this.nvc_RecordCreatedBy =	mapParams.get("nvc_RecordCreatedBy").toString();
			this.int_AssignedStateCd =	mapParams.get("int_AssignedStateCd").toString();
			this.int_AssignedDistrictCd =	mapParams.get("int_AssignedDistrictCd").toString();
			this.int_AssignedPsCd =	mapParams.get("int_AssignedPsCd").toString();
			this.int_AssignedOfficeCd =	mapParams.get("int_AssignedOfficeCd").toString();
			this.nvc_AnyDocUploaded =	mapParams.get("nvc_AnyDocUploaded").toString();
			this.nvc_RecordStatus =	mapParams.get("nvc_RecordStatus").toString();
			this.int_OriginalRecords =	mapParams.get("int_OriginalRecords").toString();
			this.int_NatureOfInformation =	mapParams.get("int_NatureOfInformation").toString();
			this.XMLDOC_CITIZENTIP_UPLOAD_DOCUMENTS =	mapParams.get("XMLDOC_CITIZENTIP_UPLOAD_DOCUMENTS").toString();
			this.XMLDOC_CITIZENTIP_INFORMER_DETAILS =	mapParams.get("XMLDOC_CITIZENTIP_INFORMER_DETAILS").toString();
			this.XMLDOC_CITIZENTIP_INFORMER_ADDRESS =	mapParams.get("XMLDOC_CITIZENTIP_INFORMER_ADDRESS").toString();
			this.XMLDOC_CITIZENTIP_SUSPECT_DETAILS =	mapParams.get("XMLDOC_CITIZENTIP_SUSPECT_DETAILS").toString();
			this.XMLDOC_CITIZENTIP_SUSPECT_ADDRESS =	mapParams.get("XMLDOC_CITIZENTIP_SUSPECT_ADDRESS").toString();


		}

	}// end json post params


}// end json post params
