package ncrb.nic.in.citizenservices.citizen_general_service;

import ncrb.nic.in.citizenservices.R;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPPoliceContact;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TelePhoneListViewActivity extends AppCompatActivity {

    private List<TelePhoneListViewFormat> telePhoneNoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TelePhoneListViewAdapter mAdapter;
    final int REQUEST_CODE_ASK_PERMISSIONS = 100;
    public ProgressDialog mProgressDialog;
    Singleton singleton = Singleton.getInstance();
    MCoCoRy mCoCoRy = new MCoCoRy();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tele_phone_list_view);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new TelePhoneListViewAdapter(telePhoneNoList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                TelePhoneListViewFormat telePhoneNo = telePhoneNoList.get(position);

                Toast.makeText(getApplicationContext(), "Calling " + telePhoneNo.getFIRST_NAME(), Toast.LENGTH_SHORT).show();

                try {

                    if (ContextCompat.checkSelfPermission(TelePhoneListViewActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                                REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telePhoneNo.getMOBILE_1()));
                        startActivity(intent);
                    }

                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(TelePhoneListViewActivity.this,
                            "Call failed, please try again later!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        //prepareMovieData();
        validate();
    }// end on create

    //public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Call Permission Granted..Please dial again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Call permission not granted", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * @ Validation
     */

    public void validate() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else {
            try {
                GetPoliceStationContact();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }// end else if else validation loop

    }// end validate

    /**
     * @ get  ps list
     */

    public void GetPoliceStationContact() throws Exception {

        this.mProgressDialog.show();

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = "";
        String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("state_cd", Constants.STATECD);
            postParams.put("m_service", Constants.mPoliceStationContact);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();


        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params " + postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mStateConnect", postParams);

        Utils.printv("mDistrictConnect json post params " + jsonPostParams.toString());


        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mPoliceStationContact(jsonPostParams,
                new Callback<WSPPoliceContact>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "connect to server telephone connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPPoliceContact result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        try {

                            Utils.printv("RESULT mPoliceStationContact " + result.getListPSContact());
                            Utils.printv("RESULT status " + result.getSTATUS_CODE());

                            for (WSPPoliceContact.ListPSContact list : result.getListPSContact()) {

                                if (singleton.telephone_district_cd.equals(list.DISTRICT_CD)) {

                                    String mobile = "";
                                    if(list.MOBILE_1.length() == 10) mobile = list.MOBILE_1;
                                    else mobile = list.MOBILE_2;


                                    TelePhoneListViewFormat telePhoneNo =
                                            new TelePhoneListViewFormat(list.FIRST_NAME, list.LAST_NAME, mobile
                                                    , list.EMAIL, list.PS);

                                    telePhoneNoList.add(telePhoneNo);
                                }

                            }// for

                            // check if data not available for district
                            if (telePhoneNoList.isEmpty()) {
                                TelePhoneListViewFormat telePhoneNo =
                                        new TelePhoneListViewFormat("-- No Data Found --", "", "", "", "");
                                telePhoneNoList.add(telePhoneNo);
                            }


                            mAdapter.notifyDataSetChanged();


                        } catch (Exception e) {

                        }


                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT mPoliceStationContact success");
                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!! Server Error.", Toast.LENGTH_SHORT);
                        }

                    }// end success

                });

    }// end  web service

}// end main class

