package ncrb.nic.in.citizenservices.citizen_general_service;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import ncrb.nic.in.citizenservices.R;

public class TelePhoneListViewAdapter extends RecyclerView.Adapter<TelePhoneListViewAdapter.MyViewHolder> {

    private List<TelePhoneListViewFormat> telePhoneList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView contact_name, contact_no, contact_email ,ps;

        public MyViewHolder(View view) {
            super(view);
            contact_name = (TextView) view.findViewById(R.id.contact_name);
            contact_no = (TextView) view.findViewById(R.id.contact_no);
            contact_email = (TextView) view.findViewById(R.id.contact_email);
            ps = (TextView) view.findViewById(R.id.ps);
        }
    }

    public TelePhoneListViewAdapter(List<TelePhoneListViewFormat> telePhoneList) {
        this.telePhoneList = telePhoneList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_tele_phone_list_view_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TelePhoneListViewFormat telePhoneFormat = telePhoneList.get(position);
        holder.contact_name.setText(telePhoneFormat.getFIRST_NAME());
        holder.contact_no.setText(telePhoneFormat.getMOBILE_1());
        holder.contact_email.setText(telePhoneFormat.getEMAIL());
        holder.ps.setText(telePhoneFormat.getPS());
    }

    @Override
    public int getItemCount() {
        return telePhoneList.size();
    }
}
