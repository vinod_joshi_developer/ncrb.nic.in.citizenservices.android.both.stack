package ncrb.nic.in.citizenservices.constants;

/**
 * Interface to hold all database constants.
 *
 * @Ultron
 */
public interface DBConstants {

    String PD_DB_NAME = "complaint_registration.db";

    int PD_DB_DEF_VERSION = 6;

    /**
     * APP Config Request params
     */
    String CONF_ANDROID_VERSION = "os_version";
    String CONF_APP_VERSION = "app_version";
    String CONF_DB_VERSION = "db_version";
    String CONF_DEVICE_ID = "id_device";
    String CONF_BRAND = "brand";
    String CONF_EMAIL = "email";
    String CONF_MOBILE = "mobile_no";
    String CONF_MODEL = "model";
    String CONF_PASSWORD = "password";
    String CONF_REGISTRATION_ID = "id_registration";
    String CONF_THEME_VERSION = "theme_version";
    String CONF_USER_NAME = "username";
    String CONF_VC_VERSION = "vc_version";

    /**
     * Interface to hold all tables naems
     */
    interface Tables {

        String M_STATE = "m_state";
        String M_DISTRICT = "m_district";
        String POLICE_STATION = "police_station";

    }

    /**
     * Interface to hold all tables columns
     */
    interface Columns {

        String ID_CATEGORY = "ID_CATEGORY";
        String DISTRICT_CD = "district_cd";
        String STATE_CD = "state_cd";
        String DISTRICT = "district";
        String STATE = "STATE";
        String PS = "ps";
        String PS_CODE = "ps_code";

    }

    /**
     * Interface to hold all select queries
     */
    interface Select {

        // find all states
        String QUERY_STATES = "SELECT * FROM " + Tables.M_STATE;

        // find districts by state code
        String QUERY_DISTRICT_BY_STATE_CODE = "SELECT * FROM " + Tables.M_DISTRICT +
                " WHERE " + Columns.STATE_CD + " = %s" +
                " ORDER BY " + Columns.DISTRICT;

        // find state code
        String Q_STATE_CODE_BY_NAME = "SELECT " + Columns.STATE_CD + " " +
                " FROM " + Tables.M_STATE +
                " WHERE " + Columns.STATE + " LIKE '%s' ";

        // find district code
        String Q_DISTRICT_CD_BY_NAME = "SELECT * " +
                " FROM " + Tables.M_DISTRICT +
                " WHERE " + Columns.DISTRICT + " LIKE '%s' " +
                " AND " + Columns.STATE_CD + " = '%s' ";

        // find police statio by code
        String Q_PS_BY_DISTRICT_CD = "SELECT * FROM " + Tables.POLICE_STATION +
                " WHERE " + Columns.DISTRICT_CD + " = %s" +
                " ORDER BY " + Columns.PS;

        // find police station code
        String Q_PS_CODE_BY_NAME = "SELECT " + Columns.PS_CODE +
                " FROM " + Tables.POLICE_STATION +
                " WHERE " + Columns.PS + " LIKE '%s' " +
                " AND " + Columns.DISTRICT_CD + " = '%s' " +
                " AND " + Columns.STATE_CD + " = '%s' ";

        // find nature of complaint
        String QUERY_NATURE_OF_COMPLAINT = "SELECT  COMPLAINT_NATURE_CD, COMPLAINT_NATURE " +
                "FROM m_complaint_nature " +
                "WHERE LANG_CD = 99 AND COMPLAINT_NATURE  != '' GROUP BY COMPLAINT_NATURE ORDER BY COMPLAINT_NATURE ASC ";

        // find country / nationality
        String QUERY_NATIONALITY = "SELECT  NATIONALITY_CD, NATIONALITY " +
                "FROM m_nationality " +
                "WHERE LANG_CD = 99 AND NATIONALITY  != '' ORDER BY NATIONALITY ASC";

    }// end select constant

    /**
     * Interface to hold all select queries
     */
    interface SyncQueryState {

        String INSERT = "INSERT INTO  m_state (state_cd, state, RECORD_SYNC_ON) VALUES (%s, '%s','%s')";
        String UPDATE = "UPDATE m_state SET state = '%s' WHERE state_cd = %s";
        String DELETE = "DELETE FROM m_state WHERE state_cd = %s";

    }// end Sync Query State

    /**
     * Interface to hold all select queries
     */
    interface SyncQueryDistrict {

        String INSERT = "INSERT INTO  m_district (district_cd, state_cd, district) VALUES ('%s', '%s','%s')";
        String UPDATE = "UPDATE m_district SET district = '%s'  ,  state_cd = '%s' WHERE district_cd = '%s' ";
        String DELETE = "DELETE FROM m_district WHERE district_cd = '%s'";

    }// end Sync Query district

    /**
     * Interface to hold all select queries
     */
    interface SyncQueryPS {

        String INSERT = "INSERT INTO police_station (ps_code, state_cd, district_cd, ps) VALUES ('%s', '%s', '%s', '%s');";
        String UPDATE = "UPDATE police_station SET state_cd = '%s'  ,  district_cd = '%s' ,  ps = '%s' WHERE ps_code = '%s' ";
        String DELETE = "DELETE FROM police_station WHERE ps_code = '%s'";

    }// end Sync Query police station


}// end main class DB constants
