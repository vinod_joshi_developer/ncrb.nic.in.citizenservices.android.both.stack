package ncrb.nic.in.citizenservices.citizen_general_service;

public class TelePhoneListViewFormat {

    private String FIRST_NAME;
    private String LAST_NAME;
    private String MOBILE_1;
    private String EMAIL;
    private String PS;

    public TelePhoneListViewFormat() {
    }

    public TelePhoneListViewFormat(String FIRST_NAME, String LAST_NAME, String MOBILE_1
            , String EMAIL
            , String PS) {
        this.FIRST_NAME = FIRST_NAME;
        this.LAST_NAME = LAST_NAME;
        this.MOBILE_1 = MOBILE_1;
        this.EMAIL = EMAIL;
        this.PS = PS;

    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getLAST_NAME() {
        return LAST_NAME;
    }

    public void setLAST_NAME(String LAST_NAME) {
        this.LAST_NAME = LAST_NAME;
    }

    public String getMOBILE_1() {
        return MOBILE_1;
    }

    public void setMOBILE_1(String MOBILE_1) {
        this.MOBILE_1 = MOBILE_1;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getPS() {
        return PS;
    }

    public void setPS(String PS) {
        this.PS = PS;
    }

}// end main class

