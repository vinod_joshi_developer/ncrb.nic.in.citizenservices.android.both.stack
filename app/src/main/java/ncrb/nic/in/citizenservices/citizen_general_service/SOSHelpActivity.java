package ncrb.nic.in.citizenservices.citizen_general_service;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.HashMap;

import ncrb.nic.in.citizenservices.R;
import ncrb.nic.in.citizenservices.utils.AppLocationService;
import ncrb.nic.in.citizenservices.utils.AppPreferences;
import ncrb.nic.in.citizenservices.utils.Utils;

public class SOSHelpActivity extends AppCompatActivity {

    // set share preferences
    AppPreferences objAppPreferences;
    Context objContext;

    ImageButton imgButton;
    Button button;

    String sos1 = "sos1";
    String sos2 = "sos2";
    String sos3 = "sos3";
    String sos4 = "sos4";
    String sos5 = "sos5";
    String sos_msg = "sos_msg";

    HashMap<String, String> latlng = new HashMap<String, String>();
    AppLocationService appLocationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soshelp);

        // core objects
        appLocationService = new AppLocationService(SOSHelpActivity.this);
        objContext = this;
        objAppPreferences = new AppPreferences(objContext);


        imgButton =(ImageButton)findViewById(R.id.sosActionImgBtn);
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"SOS Detail Sent.",Toast.LENGTH_LONG).show();

                String latlng_value = findLatLng();
                latlng_value = objAppPreferences.getUserDefaults(sos_msg)+","+latlng_value;
                Utils.printv("SOS msg "+latlng_value);

                try {

                    if(!objAppPreferences.getUserDefaults(sos1).equals("")) sendSMS(objAppPreferences.getUserDefaults(sos1),latlng_value);
                    if(!objAppPreferences.getUserDefaults(sos2).equals("")) sendSMS(objAppPreferences.getUserDefaults(sos2),latlng_value);
                    if(!objAppPreferences.getUserDefaults(sos3).equals("")) sendSMS(objAppPreferences.getUserDefaults(sos3),latlng_value);
                    if(!objAppPreferences.getUserDefaults(sos4).equals("")) sendSMS(objAppPreferences.getUserDefaults(sos4),latlng_value);
                    if(!objAppPreferences.getUserDefaults(sos5).equals("")) sendSMS(objAppPreferences.getUserDefaults(sos5),latlng_value);

                } catch (Exception e) {
                    Utils.showToastMsg(getApplicationContext(),"Please save your contacts and message", Toast.LENGTH_LONG);
                }


            }
        });

        button = (Button) findViewById(R.id.sosContactBtn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"SOS Contact.",Toast.LENGTH_LONG).show();

                Intent mainIntent = new Intent(SOSHelpActivity.this, SOSContactActivity.class);
                SOSHelpActivity.this.startActivity(mainIntent);

            }
        });

    }// end main class

    /**
     * @method
     * */

    private void sendSMS(String phoneNumber, String message) {

        PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, Object.class), 0);
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, pi, null);

    }// end send sms

    /**
     * @method
     * */

    public String findLatLng() {

        String latlng_value = "";

        latlng = Utils.getCurrentLatLng(SOSHelpActivity.this, appLocationService);
        //network_gps_enabled = true;
        if (latlng.get("network_gps_enabled").equals("true")) {

            latlng_value = " Location: http://www.google.com/maps/place/"+latlng.get("lat")+","+latlng.get("lng");

        } else {

            Utils.showAlert(SOSHelpActivity.this, "Either location permission not given to app or GPS location not available. " +
                    "\n\nPlease allow location permission. i.e. Settings > App > Permissions.");

        }

        return latlng_value;

    }// end find lat lng

}// end main activity