package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class WSPCountryConnect {

    public String STATUS_CODE;
    public String STATUS;
    public List<Country> listCountry;

    public List<Country> getCountry() {
        return listCountry;
    }

    public void setCountry(List<Country> country) {
        this.listCountry = country;
    }


    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }

    @Keep
    public class Country {

        public String COUNTRY_CD;
        public String COUNTRY;

    }// end State office names

}// end main class
