package ncrb.nic.in.citizenservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.DistrictKV;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.json.objects.Nationality;
import ncrb.nic.in.citizenservices.json.objects.PoliceStationKV;
import ncrb.nic.in.citizenservices.json.objects.StateKV;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPCountryConnect;
import ncrb.nic.in.citizenservices.services_params.WSPStateConnect;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;
import ncrb.nic.in.citizenservices.utils.AppLocationService;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ComplaintAddressActivity extends AppCompatActivity {

    private AppCompatEditText et_house_no;
    private TextInputLayout txt_house_no;

    private AppCompatEditText et_street;
    private TextInputLayout txt_street;

    private AppCompatEditText et_colony;
    private TextInputLayout txt_colony;

    private AppCompatEditText et_village;
    private TextInputLayout txt_village;

    private AppCompatEditText et_tehsil;
    private TextInputLayout txt_tehsil;

    private AppCompatEditText et_pincode;
    private TextInputLayout txt_pincode;

    Singleton singleton = Singleton.getInstance();
    public ProgressDialog mProgressDialog;
    AppLocationService appLocationService;
    HashMap<String, String> latlng = new HashMap<String, String>();

    private Spinner listOfNationality;
    private ArrayAdapter<Nationality> listOfNationalityAdaptor;

    final String txt_select = "-- select --";

    private Spinner listOfDistrict;
    private ArrayAdapter<DistrictKV> listOfDistrictAdaptor;


    private Spinner listOfPoliceStation;
    private ArrayAdapter<PoliceStationKV> listOfPoliceStationAdaptor;

    private Spinner listOfState;
    private ArrayAdapter<StateKV> listOfStateAdaptor;

    MCoCoRy mCoCoRy = new  MCoCoRy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_address);

        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        this.setToolbarAction();

        // set the editing
        formTextEditing();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

        appLocationService = new AppLocationService(ComplaintAddressActivity.this);

    }// end onCreate

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().getAttributes().windowAnimations = R.style.WindowAnimationTransition;

        try {

            getCountries();

            // reload the state, we got the state from web service in submission activity
            ArrayList<StateKV> stateKV = new ArrayList<>();
            stateKV.add(new StateKV("0", txt_select));

            for (WSPStateConnect.StateLists list : singleton.wspStateConnect.getStates()) {
                stateKV.add(new StateKV(list.STATE_CD.toString(), list.STATE.toString()));
            }

            if (!stateKV.isEmpty()) {
                setStateSpinners(stateKV);
            }

        } catch (Exception ex) {
            Utils.showToastMsg(getApplicationContext(),"Incorrect input. Please try later",Toast.LENGTH_LONG);
        }

    }// end onStart

    /*
    * @ get countries
    * */
    private void getCountries(){

        try {
            if (!Utils.isNetworkAvailable(this)) {
                Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else GetCountryWebService();

        } catch (Exception ex) {
            Utils.showToastMsg(getApplicationContext(),"Incorrect input. Please try later",Toast.LENGTH_LONG);
        }

    }// end get countries

    /**
     * @ tool bar action
     */
    public void setToolbarAction() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        //toolbar.setLogo(R.mipmap.ic_launcher);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }// end set toolbar action

    /**
     * @ option menu
     **/
    @Override
    public void onBackPressed() {

        this.singleton.add_house_no = et_house_no.getText().toString();
        this.singleton.add_street = et_street.getText().toString();
        this.singleton.add_colony = et_colony.getText().toString();
        this.singleton.add_village = et_village.getText().toString();
        this.singleton.add_tehsil = et_tehsil.getText().toString();
        this.singleton.add_pincode = et_pincode.getText().toString();

        super.onBackPressed();

    }// end on back pressed method

    /**
     * @ Text editing related
     */

    public void formTextEditing() {

        et_house_no = (AppCompatEditText) findViewById(R.id.et_house);
        et_house_no.setText(singleton.wspLoginConnect.ADDRESS_LINE_1);
        if (this.singleton.add_house_no != null && this.singleton.add_house_no != "")
            et_house_no.setText(singleton.add_house_no);
        txt_house_no = (TextInputLayout) findViewById(R.id.til_house);
        et_house_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_house_no.getText().length() > 0) {
                    txt_house_no.setError(null);
                    txt_house_no.setErrorEnabled(false);
                }
            }
        });// end method

        // complainant address
        et_street = (AppCompatEditText) findViewById(R.id.et_street);
        et_street.setText(singleton.wspLoginConnect.ADDRESS_LINE_2);
        if (this.singleton.add_street != null && this.singleton.add_street != "")
            et_street.setText(singleton.add_street);
        txt_street = (TextInputLayout) findViewById(R.id.til_street);
        et_street.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_street.getText().length() > 0) {
                    txt_street.setError(null);
                    txt_street.setErrorEnabled(false);
                }
            }
        });// end method

        et_colony = (AppCompatEditText) findViewById(R.id.et_colony);
        et_colony.setText(singleton.wspLoginConnect.ADDRESS_LINE_3);
        if (this.singleton.add_colony != null && this.singleton.add_colony != "")
            et_colony.setText(singleton.add_colony);
        txt_colony = (TextInputLayout) findViewById(R.id.til_colony);
        et_colony.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_colony.getText().length() > 0) {
                    txt_colony.setError(null);
                    txt_colony.setErrorEnabled(false);
                }
            }
        });// end method

        et_village = (AppCompatEditText) findViewById(R.id.et_village);
        et_village.setText(singleton.wspLoginConnect.VILLAGE);
        if (this.singleton.add_village != null && this.singleton.add_village != "")
            et_village.setText(singleton.add_village);
        txt_village = (TextInputLayout) findViewById(R.id.til_village);
        et_village.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_village.getText().length() > 0) {
                    txt_village.setError(null);
                    txt_village.setErrorEnabled(false);
                }
            }
        });// end method

        et_tehsil = (AppCompatEditText) findViewById(R.id.et_tehsil);
        et_tehsil.setText(singleton.wspLoginConnect.TEHSIL);
        if (this.singleton.add_tehsil != null && this.singleton.add_tehsil != "")
            et_tehsil.setText(singleton.add_tehsil);
        txt_tehsil = (TextInputLayout) findViewById(R.id.til_tehsil);
        et_tehsil.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_tehsil.getText().length() > 0) {
                    txt_tehsil.setError(null);
                    txt_tehsil.setErrorEnabled(false);
                }
            }
        });// end method

        et_pincode = (AppCompatEditText) findViewById(R.id.et_pincode);
        if (Utils.isNumeric(singleton.wspLoginConnect.PINCODE))
            et_pincode.setText(singleton.wspLoginConnect.PINCODE);
        else
            et_pincode.setText("");
        if (this.singleton.add_pincode != null && this.singleton.add_pincode != "")
            et_pincode.setText(singleton.add_pincode);

        txt_pincode = (TextInputLayout) findViewById(R.id.til_pincode);
        et_pincode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_pincode.getText().length() > 0) {
                    txt_pincode.setError(null);
                    txt_pincode.setErrorEnabled(false);
                }
            }
        });// end method

    }// end form text editing

    /**
     * @ Validation
     */

    public void validate() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (et_village.getText().toString().trim().equals("")) {
            txt_village.setError(getString(R.string.cannot_left_empty_msg));
        } else if (et_pincode.getText().toString().trim().equals("")) {
            et_pincode.setError(getString(R.string.cannot_left_empty_msg));
        } else if (listOfState.getSelectedItem().toString() == ""
                || listOfState.getSelectedItem().toString() == txt_select) {
            Utils.showToastMsg(getApplicationContext(), "Please select state", Toast.LENGTH_LONG);
        } else if (listOfDistrict.getSelectedItem().toString() == ""
                || listOfDistrict.getSelectedItem().toString() == txt_select) {
            Utils.showToastMsg(getApplicationContext(), "Please select district", Toast.LENGTH_LONG);
        } else if (listOfPoliceStation.getSelectedItem().toString() == ""
                || listOfPoliceStation.getSelectedItem().toString() == txt_select) {
            Utils.showToastMsg(getApplicationContext(), "Please select police station", Toast.LENGTH_LONG);
        } else if (listOfNationality.getSelectedItem().toString() == ""
                || listOfNationality.getSelectedItem().toString() == txt_select) {
            Utils.showToastMsg(getApplicationContext(), "Please select country", Toast.LENGTH_LONG);
        } else {


            this.singleton.add_house_no = et_house_no.getText().toString();
            this.singleton.add_street = et_street.getText().toString();
            this.singleton.add_colony = et_colony.getText().toString();
            this.singleton.add_village = et_village.getText().toString();
            this.singleton.add_tehsil = et_tehsil.getText().toString();
            this.singleton.add_pincode = et_pincode.getText().toString();

            /* Create an Intent that will start the next Activity. */
            Intent mainIntent = new Intent(ComplaintAddressActivity.this
                    , ComplaintSrcFourthActivity.class);
            ComplaintAddressActivity.this.startActivity(mainIntent);

        }// end else if else validation loop

    }// end validate

    /**
     * @ get country
     */

    public void GetCountryWebService() throws Exception {

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {


            Map postParams = new HashMap();
            postParams.put("country", "");
            postParams.put("m_service", Constants.mCountryConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        this.mProgressDialog.show();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mCountryConnect", postParams);

        apiCaller.mCountryConnect(jsonPostParams,
                new Callback<WSPCountryConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                            if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPCountryConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            ArrayList<Nationality> countries = new ArrayList<>();
                            countries.add(new Nationality("0", txt_select));

                            for (WSPCountryConnect.Country list : result.getCountry()) {
                                countries.add(new Nationality(list.COUNTRY_CD.toString(), list.COUNTRY.toString()));
                            }

                            if (!countries.isEmpty()) {
                                setNationalitySpinner(countries);
                            }

                        } else {
                            //Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end web service country


    /**
     * @ spinners
     */

    public void setNationalitySpinner(ArrayList<Nationality> nationality) {

        listOfNationality = (Spinner) findViewById(R.id.spinner_nationality);
        listOfNationalityAdaptor = new ArrayAdapter<Nationality>(this, R.layout.simple_spinner_lay, nationality);
        listOfNationalityAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfNationality.setAdapter(listOfNationalityAdaptor);
        listOfNationality.setSelection(Utils.getSpinnerIndex(listOfNationality, "INDIA"));

        listOfNationality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Nationality nationality = (Nationality) parent.getSelectedItem();
                singleton.nationality_cd = nationality.getId();
                //Utils.showToastMsg(getApplicationContext(), nationality.getName(), Toast.LENGTH_LONG);

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


    /**
     * @ spinners district
     */

    public void setDistrictSpinners(ArrayList<DistrictKV> districtKV) {

        listOfDistrict = (Spinner) findViewById(R.id.spinner_district);
        listOfDistrictAdaptor = new ArrayAdapter<DistrictKV>(this, R.layout.simple_spinner_lay, districtKV);
        listOfDistrictAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrict.setAdapter(listOfDistrictAdaptor);

        listOfDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrict.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictKV keyValueObject = (DistrictKV) parent.getSelectedItem();
                    singleton.add_district_cd = keyValueObject.getId();

                    setPSSetting(keyValueObject.getId());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // auto complete district name
        autoSelectDistrict(districtKV);


    }// end spinners


    /**
     * @ auto select district
     */
    public void autoSelectDistrict(final ArrayList<DistrictKV> districtKV) {

        int index = 0;
        for (DistrictKV district : districtKV) {
            /*@Logic: auto fill user address info, if user pressed back, save current value and display
            same value again*/
            if (singleton.add_district_cd != null && singleton.add_district_cd != "") {
                if (singleton.add_district_cd.toString().equals(district.getId().toString())) {
                    listOfDistrict.setSelection(index);
                }
            } else if (singleton.wspLoginConnect.DISTRICT_CD.toString().equals(district.getId().toString())) {
                listOfDistrict.setSelection(index);
            }
            index++;
        }//end foreach

    }// end auto Select District


    /**
     * @ spinners
     */

    public void setPSSetting(String district_cd) {

        ArrayList<PoliceStationKV> policeStationKV = new ArrayList<>();
        policeStationKV.add(new PoliceStationKV("0", txt_select));

        for (wspDistrictConnect.DistrictLists list : singleton.wspDistrictConnect.getDistrictLists()) {
            if (district_cd.toString().equals(list.DISTRICT_CD)) {
                for (wspDistrictConnect.PoliceStationLists psList : list.policeStationsDataSet.getPoliceStationLists()) {
                    policeStationKV.add(new PoliceStationKV(psList.PS_CD.toString(), psList.PS.toString()));
                }
            }
        }

        if (!policeStationKV.isEmpty()) {
            setPoliceStationSpinners(policeStationKV);
        }

    }// end setPS Setting

    /**
     * @ spinners policeStation
     */

    public void setPoliceStationSpinners(ArrayList<PoliceStationKV> policeStationKV) {

        listOfPoliceStation = (Spinner) findViewById(R.id.spinner_police_station);
        listOfPoliceStationAdaptor = new ArrayAdapter<PoliceStationKV>(this, R.layout.simple_spinner_lay, policeStationKV);
        listOfPoliceStationAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfPoliceStation.setAdapter(listOfPoliceStationAdaptor);

        listOfPoliceStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfPoliceStation.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    
                    PoliceStationKV keyValueObject = (PoliceStationKV) parent.getSelectedItem();
                    singleton.add_ps_cd = keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // auto select the police station
        autoSelectPS(policeStationKV);

    }// end spinners

    /**
     * @ auto select ps
     */
    public void autoSelectPS(final ArrayList<PoliceStationKV> policeStationKV) {

        int index = 0;
        for (PoliceStationKV psKV : policeStationKV) {
             /*@Logic: auto fill user address info, if user pressed back, save current value and display
            same value again*/
            if (singleton.add_ps_cd != null && singleton.add_ps_cd != "") {
                if (singleton.add_ps_cd.toString().equals(psKV.getId().toString())) {
                    listOfPoliceStation.setSelection(index);
                }
            } else if (singleton.wspLoginConnect.PS_CD.toString().equals(psKV.getId().toString())) {
                listOfPoliceStation.setSelection(index);
            }
            index++;
        }//end foreach

    }// end auto Select ps

    /**
     * @ spinners state
     */

    public void setStateSpinners(ArrayList<StateKV> stateKV) {

        listOfState = (Spinner) findViewById(R.id.spinner_state);
        listOfStateAdaptor = new ArrayAdapter<StateKV>(this, R.layout.simple_spinner_lay, stateKV);
        listOfStateAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfState.setAdapter(listOfStateAdaptor);

        listOfState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfState.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    StateKV keyValueObject = (StateKV) parent.getSelectedItem();
                    singleton.add_state_cd = keyValueObject.getId();

                    if (!Utils.isNetworkAvailable(ComplaintAddressActivity.this)) {
                        Utils.showToastMsg(ComplaintAddressActivity.this, "Please check internet connection.", Toast.LENGTH_LONG);
                    } else GetDistrictCumPSWebService();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // auto complete state name
        autoSelectState(stateKV);

    }// end spinners

    /**
     * @ auto select state
     */
    public void autoSelectState(final ArrayList<StateKV> stateKV) {

        int index = 0;
        for (StateKV state : stateKV) {
             /*@Logic: auto fill user address info, if user pressed back, save current value and display
            same value again*/
            if (singleton.add_state_cd != null && singleton.add_state_cd != "") {
                if (singleton.add_state_cd.toString().equals(state.getId().toString())) {
                    listOfState.setSelection(index);
                }
            } else if (singleton.wspLoginConnect.STATE_CD.toString().equals(state.getId().toString())) {
                listOfState.setSelection(index);
            }
            index++;
        }//end foreach

    }// end auto Select state

    /**
     * @ get district and ps list
     */

    public void GetDistrictCumPSWebService() throws Exception {

        this.mProgressDialog.show();

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("state_cd", singleton.add_state_cd);
            postParams.put("m_service", Constants.mDistrictConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mDistrictConnect", postParams);

        apiCaller.mDistrictConnect(jsonPostParams,
                new Callback<wspDistrictConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(wspDistrictConnect result, Response response) {

                        try {

                            Utils.printv("RESULT status " + result.getSTATUS_CODE());

                            if (result.getSTATUS_CODE().toString().equals("200")) {
                                Utils.printv("RESULT success mDistrictConnect ");

                                singleton.wspDistrictConnect = result;

                                ArrayList<DistrictKV> districtKV = new ArrayList<>();
                                districtKV.add(new DistrictKV("0", txt_select));

                                for (wspDistrictConnect.DistrictLists list : result.getDistrictLists()) {
                                    districtKV.add(new DistrictKV(list.DISTRICT_CD.toString(), list.DISTRICT.toString()));
                                }

                                if (!districtKV.isEmpty()) {
                                    setDistrictSpinners(districtKV);
                                }

                            } else {

                                if (mProgressDialog != null && mProgressDialog.isShowing())
                                    mProgressDialog.dismiss();

                                Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                            }

                        } catch (Exception e) {

                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();

                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }

                    }// end success
                });

    }// end Register web service


}// end main class