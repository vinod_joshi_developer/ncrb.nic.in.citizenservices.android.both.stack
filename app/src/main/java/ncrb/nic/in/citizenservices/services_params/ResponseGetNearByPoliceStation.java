package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class ResponseGetNearByPoliceStation {

    @Keep
    private int statusCode;
    @Keep
    private String status;
    @Keep
    private ResponseData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    /**
     * Response class
     * */

    @Keep
    public class ResponseData {

        @Keep
        private String message;
        //public Stations stations;
        @Keep
        public List<Stations> stations;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<Stations> getData() {
            return stations;
        }

        public void setData(List<Stations> data) {
            this.stations = data;
        }
    }

    /**
     * Station class
     * */
    @Keep
    public class Stations {

        @Keep
        private String ps;
        @Keep
        private String ps_distance;
        @Keep
        private String distance;
        @Keep
        private String district_cd;
        @Keep
        private String district;
        @Keep
        private String ps_code;
        @Keep
        private String state;
        @Keep
        private String state_cd;

        public String getPs() {
            return ps;
        }

        public void setPs(String ps) {
            this.ps = ps;
        }

        public String getPs_distance() {
            return ps_distance;
        }

        public void setPs_distance(String ps_distance) {
            this.ps_distance = ps_distance;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getDistrict_cd() {
            return district_cd;
        }

        public void setDistrict_cd(String district_cd) {
            this.district_cd = district_cd;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getPs_code() {
            return ps_code;
        }

        public void setPs_code(String ps_code) {
            this.ps_code = ps_code;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getState_cd() {
            return state_cd;
        }

        public void setState_cd(String state_cd) {
            this.state_cd = state_cd;
        }

    }// end data

}// end main class response get near by police station
