package ncrb.nic.in.citizenservices.citizen_general_service;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ncrb.nic.in.citizenservices.R;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.DistrictKV;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TelePhoneDirActivity extends AppCompatActivity {

    final String txt_select = "-- Select District --";
    private Spinner listOfDistrict;
    private ArrayAdapter<DistrictKV> listOfDistrictAdaptor;
    Singleton singleton = Singleton.getInstance();
    public ProgressDialog mProgressDialog;
    Button btnShowContact;
    MCoCoRy mCoCoRy = new  MCoCoRy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tele_phone_dir);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        try {

            if (!Utils.isNetworkAvailable(TelePhoneDirActivity.this)) {
                Utils.showToastMsg(TelePhoneDirActivity.this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else GetDistrictConnectWithoutPS();

        } catch (Exception e) {
            e.printStackTrace();
        }


        btnShowContact = (Button) findViewById(R.id.btn_show_tele_contact);
        btnShowContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"SOS Contact.",Toast.LENGTH_LONG).show();
                validate();
            }
        });

    }// end on create


    /**
     * @ Validation
     */

    public void validate() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (listOfDistrict.getSelectedItem().toString() == ""
                || listOfDistrict.getSelectedItem().toString() == txt_select) {
            Utils.showToastMsg(getApplicationContext(), "Please select district", Toast.LENGTH_LONG);
        } else {

            /* Create an Intent that will start the next Activity. */
            Intent mainIntent = new Intent(TelePhoneDirActivity.this, TelePhoneListViewActivity.class);
            TelePhoneDirActivity.this.startActivity(mainIntent);

        }// end else if else validation loop

    }// end validate

    /**
     * @ get district and ps list
     */

    public void GetDistrictConnectWithoutPS() throws Exception {

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {


            Map postParams = new HashMap();
            postParams.put("state_cd", Constants.STATECD);
            postParams.put("m_service", Constants.mDistrictConnectWithoutPS);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        this.mProgressDialog.show();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mStateConnect", postParams);

        Utils.printv("mDistrictConnect json post params " + jsonPostParams.toString());

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mDistrictConnectWithoutPS(jsonPostParams,
                new Callback<wspDistrictConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "connect to server district connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());


                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(wspDistrictConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT mDistrictConnectWithoutPS " + result.getDistrictLists());

                        try {

                            if (result.getSTATUS_CODE().toString().equals("200")) {
                                Utils.printv("RESULT VJ success");

                                singleton.wspDistrictConnect = result;

                                ArrayList<DistrictKV> districtKV = new ArrayList<>();
                                districtKV.add(new DistrictKV("0", txt_select));

                                for (wspDistrictConnect.DistrictLists list : result.getDistrictLists()) {
                                    districtKV.add(new DistrictKV(list.DISTRICT_CD.toString(), list.DISTRICT.toString()));
                                }

                                if (!districtKV.isEmpty()) {
                                    setDistrictSpinners(districtKV);
                                }

                            } else {
                                Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                            }

                        }catch (Exception e) {
                            Utils.showToastMsg(getApplicationContext(), "Server error. Please try later." , Toast.LENGTH_LONG);
                        }

                    }// end success
                });

    }// end Register web service

    /**
     * @ spinners district
     */

    public void setDistrictSpinners(ArrayList<DistrictKV> districtKV) {

        listOfDistrict = (Spinner) findViewById(R.id.spinner_district);
        listOfDistrictAdaptor = new ArrayAdapter<DistrictKV>(this, R.layout.simple_spinner_lay, districtKV);
        listOfDistrictAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrict.setAdapter(listOfDistrictAdaptor);

        listOfDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrict.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictKV keyValueObject = (DistrictKV) parent.getSelectedItem();
                    singleton.telephone_district_cd = keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners

}// end main class
