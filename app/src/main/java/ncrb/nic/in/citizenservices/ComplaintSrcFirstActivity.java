package ncrb.nic.in.citizenservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.rey.material.widget.RadioButton;
import com.squareup.okhttp.OkHttpClient;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.DistrictKV;
import ncrb.nic.in.citizenservices.json.objects.DistrictCumStateOfficeKV;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.json.objects.PoliceStationKV;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPDistrictOfficeConnect;
import ncrb.nic.in.citizenservices.services_params.WSPStateOfficeConnect;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class ComplaintSrcFirstActivity extends AppCompatActivity {


    private RadioButton knowYourLocationYes;
    private RadioButton knowYourLocationNo;

    private RadioButton knowDistrictYes;
    private RadioButton knowDistrictNo;

    private LinearLayout linearLayoutDistrict;
    private LinearLayout viewSpinnerDistrict;
    private LinearLayout viewSpinnerPS;
    private LinearLayout viewSpinnerOffice;

    public ProgressDialog mProgressDialog;

    private Spinner listOfDistrict;
    private ArrayAdapter<DistrictKV> listOfDistrictAdaptor;


    private Spinner listOfPoliceStation;
    private ArrayAdapter<PoliceStationKV> listOfPoliceStationAdaptor;

    private Spinner listOfDistrictOffice;
    private ArrayAdapter<DistrictCumStateOfficeKV> listOfDistrictOfficeAdaptor;

    Singleton singleton = Singleton.getInstance();
    //wspDistrictConnect main_json_result;

    final String txt_select = "-- select --";

    private AppCompatEditText etPlaceOfOccurance;
    private TextInputLayout txtILPlaceOfOccurance;

    private AppCompatEditText etNationalityCode;
    private TextInputLayout txtILNationalityCode;

    private AppCompatEditText etMobileNumber;
    private TextInputLayout txtILMobileNumber;

    MCoCoRy mCoCoRy = new  MCoCoRy();


    /**
     * @
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submission);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        // district layout to hide or show or related actions
        linearLayoutDistrict = (LinearLayout) findViewById(R.id.lineLay_district);
        viewSpinnerDistrict = (LinearLayout) findViewById(R.id.view_district_spinner);
        viewSpinnerPS = (LinearLayout) findViewById(R.id.view_ps_spinner);
        viewSpinnerOffice = (LinearLayout) findViewById(R.id.view_office_spinner);

        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        settingCheckbox();

        formTextEditing();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean validated = false;
                try {

                } catch (Exception ex) {
                    Utils.printv("Exception " + ex.getMessage());
                }

                validated = validateSubmission();

                if (validated) {
                    /* Create an Intent that will start the next Activity. */
                    Intent mainIntent = new Intent(ComplaintSrcFirstActivity.this, ComplaintSrcSecondActivity.class);
                    ComplaintSrcFirstActivity.this.startActivity(mainIntent);
                }
            }
        });// end floating action button

        // get district with police station
        accessDistrictWithPS();

    }// end on create

    private void accessDistrictWithPS() {

        try {
            if (!Utils.isNetworkAvailable(ComplaintSrcFirstActivity.this)) {
                Utils.showToastMsg(ComplaintSrcFirstActivity.this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else GetDistrictCumPSWebService();
        } catch (Exception ex) {
            Utils.printv("Exception " + ex.getMessage());
        }

    }// end access district with ps

    /*
    * @on start
    * */
    @Override
    protected void onStart() {
        super.onStart();

    }// on start

    /**
     * @
     */

    public void onRadioClicked(View view) {

        // Is the view now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which checkbox was clicked
        switch (view.getId()) {

            case R.id.location_known_yes:

                if (checked) {
                    Utils.printv("location know yes");
                    linearLayoutDistrict.setVisibility(View.GONE);
                    viewSpinnerOffice.setVisibility(View.GONE);
                    viewSpinnerDistrict.setVisibility(View.VISIBLE);
                    viewSpinnerPS.setVisibility(View.VISIBLE);
                    resetSpinners();

                }
                break;
            case R.id.location_known_no:

                if (checked) {
                    Utils.printv("location know no");
                    linearLayoutDistrict.setVisibility(View.VISIBLE);
                    viewSpinnerPS.setVisibility(View.GONE);
                    viewSpinnerOffice.setVisibility(View.VISIBLE);
                    knowDistrictYes.setChecked(true);
                    resetSpinners();
                }
                break;
            case R.id.district_known_yes:

                if (checked) {
                    Utils.printv("district know yes");
                    viewSpinnerDistrict.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.district_known_no:

                if (checked) {
                    Utils.printv("district know no");
                    viewSpinnerDistrict.setVisibility(View.GONE);
                    try {
                        GetDistrictCumStateOfficeWebService();
                    } catch (Exception ex) {
                        Utils.printv("Exception " + ex.getMessage());
                    }

                }
                break;
        }

    }// end on Checkbox Clicked

    /**
     * @ checkboxes
     */
    public void resetSpinners() {

        listOfDistrict.setSelection(0);

    }// end reset spinners

    /**
     * @ checkboxes
     */

    public void settingCheckbox() {

        // check selection for location known or not
        knowYourLocationYes = (RadioButton) findViewById(R.id.location_known_yes);
        knowYourLocationNo = (RadioButton) findViewById(R.id.location_known_no);

        //  check selection for district known or not
        knowDistrictYes = (RadioButton) findViewById(R.id.district_known_yes);
        knowDistrictNo = (RadioButton) findViewById(R.id.district_known_no);


        // compound button for radio location known or not

        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    knowYourLocationYes.setChecked(knowYourLocationYes == buttonView);
                    knowYourLocationNo.setChecked(knowYourLocationNo == buttonView);

                }

            }

        };

        knowYourLocationYes.setOnCheckedChangeListener(listener);
        knowYourLocationNo.setOnCheckedChangeListener(listener);

        // compound button for radio district known or not

        CompoundButton.OnCheckedChangeListener listenerDistrictRadio = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    knowDistrictYes.setChecked(knowDistrictYes == buttonView);
                    knowDistrictNo.setChecked(knowDistrictNo == buttonView);

                }

            }

        };

        knowDistrictYes.setOnCheckedChangeListener(listenerDistrictRadio);
        knowDistrictNo.setOnCheckedChangeListener(listenerDistrictRadio);


        // select true as user know the police station
        knowYourLocationYes.setChecked(true);
        knowYourLocationNo.setChecked(false);

        // select true as user know the police station
        knowDistrictYes.setChecked(true);
        knowDistrictNo.setChecked(false);

        // initial hide the district option for user
        linearLayoutDistrict.setVisibility(View.GONE);
        viewSpinnerOffice.setVisibility(View.GONE);


    }// end setting location checkbox


    /**
     * @ get district and ps list
     */

    public void GetDistrictCumPSWebService() throws Exception {

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {


            Map postParams = new HashMap();
            postParams.put("state_cd", Constants.STATECD);
            postParams.put("m_service", Constants.mDistrictConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        this.mProgressDialog.show();


        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mStateConnect", postParams);

        Utils.printv("mDistrictConnect json post params " + jsonPostParams.toString());

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mDistrictConnect(jsonPostParams,
                new Callback<wspDistrictConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "connect to server district connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());


                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(wspDistrictConnect result, Response response) {


                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT VJ " + result.getDistrictLists());
                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        for (wspDistrictConnect.DistrictLists list : result.getDistrictLists()) {
                            Utils.printv("list item " + list.DISTRICT);
                        }

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ success");

                            singleton.wspDistrictConnect = result;

                            ArrayList<DistrictKV> districtKV = new ArrayList<>();
                            districtKV.add(new DistrictKV("0", txt_select));

                            for (wspDistrictConnect.DistrictLists list : result.getDistrictLists()) {
                                districtKV.add(new DistrictKV(list.DISTRICT_CD.toString(), list.DISTRICT.toString()));
                            }

                            if (!districtKV.isEmpty()) {
                                setDistrictSpinners(districtKV);
                            }

                        } else {

                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();

                            Utils.showToastMsg(getApplicationContext(), "District list not available!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end Register web service

    /**
     * @ spinners district
     */

    public void setDistrictSpinners(ArrayList<DistrictKV> districtKV) {

        listOfDistrict = (Spinner) findViewById(R.id.spinner_district);
        listOfDistrictAdaptor = new ArrayAdapter<DistrictKV>(this, R.layout.simple_spinner_lay, districtKV);
        listOfDistrictAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrict.setAdapter(listOfDistrictAdaptor);

        listOfDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrict.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictKV keyValueObject = (DistrictKV) parent.getSelectedItem();
                    singleton.district_cd = keyValueObject.getId();

                    // check if user don't know police station
                    if (knowYourLocationNo.isChecked()) {
                        GetDistrictCumStateOfficeWebService();
                    } else {
                        setPSSetting(keyValueObject.getId());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // auto complete district name
        autoSelectDistrict(districtKV);

    }// end spinners

    /**
     * @ auto select district
     */
    public void autoSelectDistrict(final ArrayList<DistrictKV> districtKV) {

        int index = 0;
        for (DistrictKV district : districtKV) {
            if (singleton.wspLoginConnect.DISTRICT_CD.toString().equals(district.getId().toString())) {
                listOfDistrict.setSelection(index);
            }
            index++;
        }//end foreach

    }// end auto Select District

    /**
     * @ spinners
     */

    public void setPSSetting(String district_cd) {

        ArrayList<PoliceStationKV> policeStationKV = new ArrayList<>();
        policeStationKV.add(new PoliceStationKV("0", txt_select));

        for (wspDistrictConnect.DistrictLists list : singleton.wspDistrictConnect.getDistrictLists()) {
            if (district_cd.toString().equals(list.DISTRICT_CD)) {
                for (wspDistrictConnect.PoliceStationLists psList : list.policeStationsDataSet.getPoliceStationLists()) {
                    policeStationKV.add(new PoliceStationKV(psList.PS_CD.toString(), psList.PS.toString()));
                }
            }
        }

        if (!policeStationKV.isEmpty()) {
            setPoliceStationSpinners(policeStationKV);
        }

    }// end setPS Setting

    /**
     * @ spinners policeStation
     */

    public void setPoliceStationSpinners(ArrayList<PoliceStationKV> policeStationKV) {

        listOfPoliceStation = (Spinner) findViewById(R.id.spinner_police_station);
        listOfPoliceStationAdaptor = new ArrayAdapter<PoliceStationKV>(this, R.layout.simple_spinner_lay, policeStationKV);
        listOfPoliceStationAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfPoliceStation.setAdapter(listOfPoliceStationAdaptor);

        listOfPoliceStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfPoliceStation.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    PoliceStationKV keyValueObject = (PoliceStationKV) parent.getSelectedItem();
                    singleton.ps_code = keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


    /**
     * @ get district and ps list
     */

    public void GetDistrictCumStateOfficeWebService() throws Exception {

        this.mProgressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("district_cd", singleton.district_cd);
            postParams.put("state_cd", Constants.STATECD);

            if (knowDistrictYes.isChecked()) {
                postParams.put("m_service", Constants.mDistrictOfficeConnect);
            } else {
                postParams.put("m_service", Constants.mStateOfficeConnect);
            }


            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mDistrictOfficeConnect", postParams);

        Utils.printv("district for office " + singleton.district_cd);


        if (knowDistrictYes.isChecked()) {
            districtWebCaller(restAdapter, jsonPostParams);
        } else {
            stateWebCaller(restAdapter, jsonPostParams);
        }

    }// end Register web service

    /**
     * @ district office web caller
     */
    public void districtWebCaller(RestAdapter restAdapter, JSONPostParams jsonPostParams) {

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mDistrictOfficeConnect(jsonPostParams,
                new Callback<WSPDistrictOfficeConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server district office.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPDistrictOfficeConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ getting offices");


                            ArrayList<DistrictCumStateOfficeKV> districtCumStateOfficeKV = new ArrayList<>();
                            districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV("0", txt_select));

                            for (WSPDistrictOfficeConnect.DistrictOffices offices : result.getDistrictOffices()) {
                                districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV(offices.OFFICE_CD.toString(), offices.OFFICE_NAME.toString()));
                            }

                            if (!districtCumStateOfficeKV.isEmpty()) {
                                setDistrictCumStateOfficeSpinners(districtCumStateOfficeKV);
                            }


                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });


    }// end district web caller

    /**
     * @ state office web caller
     */
    public void stateWebCaller(RestAdapter restAdapter, JSONPostParams jsonPostParams) {

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mStateOfficeConnect(jsonPostParams,
                new Callback<WSPStateOfficeConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server state office connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPStateOfficeConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ getting offices");


                            ArrayList<DistrictCumStateOfficeKV> districtCumStateOfficeKV = new ArrayList<>();
                            districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV("0", txt_select));

                            for (WSPStateOfficeConnect.StateOffices offices : result.getStateOffices()) {
                                districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV(offices.OFFICE_CD.toString(), offices.OFFICE_NAME.toString()));
                            }

                            if (!districtCumStateOfficeKV.isEmpty()) {
                                setDistrictCumStateOfficeSpinners(districtCumStateOfficeKV);
                            }


                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end state web caller

    /**
     * @ spinners district Office
     */

    public void setDistrictCumStateOfficeSpinners(ArrayList<DistrictCumStateOfficeKV> districtCumStateOfficeKV) {

        listOfDistrictOffice = (Spinner) findViewById(R.id.spinner_office_name);
        listOfDistrictOfficeAdaptor = new ArrayAdapter<DistrictCumStateOfficeKV>(this, R.layout.simple_spinner_lay, districtCumStateOfficeKV);
        listOfDistrictOfficeAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrictOffice.setAdapter(listOfDistrictOfficeAdaptor);

        listOfDistrictOffice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrictOffice.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictCumStateOfficeKV keyValueObject = (DistrictCumStateOfficeKV) parent.getSelectedItem();
                    singleton.office_code = keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


    /**
     * @ Text editing related
     */

    public void formTextEditing() {

        etPlaceOfOccurance = (AppCompatEditText) findViewById(R.id.et_place_of_occurace);
        txtILPlaceOfOccurance = (TextInputLayout) findViewById(R.id.til_place_of_occurance);
        etPlaceOfOccurance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etPlaceOfOccurance.getText().length() > 0) {
                    txtILPlaceOfOccurance.setError(null);
                    txtILPlaceOfOccurance.setErrorEnabled(false);
                }
            }
        });// end afterTextChanged

        etMobileNumber = (AppCompatEditText) findViewById(R.id.et_mobile_number);

        if (singleton.wspLoginConnect.MOBILE_2 != null)
            etMobileNumber.setText(singleton.wspLoginConnect.MOBILE_2);

        txtILMobileNumber = (TextInputLayout) findViewById(R.id.til_mobile_number);

        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etMobileNumber.getText().length() > 0) {
                    txtILMobileNumber.setError(null);
                    txtILMobileNumber.setErrorEnabled(false);
                }
            }
        });// end afterTextChanged

        etNationalityCode = (AppCompatEditText) findViewById(R.id.et_nationality_code);
        etNationalityCode.setText(singleton.wspLoginConnect.MOBILE_1);
        txtILNationalityCode = (TextInputLayout) findViewById(R.id.til_nationality_code);
        etNationalityCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etNationalityCode.getText().length() > 0) {
                    txtILNationalityCode.setError(null);
                    txtILNationalityCode.setErrorEnabled(false);
                }
            }
        });// end afterTextChanged

    }// end form text editing

    /**
     * @ validate
     */

    public boolean validateSubmission() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (viewSpinnerDistrict.getVisibility() == View.VISIBLE && (listOfDistrict.getSelectedItem().toString() == ""
                || listOfDistrict.getSelectedItem().toString() == this.txt_select)) {
            Utils.showToastMsg(this, "Select District", Toast.LENGTH_LONG);
        } else if (viewSpinnerPS.getVisibility() == View.VISIBLE && (listOfPoliceStation.getSelectedItem().toString() == ""
                || listOfPoliceStation.getSelectedItem().toString() == this.txt_select)) {
            Utils.showToastMsg(this, "Select Police Station", Toast.LENGTH_LONG);
        } else if (viewSpinnerOffice.getVisibility() == View.VISIBLE && (listOfDistrictOffice.getSelectedItem().toString() == ""
                || listOfDistrictOffice.getSelectedItem().toString() == this.txt_select)) {
            Utils.showToastMsg(this, "Select Office Name", Toast.LENGTH_LONG);
        } else if (etPlaceOfOccurance.getText().toString().trim().equals("")) {
            txtILPlaceOfOccurance.setError(getString(R.string.cannot_left_empty_msg));
        } else if (etNationalityCode.getText().toString().trim().equals("")) {
            txtILMobileNumber.setError(getString(R.string.cannot_left_empty_msg));
        } else if (etMobileNumber.getText().toString().trim().equals("")) {
            txtILMobileNumber.setError(getString(R.string.cannot_left_empty_msg));
        } else if (Utils.isValidPhoneNumber(etMobileNumber.getText().toString().trim()) == false) {
            txtILMobileNumber.setError(getString(R.string.validate_mobile_number));
        } else {

            // save the reference value
            this.singleton.place_of_occurance = etPlaceOfOccurance.getText().toString();
            this.singleton.mobile_1 = etNationalityCode.getText().toString();
            this.singleton.mobile_2 = etMobileNumber.getText().toString();
            this.singleton.know_your_ps = knowYourLocationYes.isChecked();
            this.singleton.know_your_district = knowDistrictYes.isChecked();

            return true;
        }

        return false;

    }// end validate


}// end main class submission activity
