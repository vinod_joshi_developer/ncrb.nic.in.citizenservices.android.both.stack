package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class MDistrictConnectParam {

    @Keep
    private int statusCode;
    @Keep
    private String status;
    @Keep
    private ResponseData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    @Keep
    public class ResponseData {

        @Keep
        private String message;
        @Keep
        public Data data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }
    }

    @Keep
    public class Data {

        @Keep
        private int application_id;

        public int getApplication_id() {
            return application_id;
        }
        public void setApplication_id(int application_id) {
            this.application_id = application_id;
        }
    }
}
