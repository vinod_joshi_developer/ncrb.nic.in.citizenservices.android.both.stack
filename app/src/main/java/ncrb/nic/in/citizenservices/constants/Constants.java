/**
 * 
 */
package ncrb.nic.in.citizenservices.constants;


/**
 *
 * Interface to hold common constants.
 * @author Plantron
 *
 */
public interface Constants {


    public static String API_BASE_URL = "http://192.168.2.2:8080/citizen";
    //public static String API_BASE_URL = "http://192.168.1.146/ncrb";
    //public static String API_BASE_URL = "https://www.jasonbase.com/things";
    //public static String API_BASE_URL = "https://api.myjson.com/bins";

    // declare the stack for the state
    public static final String STACK_OF_STATE = "JAVA"; // EITHER JAVA OR MICROSOFT

    public static String API_SIGNUP = "http://haryanapoliceonline.gov.in/SignUp.aspx?Mode=Q3JlYXRl";
    public static String API_FORGOT_PASSWORD = "http://haryanapoliceonline.gov.in/ForgotPassword.aspx";
    public static String STATECD = "27";
    public static String VAHAN_SAMANVAY = "https://play.google.com/store/apps/details?id=in.gov.ncrb.app.vahansamanvay&hl=en";
    public static String TALASH = "http://ncrb.gov.in/MissingUidb/talash.htm";

    //public static String API_BASE_URL = "http://10.0.3.2:8080/citizencomp";


    // list of services
    public static String mReqSedition = "mReqSedition";
    public static String mGetCitizenLoginDetailsConnect = "mGetCitizenLoginDetailsConnect";
    public static String mStateConnect = "mStateConnect";
    public static String mDistrictConnect = "mDistrictConnect";
    public static String mDistrictOfficeConnect = "mDistrictOfficeConnect";
    public static String mStateOfficeConnect = "mStateOfficeConnect";
    public static String mNatureOfComplaintConnect = "mNatureOfComplaintConnect";
    public static String mCountryConnect = "mCountryConnect";
    public static String mRegisterComplaintConnect = "mRegisterComplaintConnect";
    public static String mGetFIRStatus = "mGetFIRStatus";
    public static String mGetServiceStatus = "mGetServiceStatus";
    public static String mDistrictConnectWithoutPS = "mDistrictConnectWithoutPS";
    public static String mPoliceStationContact = "mPoliceStationContact";
    public static String mSaveCitizenTip = "mSaveCitizenTip";




   interface postParams {

       String COMPL_REG_NUM = "COMPL_REG_NUM";
       String STATE_CD = "STATE_CD";
       String DISTRICT_CD = "DISTRICT_CD";
       String PS_CD = "PS_CD";
       String COMPL_REG_DT = "COMPL_REG_DT";
       String INCIDENT_FROM_DT = "INCIDENT_FROM_DT";
       String INCIDENT_TO_DT = "INCIDENT_TO_DT";
       String FULL_NAME = "FULL_NAME";
       String COMPLAINT_REMARKS = "COMPLAINT_REMARKS";
       String COMPL_DESC = "COMPL_DESC";
       String IMEI_NO = "IMEI_NO";
       String DEV_INFO = "DEV_INFO";
       String MOBILE_1 = "MOBILE_1";
       String MOBILE_2 = "MOBILE_2";
       String INCIDENT_PLC = "INCIDENT_PLC";
       String LAT = "LAT";
       String LNG = "LNG";
       String COUNTRY_CD = "COUNTRY_CD";
       String ADDRESS_LINE_1 = "ADDRESS_LINE_1";
       String ADDRESS_LINE_2 = "ADDRESS_LINE_2";
       String ADDRESS_LINE_3 = "ADDRESS_LINE_3";
       String VILLAGE = "VILLAGE";
       String TEHSIL = "TEHSIL";
       String PINCODE = "PINCODE";
       String NATIONALITY_CD = "NATIONALITY_CD";
       String COMPLAINT_NATURE_CD = "COMPLAINT_NATURE_CD";


   }// end post params


}// end constants
