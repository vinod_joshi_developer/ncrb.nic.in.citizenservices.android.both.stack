package ncrb.nic.in.citizenservices.json.objects;

/**
 * Created by Lenovo on 0005 05-05-2016.
 */
public class Nationality {

    private String id;
    private String name;

    public Nationality(String id, String name) {
        this.id = id;
        this.name = name;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //to display object as a string in spinner
    public String toString() {
        return name;
    }

}// end  main class Nature Of Complaint
