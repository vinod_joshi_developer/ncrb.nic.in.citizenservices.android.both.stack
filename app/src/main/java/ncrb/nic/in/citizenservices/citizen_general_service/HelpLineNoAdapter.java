package ncrb.nic.in.citizenservices.citizen_general_service;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import ncrb.nic.in.citizenservices.R;

public class HelpLineNoAdapter extends RecyclerView.Adapter<HelpLineNoAdapter.MyViewHolder> {

    private List<HelpLineNoFormat> helpLineNoList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
        }
    }


    public HelpLineNoAdapter(List<HelpLineNoFormat> helpLineNoList) {
        this.helpLineNoList = helpLineNoList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_help_line_no_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        HelpLineNoFormat helpLineNoFormat = helpLineNoList.get(position);
        holder.title.setText(helpLineNoFormat.getTitle());
        holder.genre.setText(helpLineNoFormat.getGenre());
        holder.year.setText(helpLineNoFormat.getYear());
    }

    @Override
    public int getItemCount() {
        return helpLineNoList.size();
    }
}
