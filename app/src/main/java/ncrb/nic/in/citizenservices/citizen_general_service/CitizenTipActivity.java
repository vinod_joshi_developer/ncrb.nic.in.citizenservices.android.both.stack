package ncrb.nic.in.citizenservices.citizen_general_service;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.rey.material.widget.RadioButton;
import com.squareup.okhttp.OkHttpClient;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ncrb.nic.in.citizenservices.FrontGridViewImageTextActivity;
import ncrb.nic.in.citizenservices.R;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.DistrictCumStateOfficeKV;
import ncrb.nic.in.citizenservices.json.objects.DistrictKV;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.json.objects.PoliceStationKV;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPDistrictOfficeConnect;
import ncrb.nic.in.citizenservices.services_params.WSPSaveCitizenTip;
import ncrb.nic.in.citizenservices.services_params.WSPStateOfficeConnect;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


public class CitizenTipActivity extends AppCompatActivity {


    private RadioButton knowYourLocationYes;
    private RadioButton knowYourLocationNo;

    private RadioButton knowDistrictYes;
    private RadioButton knowDistrictNo;

    private LinearLayout linearLayoutDistrict;
    private LinearLayout viewSpinnerDistrict;
    private LinearLayout viewSpinnerPS;
    private LinearLayout viewSpinnerOffice;

    public ProgressDialog mProgressDialog;

    private Spinner listOfDistrict;
    private ArrayAdapter<DistrictKV> listOfDistrictAdaptor;


    private Spinner listOfPoliceStation;
    private ArrayAdapter<PoliceStationKV> listOfPoliceStationAdaptor;

    private Spinner listOfDistrictOffice;
    private ArrayAdapter<DistrictCumStateOfficeKV> listOfDistrictOfficeAdaptor;

    Singleton singleton = Singleton.getInstance();

    final String txt_select = "-- select --";


    private AppCompatEditText et_complaint_desc;
    private TextInputLayout txt_ip_complaint_desc;

    MCoCoRy mCoCoRy = new  MCoCoRy();



    /**
     * @
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citizen_tip);

        getSupportActionBar().hide();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        // district layout to hide or show or related actions
        linearLayoutDistrict = (LinearLayout) findViewById(R.id.lineLay_district);
        viewSpinnerDistrict = (LinearLayout) findViewById(R.id.view_district_spinner);
        viewSpinnerPS = (LinearLayout) findViewById(R.id.view_ps_spinner);
        viewSpinnerOffice = (LinearLayout) findViewById(R.id.view_office_spinner);


        settingCheckbox();

        formTextEditing();

        //Utils.showAlert(CitizenTipActivity.this, "Please complete the detail. Thank you for Sharing information with Police.");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.citizen_tip_submit);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean validated = false;

                try {

                    validated = validateSubmission();

                    if (validated) {
                        SaveCitizenTipWebService();
                    }

                } catch (Exception ex) {
                    Utils.printv("Exception " + ex.getMessage());
                }


            }
        });// end floating action button

        // get district with police station
        accessDistrictWithPS();

    }// end on create

    private void accessDistrictWithPS() {

        try {
            if (!Utils.isNetworkAvailable(CitizenTipActivity.this)) {
                Utils.showToastMsg(CitizenTipActivity.this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else GetDistrictCumPSWebService();
        } catch (Exception ex) {
            Utils.printv("Exception " + ex.getMessage());
        }

    }// end access district with ps

    /*
    * @on start
    * */
    @Override
    protected void onStart() {
        super.onStart();

    }// on start

    /**
     * @
     */

    public void onRadioClicked(View view) {

        // Is the view now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which checkbox was clicked
        switch (view.getId()) {

            case R.id.location_known_yes:

                if (checked) {
                    Utils.printv("location know yes");
                    linearLayoutDistrict.setVisibility(View.GONE);
                    viewSpinnerOffice.setVisibility(View.GONE);
                    viewSpinnerDistrict.setVisibility(View.VISIBLE);
                    viewSpinnerPS.setVisibility(View.VISIBLE);
                    resetSpinners();

                }
                break;
            case R.id.location_known_no:

                if (checked) {
                    Utils.printv("location know no");
                    linearLayoutDistrict.setVisibility(View.VISIBLE);
                    viewSpinnerPS.setVisibility(View.GONE);
                    viewSpinnerOffice.setVisibility(View.VISIBLE);
                    knowDistrictYes.setChecked(true);
                    resetSpinners();
                }
                break;
            case R.id.district_known_yes:

                if (checked) {
                    Utils.printv("district know yes");
                    viewSpinnerDistrict.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.district_known_no:

                if (checked) {
                    Utils.printv("district know no");
                    viewSpinnerDistrict.setVisibility(View.GONE);
                    try {
                        GetDistrictCumStateOfficeWebService();
                    } catch (Exception ex) {
                        Utils.printv("Exception " + ex.getMessage());
                    }

                }
                break;
        }

    }// end on Checkbox Clicked

    /**
     * @ checkboxes
     */
    public void resetSpinners() {

        listOfDistrict.setSelection(0);

    }// end reset spinners

    /**
     * @ checkboxes
     */

    public void settingCheckbox() {

        // check selection for location known or not
        knowYourLocationYes = (RadioButton) findViewById(R.id.location_known_yes);
        knowYourLocationNo = (RadioButton) findViewById(R.id.location_known_no);

        //  check selection for district known or not
        knowDistrictYes = (RadioButton) findViewById(R.id.district_known_yes);
        knowDistrictNo = (RadioButton) findViewById(R.id.district_known_no);


        // compound button for radio location known or not

        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    knowYourLocationYes.setChecked(knowYourLocationYes == buttonView);
                    knowYourLocationNo.setChecked(knowYourLocationNo == buttonView);

                }

            }

        };

        knowYourLocationYes.setOnCheckedChangeListener(listener);
        knowYourLocationNo.setOnCheckedChangeListener(listener);

        // compound button for radio district known or not

        CompoundButton.OnCheckedChangeListener listenerDistrictRadio = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    knowDistrictYes.setChecked(knowDistrictYes == buttonView);
                    knowDistrictNo.setChecked(knowDistrictNo == buttonView);

                }

            }

        };

        knowDistrictYes.setOnCheckedChangeListener(listenerDistrictRadio);
        knowDistrictNo.setOnCheckedChangeListener(listenerDistrictRadio);


        // select true as user know the police station
        knowYourLocationYes.setChecked(true);
        knowYourLocationNo.setChecked(false);

        // select true as user know the police station
        knowDistrictYes.setChecked(true);
        knowDistrictNo.setChecked(false);

        // initial hide the district option for user
        linearLayoutDistrict.setVisibility(View.GONE);
        viewSpinnerOffice.setVisibility(View.GONE);


    }// end setting location checkbox


    /**
     * @ get district and ps list
     */

    public void GetDistrictCumPSWebService() throws Exception {

        this.mProgressDialog.show();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("state_cd", Constants.STATECD);
            postParams.put("m_service", Constants.mDistrictConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();


        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mStateConnect", postParams);

        Utils.printv("mDistrictConnect json post params " + jsonPostParams.toString());


        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mDistrictConnect(jsonPostParams,
                new Callback<wspDistrictConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "connect to server district connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());


                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();


                    }// end failure

                    @Override
                    public void success(wspDistrictConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();


                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            Utils.printv("RESULT mDistrictConnect success");

                            singleton.wspDistrictConnect = result;

                            ArrayList<DistrictKV> districtKV = new ArrayList<>();
                            districtKV.add(new DistrictKV("0", txt_select));

                            for (wspDistrictConnect.DistrictLists list : result.getDistrictLists()) {
                                districtKV.add(new DistrictKV(list.DISTRICT_CD.toString(), list.DISTRICT.toString()));
                            }

                            if (!districtKV.isEmpty()) {
                                setDistrictSpinners(districtKV);
                            }

                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!! Server Error.", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end Register web service

    /**
     * @ spinners district
     */

    public void setDistrictSpinners(ArrayList<DistrictKV> districtKV) {

        listOfDistrict = (Spinner) findViewById(R.id.spinner_district);
        listOfDistrictAdaptor = new ArrayAdapter<DistrictKV>(this, R.layout.simple_spinner_lay, districtKV);
        listOfDistrictAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrict.setAdapter(listOfDistrictAdaptor);

        listOfDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrict.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictKV keyValueObject = (DistrictKV) parent.getSelectedItem();
                    singleton.district_cd = keyValueObject.getId();

                    // check if user don't know police station
                    if (knowYourLocationNo.isChecked()) {
                        GetDistrictCumStateOfficeWebService();
                    } else {
                        setPSSetting(keyValueObject.getId());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // auto complete district name
        autoSelectDistrict(districtKV);

    }// end spinners

    /**
     * @ auto select district
     */
    public void autoSelectDistrict(final ArrayList<DistrictKV> districtKV) {

        if(singleton.wspLoginConnect != null){

            int index = 0;
            for (DistrictKV district : districtKV) {
                if (singleton.wspLoginConnect.DISTRICT_CD.toString().equals(district.getId().toString())) {
                    listOfDistrict.setSelection(index);
                }
                index++;
            }//end foreach
        }


    }// end auto Select District

    /**
     * @ spinners
     */

    public void setPSSetting(String district_cd) {

        ArrayList<PoliceStationKV> policeStationKV = new ArrayList<>();
        policeStationKV.add(new PoliceStationKV("0", txt_select));

        for (wspDistrictConnect.DistrictLists list : singleton.wspDistrictConnect.getDistrictLists()) {
            if (district_cd.toString().equals(list.DISTRICT_CD)) {
                for (wspDistrictConnect.PoliceStationLists psList : list.policeStationsDataSet.getPoliceStationLists()) {
                    policeStationKV.add(new PoliceStationKV(psList.PS_CD.toString(), psList.PS.toString()));
                }
            }
        }

        if (!policeStationKV.isEmpty()) {
            setPoliceStationSpinners(policeStationKV);
        }

    }// end setPS Setting

    /**
     * @ spinners policeStation
     */

    public void setPoliceStationSpinners(ArrayList<PoliceStationKV> policeStationKV) {

        listOfPoliceStation = (Spinner) findViewById(R.id.spinner_police_station);
        listOfPoliceStationAdaptor = new ArrayAdapter<PoliceStationKV>(this, R.layout.simple_spinner_lay, policeStationKV);
        listOfPoliceStationAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfPoliceStation.setAdapter(listOfPoliceStationAdaptor);

        listOfPoliceStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfPoliceStation.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    PoliceStationKV keyValueObject = (PoliceStationKV) parent.getSelectedItem();
                    singleton.ps_code = keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


    /**
     * @ get district and ps list
     */

    public void GetDistrictCumStateOfficeWebService() throws Exception {

        this.mProgressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("district_cd", singleton.district_cd);
            postParams.put("state_cd", Constants.STATECD);

            if (knowDistrictYes.isChecked()) {
                postParams.put("m_service", Constants.mDistrictOfficeConnect);
            } else {
                postParams.put("m_service", Constants.mStateOfficeConnect);
            }

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mDistrictOfficeConnect", postParams);

        Utils.printv("district for office " + singleton.district_cd);


        if (knowDistrictYes.isChecked()) {
            districtWebCaller(restAdapter, jsonPostParams);
        } else {
            stateWebCaller(restAdapter, jsonPostParams);
        }

    }// end Register web service

    /**
     * @ district office web caller
     */
    public void districtWebCaller(RestAdapter restAdapter, JSONPostParams jsonPostParams) {


        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mDistrictOfficeConnect(jsonPostParams,
                new Callback<WSPDistrictOfficeConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server district office.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPDistrictOfficeConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ getting offices");


                            ArrayList<DistrictCumStateOfficeKV> districtCumStateOfficeKV = new ArrayList<>();
                            districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV("0", txt_select));

                            for (WSPDistrictOfficeConnect.DistrictOffices offices : result.getDistrictOffices()) {
                                districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV(offices.OFFICE_CD.toString(), offices.OFFICE_NAME.toString()));
                            }

                            if (!districtCumStateOfficeKV.isEmpty()) {
                                setDistrictCumStateOfficeSpinners(districtCumStateOfficeKV);
                            }


                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });


    }// end district web caller

    /**
     * @ state office web caller
     */
    public void stateWebCaller(RestAdapter restAdapter, JSONPostParams jsonPostParams) {

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mStateOfficeConnect(jsonPostParams,
                new Callback<WSPStateOfficeConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server state office connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPStateOfficeConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ getting offices");


                            ArrayList<DistrictCumStateOfficeKV> districtCumStateOfficeKV = new ArrayList<>();
                            districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV("0", txt_select));

                            for (WSPStateOfficeConnect.StateOffices offices : result.getStateOffices()) {
                                districtCumStateOfficeKV.add(new DistrictCumStateOfficeKV(offices.OFFICE_CD.toString(), offices.OFFICE_NAME.toString()));
                            }

                            if (!districtCumStateOfficeKV.isEmpty()) {
                                setDistrictCumStateOfficeSpinners(districtCumStateOfficeKV);
                            }


                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end state web caller

    /**
     * @ spinners district Office
     */

    public void setDistrictCumStateOfficeSpinners(ArrayList<DistrictCumStateOfficeKV> districtCumStateOfficeKV) {

        listOfDistrictOffice = (Spinner) findViewById(R.id.spinner_office_name);
        listOfDistrictOfficeAdaptor = new ArrayAdapter<DistrictCumStateOfficeKV>(this, R.layout.simple_spinner_lay, districtCumStateOfficeKV);
        listOfDistrictOfficeAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrictOffice.setAdapter(listOfDistrictOfficeAdaptor);

        listOfDistrictOffice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrictOffice.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictCumStateOfficeKV keyValueObject = (DistrictCumStateOfficeKV) parent.getSelectedItem();
                    singleton.office_code = keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


    /**
     * @ Text editing related
     */

    public void formTextEditing() {

        et_complaint_desc = (AppCompatEditText) findViewById(R.id.et_complainant_desc);
        if(singleton.complaint_desc != "" || singleton.complaint_desc != null){
            et_complaint_desc.setText(singleton.complaint_desc);
        }
        txt_ip_complaint_desc = (TextInputLayout) findViewById(R.id.til_complainant_desc);
        et_complaint_desc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_complaint_desc.getText().length() > 0) {
                    txt_ip_complaint_desc.setError(null);
                    txt_ip_complaint_desc.setErrorEnabled(false);
                }
            }
        });// end complainant name


    }// end form text editing

    /**
     * @ validate
     */

    public boolean validateSubmission() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (viewSpinnerDistrict.getVisibility() == View.VISIBLE && (listOfDistrict.getSelectedItem().toString() == ""
                || listOfDistrict.getSelectedItem().toString() == this.txt_select)) {
            Utils.showToastMsg(this, "Select District", Toast.LENGTH_LONG);
        } else if (viewSpinnerPS.getVisibility() == View.VISIBLE && (listOfPoliceStation.getSelectedItem().toString() == ""
                || listOfPoliceStation.getSelectedItem().toString() == this.txt_select)) {
            Utils.showToastMsg(this, "Select Police Station", Toast.LENGTH_LONG);
        } else if (viewSpinnerOffice.getVisibility() == View.VISIBLE && (listOfDistrictOffice.getSelectedItem().toString() == ""
                || listOfDistrictOffice.getSelectedItem().toString() == this.txt_select)) {
            Utils.showToastMsg(this, "Select Office Name", Toast.LENGTH_LONG);
        } else if (et_complaint_desc.getText().toString().trim().equals("")) {
            Utils.showToastMsg(this, "Please write tip detail.", Toast.LENGTH_LONG);
            txt_ip_complaint_desc.setError(getString(R.string.cannot_left_empty_msg));
        }

        else {

            this.singleton.know_your_ps = knowYourLocationYes.isChecked();
            this.singleton.know_your_district = knowDistrictYes.isChecked();

            return true;
        }

        return false;

    }// end validate

    /**
     * @ register complaint
     */

    public void SaveCitizenTipWebService() throws Exception {

        this.mProgressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res VISA -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();


        Map postParams = new HashMap();

        if(Constants.STACK_OF_STATE.equals("JAVA")) {
            paramsForJava(postParams);
        } else {
            paramsForMicroSoft(postParams);
        }


        Gson gsonObj = new Gson();
        String jsonStrToPostOnServer = gsonObj.toJson(postParams);

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            coco_seed = jsonStrToPostOnServer;

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        // create a new hash which you want to send on server
        Map postParamsSeed = new HashMap();

        postParamsSeed.put("seed", coco_seed_encd);

        Utils.printv("postParamsSeed "+postParamsSeed);

        JSONPostParams jsonPostParams = new JSONPostParams("mSaveCitizenTip", postParamsSeed);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        apiCaller.mSaveCitizenTip(jsonPostParams,
                new Callback<WSPSaveCitizenTip>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPSaveCitizenTip result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            singleton.success_message = result.MESSAGE;
                            CitizenTipActivity.this.showAlert(CitizenTipActivity.this,result.MESSAGE);

                        } else {
                            Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                        }
                    }// end success
                });
    }// end Register web service

    /**
     * @
     */

    public static void showAlert(final Context context , String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Intent mainIntent = new Intent(context, FrontGridViewImageTextActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(mainIntent);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }//  end show alert


    public Map paramsForJava(Map postParams) {

        Date date = new Date();
        String RecordCreatedOn = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String tipYear = new SimpleDateFormat("yyyy").format(date);

        postParams.put("CITIZEN_TIP_SRNO", "");
        postParams.put("LANG_CD", "99");
        postParams.put("TIP_YEAR", tipYear);
        postParams.put("TIP_SRNO", "");
        postParams.put("TIP_DT", RecordCreatedOn);
        postParams.put("ASSIGNED_STATE_CD", Constants.STATECD);
        postParams.put("ASSIGNED_DISTRICT_CD", singleton.know_your_district ? singleton.district_cd : "0");
        postParams.put("ASSIGNED_PS_CD", singleton.know_your_ps ? singleton.ps_code : "0");
        postParams.put("ASSIGNED_OFFICE_CD", singleton.know_your_ps ? "0" : singleton.office_code);
        postParams.put("INFORMATION_NATURE", "");
        postParams.put("INFORMATION_DESC", et_complaint_desc.getText().toString());
        postParams.put("INFORMATION_MODE_CD", "");
        postParams.put("ANY_DOC_UPLOADED", "N");
        postParams.put("IS_PS_KNOWN", singleton.know_your_ps ? "Y" : "N");
        postParams.put("IS_DISTRICT_KNOWN", singleton.know_your_district ? "Y" : "N");
        postParams.put("RECORD_STATUS", "C");
        postParams.put("RECORD_CREATED_ON", RecordCreatedOn);
        postParams.put("RECORD_CREATED_BY",  singleton.username);
        postParams.put("RECORD_UPDATED_ON", RecordCreatedOn);
        postParams.put("RECORD_UPDATED_BY", singleton.username);
        postParams.put("RECORD_SYNC_FROM", "");
        postParams.put("RECORD_SYNC_TO", "");
        postParams.put("RECORD_SYNC_ON", RecordCreatedOn);
        postParams.put("RECORD_UPDATED_FROM", "");
        postParams.put("DUMMY_COLUMN_1", "");
        postParams.put("DUMMY_COLUMN_2", "");
        postParams.put("ORIGINAL_RECORD", "");
        postParams.put("INFORMER_FULL_NAME", singleton.username);
        postParams.put("SUSPECT_FULL_NAME", "");
        postParams.put("IS_FIRST_SYNC_DONE", "");
        postParams.put("INFORMER_PERSON_CD", "");
        postParams.put("SUSPECT_PERSON_CD", "");
        postParams.put("m_service", Constants.mSaveCitizenTip);



        return  postParams;

    }// end params for java


    public Map paramsForMicroSoft(Map postParams) {



        postParams.put("int_LangCd","99");
        postParams.put("nvc_InformationDesc",et_complaint_desc.getText().toString());
        postParams.put("int_InformationModeCd","1");
        postParams.put("nvc_IsPsKnown", singleton.know_your_ps ? "Y" : "N");
        postParams.put("nvc_IsDistrictKnown",singleton.know_your_district ? "Y" : "N");
        postParams.put("nvc_RecordCreatedBy","Anonymous");
        postParams.put("int_AssignedStateCd",Constants.STATECD);
        postParams.put("int_AssignedDistrictCd",singleton.know_your_district ? singleton.district_cd : "0");
        postParams.put("int_AssignedPsCd",singleton.know_your_ps ? singleton.ps_code : "0");
        postParams.put("int_AssignedOfficeCd", singleton.know_your_ps ? "0" : singleton.office_code);
        postParams.put("nvc_AnyDocUploaded","s");
        postParams.put("nvc_RecordStatus","C");
        postParams.put("int_OriginalRecords","0");
        postParams.put("int_NatureOfInformation","0");
        postParams.put("XMLDOC_CITIZENTIP_UPLOAD_DOCUMENTS","");
        postParams.put("XMLDOC_CITIZENTIP_INFORMER_DETAILS","");
        postParams.put("XMLDOC_CITIZENTIP_INFORMER_ADDRESS","");
        postParams.put("XMLDOC_CITIZENTIP_SUSPECT_DETAILS","");
        postParams.put("XMLDOC_CITIZENTIP_SUSPECT_ADDRESS","");
        postParams.put("m_service", Constants.mSaveCitizenTip);



        return  postParams;

    }// end params for micro soft


}// end main class submission activity
