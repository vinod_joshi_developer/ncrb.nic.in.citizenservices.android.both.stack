package ncrb.nic.in.citizenservices;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPLoginConnect;
import ncrb.nic.in.citizenservices.services_params.WSPReqSedition;
import ncrb.nic.in.citizenservices.utils.AppLocationService;
import ncrb.nic.in.citizenservices.utils.AppPreferences;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ComplaintLoginActivity extends AppCompatActivity {

    private AppCompatEditText et_username;
    private TextInputLayout txt_username;

    private AppCompatEditText et_password;
    private TextInputLayout txt_password;

    Singleton singleton = Singleton.getInstance();
    public ProgressDialog mProgressDialog;
    AppLocationService appLocationService;
    AppPreferences objAppPreferences;
    Context objContext;
    String app_user="app_user"; String app_pass="app_pass";
    MCoCoRy mCoCoRy = new  MCoCoRy();

    public native String getCocoSeed0();

    public String coco_seed_encd ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_login);

        // core objects
        objContext = this;
        objAppPreferences = new AppPreferences(objContext);


        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        this.setToolbarAction();

        // set the editing
        formTextEditing();
        appLocationService = new AppLocationService(ComplaintLoginActivity.this);


        /**
         *
         * @logic: security audit suggest : not to store shared preference
         *
         *
         *
         * */

        //auto login - check if user already logged in
        if(!singleton.username.isEmpty()
                && !singleton.password.isEmpty()) {

            et_username.setText(singleton.username);
            et_password.setText(singleton.password);

            try {

                GetSeditionWebService();
            } catch (Exception ex) {
                Utils.printv("Exception " + ex.getMessage());
            }

        }// end if auto login


    }// end onCreate

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().getAttributes().windowAnimations = R.style.WindowAnimationTransition;

    }// end onStart

    /**
     * @ tool bar action
     */
    public void setToolbarAction() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }// end set toolbar action


    /**
     * @ Text editing related
     */

    public void formTextEditing() {

        et_username = (AppCompatEditText) findViewById(R.id.et_username);

        txt_username = (TextInputLayout) findViewById(R.id.til_username);

        et_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_username.getText().length() > 0) {
                    txt_username.setError(null);
                    txt_username.setErrorEnabled(false);
                }
            }
        });// end method

        // complainant address
        et_password = (AppCompatEditText) findViewById(R.id.et_password);
        txt_password = (TextInputLayout) findViewById(R.id.til_password);
        et_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_password.getText().length() > 0) {
                    txt_password.setError(null);
                    txt_password.setErrorEnabled(false);
                }
            }
        });// end method

    }// end form text editing

    /**
     * @ method
     */

    public void authenticateUser(View v) {

        if(Utils.isNetworkAvailable(this)) validate();
        else  Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);

    }//end auth user

    /**
     * @ method
     */

    public void signUpUser(View v) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.API_SIGNUP));
        startActivity(browserIntent);

    }//end auth user


    /**
     * @ method
     */

    public void forgotPassword(View v) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.API_FORGOT_PASSWORD));
        startActivity(browserIntent);

    }//end auth user


    /**
     * @ Validation
     */

    public void validate() {

        if (et_username.getText().toString().trim().equals("")) {

            txt_username.setError(getString(R.string.cannot_left_empty_msg));

        } else if (et_password.getText().toString().trim().equals("")) {

            txt_password.setError(getString(R.string.cannot_left_empty_msg));

        } else {

            try {

                // get the sedition detail from server, if any one trying for sedition
                GetSeditionWebService();

            } catch (Exception ex) {
                Utils.printv("Exception " + ex.getMessage());
            }


        }// end else if else validation loop

    }// end validate


    /**
     * @ get auth service
     */

    public void GetAuthDetailWebService() throws Exception {

        this.mProgressDialog.show();

        // keep the user and pass for further reference
        singleton.username = et_username.getText().toString().trim();
        singleton.password = et_password.getText().toString().trim();


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("username", et_username.getText().toString().trim());
            postParams.put("password", et_password.getText().toString().trim());
            postParams.put("sedition", singleton.sedition_token);
            postParams.put("m_service", Constants.mGetCitizenLoginDetailsConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("LOGIN", postParams);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        apiCaller.mGetCitizenLoginDetailsConnect(jsonPostParams,
                new Callback<WSPLoginConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPLoginConnect result, Response response) {


                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            Utils.printv("RESULT VJ getting offices");

                            // don't store username and password in shared preference for security reason

                            try {

                                Gson gson = new Gson();

                                String unSeed = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), result.seed, "DECODE");

                                singleton.wspLoginConnect = gson.fromJson(unSeed, WSPLoginConnect.class);

                                if (mProgressDialog != null && mProgressDialog.isShowing())
                                    mProgressDialog.dismiss();

                                Intent mainIntent = new Intent(ComplaintLoginActivity.this, DashboardActivity.class);
                                ComplaintLoginActivity.this.startActivity(mainIntent);
                                ComplaintLoginActivity.this.finish();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {

                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();

                            Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                        }
                    }// end success
                });


    }// end auth web service

    /**
     * @ very important logic : call the server to get token.
     *  token attached with login and validated at server end
     *  token logic code and decode written on server side only
     * @
     */

    public void GetSeditionWebService() throws Exception {

        this.mProgressDialog.show();


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("m_service", Constants.mReqSedition);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("LOGIN", postParams);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        apiCaller.mReqSedition(jsonPostParams,
                new Callback<WSPReqSedition>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPReqSedition result, Response response) {

                        seditionResponse(result, response);

                    }// end success
                });


    }// end auth web service

    /*
    *
    *
    *
    * */

    public void seditionResponse(WSPReqSedition result, Response response) {

        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();

        Utils.printv("RESULT status " + result.getSTATUS_CODE());

        if (result.getSTATUS_CODE().toString().equals("200")) {

            Utils.printv("RESULT for seditionResponse");

            // don't store username and password in shared preference for security reason

            try {

                singleton.sedition_token = result.SEDITION;

                GetAuthDetailWebService();

            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
            }

        } else {
            Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
        }

    }// end seqdition response



}// end main class