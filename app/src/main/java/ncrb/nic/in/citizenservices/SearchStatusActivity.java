package ncrb.nic.in.citizenservices;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.json.objects.KeyValueObject;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPSearchRecordConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.QueryHandler;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchStatusActivity extends AppCompatActivity {

    private AppCompatEditText et_complainant_name;
    private TextInputLayout txt_ip_complainant_name;

    private AppCompatEditText et_complaint_no;
    private TextInputLayout txt_ip_complaint_no;

    Singleton singleton = Singleton.getInstance();

    private Spinner listOfTypeOfService;
    private ArrayAdapter<KeyValueObject> listOfTypeOfServiceAdaptor;

    private Spinner listOfYear;
    private ArrayAdapter<KeyValueObject> listOfYearAdaptor;

    QueryHandler queryHandler;

    private LinearLayout linearLayoutSearchRecord;
    private TextView txtViewSearchRecord;
    public ProgressDialog mProgressDialog;

    // search record
    String m_lang_cd = "99";
    String o_year;
    String m_service_id_no;
    String m_service_type_cd;
    MCoCoRy mCoCoRy = new  MCoCoRy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_status);
        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        linearLayoutSearchRecord = (LinearLayout) findViewById(R.id.linelay_search_status);
        // don't show the search record initially
        linearLayoutSearchRecord.setVisibility(View.GONE);

        txtViewSearchRecord = (TextView) findViewById(R.id.txt_record_status);

        // setting up tool bar action
        this.setToolbarAction();

        queryHandler = new QueryHandler(this);

        // set and get the text editor and related values
        this.formTextEditing();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validate();
            }
        });

    }// end  on Create

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().getAttributes().windowAnimations = R.style.WindowAnimationTransition;

        // load the nature of complaint
        loadTypeOfServiceSpinner();

        // load nationality
        loadYears();

    }// end onStart

    /**
     * @ tool bar action
     */
    public void setToolbarAction() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.search_status_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        //toolbar.setLogo(R.mipmap.ic_launcher);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }// end set toolbar action

    /**
     * @ Text editing related
     */

    public void formTextEditing() {

        et_complainant_name = (AppCompatEditText) findViewById(R.id.et_complainant_name);
        txt_ip_complainant_name = (TextInputLayout) findViewById(R.id.til_complainant_name);
        et_complainant_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_complainant_name.getText().length() > 0) {
                    txt_ip_complainant_name.setError(null);
                    txt_ip_complainant_name.setErrorEnabled(false);
                }
            }
        });// end


        et_complaint_no = (AppCompatEditText) findViewById(R.id.et_complaint_no);
        txt_ip_complaint_no = (TextInputLayout) findViewById(R.id.til_complaint_no);
        et_complaint_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_complaint_no.getText().length() > 0) {
                    txt_ip_complaint_no.setError(null);
                    txt_ip_complaint_no.setErrorEnabled(false);
                }
            }
        });// end

    }// end form text editing


    /**
     * load data from SQLite
     */
    public void loadTypeOfServiceSpinner() {

        ArrayList<KeyValueObject> keyValueObjects = new ArrayList<>();
        keyValueObjects.add(new KeyValueObject("0", queryHandler.select_text));

        if(Constants.STACK_OF_STATE.equals("JAVA")) {
            keyValueObjects.add(new KeyValueObject("14", "Complaint"));
        } else {
            keyValueObjects.add(new KeyValueObject("1", "Complaint"));
        }

        //keyValueObjects.add(new KeyValueObject("2", "FIR"));
        //keyValueObjects.add(new KeyValueObject("3", "Service Request"));

        if (!keyValueObjects.isEmpty()) {
            this.setTypeOfServiceSpinner(keyValueObjects);
        }

    }// end load SQLite data

    /**
     * @ spinners
     */

    public void setTypeOfServiceSpinner(ArrayList<KeyValueObject> keyValueObject) {

        listOfTypeOfService = (Spinner) findViewById(R.id.spinner_type_of_service);
        listOfTypeOfServiceAdaptor = new ArrayAdapter<KeyValueObject>(this, R.layout.simple_spinner_lay, keyValueObject);
        listOfTypeOfServiceAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfTypeOfService.setAdapter(listOfTypeOfServiceAdaptor);

        listOfTypeOfService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueObject keyValueObject = (KeyValueObject) parent.getSelectedItem();
                m_service_type_cd = keyValueObject.getId();
                //Utils.showToastMsg(getApplicationContext(), natureOfComplaint.getName(), Toast.LENGTH_LONG);

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners

    /**
     * load data from SQLite
     */
    public void loadYears() {

        ArrayList<KeyValueObject> keyValueObjects = new ArrayList<>();
        keyValueObjects.add(new KeyValueObject("0", queryHandler.select_text));
        int year = Calendar.getInstance().get(Calendar.YEAR);
        // @ '2011' can be updated as required
        for (int i = 2011; i <= year; i++) {
            keyValueObjects.add(new KeyValueObject("" + i, "" + i));
        }

        if (!keyValueObjects.isEmpty()) {
            this.setYearSpinner(keyValueObjects);
        }
    }// end load nationality

    /**
     * @ spinners
     */

    public void setYearSpinner(ArrayList<KeyValueObject> keyValueObject) {

        listOfYear = (Spinner) findViewById(R.id.spinner_year);
        listOfYearAdaptor = new ArrayAdapter<KeyValueObject>(this, R.layout.simple_spinner_lay, keyValueObject);
        listOfYearAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfYear.setAdapter(listOfYearAdaptor);
        listOfYear.setSelection(Utils.getSpinnerIndex(listOfYear, "INDIA"));

        listOfYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                KeyValueObject keyValueObject = (KeyValueObject) parent.getSelectedItem();
                o_year = keyValueObject.getId();
                //Utils.showToastMsg(getApplicationContext(), nationality.getName(), Toast.LENGTH_LONG);

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners

    /**
     * @ Validation
     */

    public void validate() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (this.listOfTypeOfService.getSelectedItem().toString() == ""
                || listOfTypeOfService.getSelectedItem().toString() == queryHandler.select_text) {
            Utils.showToastMsg(getApplicationContext(), queryHandler.select_type_of_service, Toast.LENGTH_LONG);
        } else if (et_complainant_name.getText().toString().trim().equals("")) {
            txt_ip_complainant_name.setError(getString(R.string.cannot_left_empty_msg));
        } else if (et_complainant_name.getText().toString().trim().length() > 255) {
            txt_ip_complainant_name.setError(getString(R.string.number_of_chars));
        } else if (et_complaint_no.getText().toString().trim().equals("")) {
            txt_ip_complaint_no.setError(getString(R.string.cannot_left_empty_msg));
        } else {

            try {
                GetSearchRecordSWebService();
            } catch (Exception ex) {
                Utils.printv("Exception while calling get search record web service");
            }

        }

    }// end validate


    /**
     * @ get district and ps list
     */

    public void GetSearchRecordSWebService() throws Exception {

        this.mProgressDialog.show();

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map Message = new HashMap();
            Message.put("m_login_id", singleton.username);
            Message.put("m_lang_cd", m_lang_cd);
            Message.put("o_year", o_year);
            Message.put("m_service_id_no", et_complaint_no.getText().toString().trim());
            Message.put("m_service_type_cd", m_service_type_cd);
            Message.put("m_first_name", et_complainant_name.getText().toString().trim());

            Message.put("m_service", Constants.mGetServiceStatus);

            if(Constants.STACK_OF_STATE.equals("JAVA")) {
                Message.put("u_applicant_num", singleton.wspLoginConnect.USER_APPLICANT_NUM);
            }

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(Message);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mGetServiceStatus", postParams);

        apiCaller.mGetServiceStatus(jsonPostParams,
                new Callback<WSPSearchRecordConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        System.out.println("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPSearchRecordConnect result, Response response) {

                        // remove keyboard on load
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        System.out.println("RESULT status " + result.getSTATUS_CODE());

                        // show results to user
                        linearLayoutSearchRecord.setVisibility(View.VISIBLE);

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            System.out.println("RESULT VJ success");
                            txtViewSearchRecord.setText(result.RECORD_STATUS);
                        } else {
                            txtViewSearchRecord.setText(result.MESSAGE);
                        }
                    }// end success
                });

    }// end Register web service

}// end main class
