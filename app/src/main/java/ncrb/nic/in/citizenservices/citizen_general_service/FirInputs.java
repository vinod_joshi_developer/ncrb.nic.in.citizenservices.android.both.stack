package ncrb.nic.in.citizenservices.citizen_general_service;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import org.ksoap2.serialization.SoapPrimitive;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ncrb.nic.in.citizenservices.R;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.DistrictKV;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.json.objects.PoliceStationKV;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPGetFIR;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class FirInputs extends AppCompatActivity {


    //todo delete me
    /**
     *
     * find the FIR which is published and user can see
     *
     *
     select fr.FIR_SRNO, fr.REG_YEAR, ps.PS,ps.PS_CD, ps.DISTRICT_CD, md.DISTRICT
     from t_fir_registration fr, m_police_station ps, m_district md
     where fr.STATE_CD=11 and fr.IS_PUBLISHED='Y'
     and ps.PS_CD=fr.PS_CD
     and md.DISTRICT_CD = ps.DISTRICT_CD

     * */

    //private static final String URL= "http://164.100.181.132/FIR_VIEW_WEB_SERVICE/GetStatusFirWebService.asmx";
    //private static final String URL = "http://10.23.72.58:5152/GetStatusFirWebService.asmx";
    private static final String URL = Constants.API_BASE_URL;

    //private static final String URL="http://10.23.72.161:8080/citizen/ViewFIR?q={\"firregnum\":\"13258068160071\",\"district_cd\":\"13258\",\"ps_cd\":\"1325806  8\",\"year\":\"2016\"}";

    private static final String NAMESPASE="http://tempuri.org/";
    private static final String SOAP_ACTION= "http://tempuri.org/GetStatusFIR";
    private static final String METHOD_NAME="GetStatusFIR";

    AlertDialog alertDialog;
    Button submitbtn;
    EditText EditFirNo;

    Spinner yearSpn;

    ArrayList<String> YearValue=new ArrayList<>();
    int subDistrictCD,subPsCD,thisYear;
    private String TAG = "TEST";
    SoapPrimitive s;
    Object response;
    File filePath;
    String nameapp=null;
    boolean ispdfexist;
    String district=null,ps=null,year,firNO=null,result=null;
    String msg= "";

    public ProgressDialog mProgressDialog;
    Singleton singleton = Singleton.getInstance();

    private Spinner listOfDistrict;
    private ArrayAdapter<DistrictKV> listOfDistrictAdaptor;


    private Spinner listOfPoliceStation;
    private ArrayAdapter<PoliceStationKV> listOfPoliceStationAdaptor;

    final String txt_select = "-- select --";

    String district_code = "";
    String police_station_code = "";
    MCoCoRy mCoCoRy = new  MCoCoRy();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fir_inputs);


        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        submitbtn=(Button)findViewById(R.id.submit);
        EditFirNo=(EditText)findViewById(R.id.editFIR);


        yearSpn=(Spinner)findViewById(R.id.dateinp);


        try {
            if (!Utils.isNetworkAvailable(FirInputs.this)) {
                Utils.showToastMsg(FirInputs.this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else GetDistrictCumPSWebService();
        } catch (Exception ex) {
            Utils.printv("Exception " + ex.getMessage());
        }



        thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 2016; i<=thisYear; i++) {
            YearValue.add(Integer.toString(i));
        }

        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(this, R.layout.simple_spinner_lay, YearValue);
        yearAdapter.setDropDownViewResource(R.layout.spinnerdropdown);
        yearSpn.setAdapter(yearAdapter);



        submitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager connMgr = (ConnectivityManager)
                        getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {

                    // fetch data
                    year=yearSpn.getSelectedItem().toString();
                    firNO=EditFirNo.getText().toString();

                    subDistrictCD = Integer.parseInt(FirInputs.this.district_code);

                    subPsCD = Integer.parseInt(FirInputs.this.police_station_code);

                    if(firNO.equals(null)||firNO.equals("")){

                        Utils.showAlert(getApplicationContext(),"Please enter FIR NO..");

                    } else if(district_code.equals("")|| district_code.equals("0")){

                        Toast.makeText(FirInputs.this, "Please select district.", Toast.LENGTH_LONG).show();

                    } else if(police_station_code.equals("")|| police_station_code.equals("0")){

                        Toast.makeText(FirInputs.this, "Please select police station.", Toast.LENGTH_LONG).show();

                    } else {

                        String appendstring=null;

                        if(networkInfo != null && networkInfo.isConnected()) {

                            // @ removing old logic , the name of pdf file

                            if(firNO.length()==4) {
                                nameapp = firNO + district_code + police_station_code;
                            }else if(firNO.length()==3){
                                appendstring="0"+firNO;
                                nameapp = appendstring + district_code + police_station_code;
                            }else if(firNO.length()==2){
                                appendstring="0"+"0"+firNO;
                                nameapp = appendstring + district_code + police_station_code;
                            }else  if(firNO.length()==1){
                                appendstring="0"+"0"+"0"+firNO;
                                nameapp = appendstring + district_code + police_station_code;
                            }

                            Utils.printv("PDF Name "+ nameapp + " FIR num "+firNO);

                            try {

                                GetFIRStatusDetail();

                            } catch (Exception e){
                                Utils.printv("Error while parsing data.!! Please try later.");
                            }

                            //AsyntaskCall asyncalltest = new AsyntaskCall(FirInputs.this);
                            //asyncalltest.execute();

                            //showpdf();

                        }else{

                            Utils.showAlert(getApplicationContext(),"Network Connection Failed..");

                        }
                    }

                }else {

                    Utils.showAlert(getApplicationContext(),"Network Connection Failed..");

                }
            }
        });
    }// end on create

    //todo - for demo purpose only
    private void showpdf() {


        if(firNO.equals(null)||firNO.equals("")){

            Toast.makeText(this, "Please select FIR.", Toast.LENGTH_LONG).show();

        }else if(district_code.equals("") || district_code.equals("0")){

            Toast.makeText(this, "Please select district.", Toast.LENGTH_LONG).show();

        } else if(police_station_code.equals("") || police_station_code.equals("0")){

            Toast.makeText(this, "Please select police station.", Toast.LENGTH_LONG).show();

        } else {

            Utils.printv("district and police code "+district_code + " ps code " + police_station_code);


            try {

                // todo for demo purpose only
                String url = "https://drive.google.com/file/d/0B3c7xNH3SdNESlZ0cFJmM3VWLUU/view?usp=sharing";

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);


            }
            catch (ActivityNotFoundException e) {
                Toast.makeText(this, "No application available to view PDF", Toast.LENGTH_LONG).show();
            }


        }


    }// end show pdf



    /**
     * @ get auth service
     */

    public void GetFIRStatusDetail() throws Exception {

        this.mProgressDialog.show();

        Gson gsonObj = new Gson();

        Map postParamsFIR = new HashMap();
        postParamsFIR.put("firregnum", firNO);
        postParamsFIR.put("district_cd", subDistrictCD);
        postParamsFIR.put("ps_cd", subPsCD);
        postParamsFIR.put("year", year);
        postParamsFIR.put("m_service", Constants.mGetFIRStatus);

        String jsonStrSeed = gsonObj.toJson(postParamsFIR);

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            coco_seed = jsonStrSeed;

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        // create a new hash which you want to send on server
        Map postParamsSeed = new HashMap();

        postParamsSeed.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParamsSeed);

        JSONPostParams jsonPostParams = new JSONPostParams("LOGIN", postParamsSeed);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        apiCaller.mGetFIRStatus(jsonPostParams,
                new Callback<WSPGetFIR>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPGetFIR result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            Utils.printv("RESULT VJ getting offices");

                            // don't store username and password in shared preference for security reason

                            try {

                                Utils.printv("FIR base code" +  result.BASE64_BINARY);

                                showFIRtoUser(result.BASE64_BINARY);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                        }
                    }// end success
                });


    }// end auth web service

    /**
     * @ get district and ps list
     */

    public void showFIRtoUser(String result) throws Exception {

        if(result==null){

            Utils.showAlert(getApplicationContext(),"Error while trying to fetch your FIR request." );

        }

        else {

            EditFirNo.setText("");

            Log.i(TAG, "onPostExecute");

            try {
                if (result.equals("ZWZk")) {

                    Utils.showAlert(getApplicationContext(), "FIR Not Found Or FIR is restricted to view as per supreme court order.");


                } else if (result.equals("YWJjZA==")) {

                    Utils.showAlert(getApplicationContext(), "FIR Not Found Or FIR is restricted to view as per supreme court order.");


                } else if (ispdfexist == true) {


                    Utils.showAlert(getApplicationContext(), "FIR file already exist. Please check your download folder.");

                } else {

                    {

                        byte[] pdfAsBytes = Base64.decode(result.toString(), 0);

                        Log.i(TAG, "sendEnvelope");

                        File direct = new File(Environment.getExternalStorageDirectory()+"/FIR Folder");

                        if(!direct.exists()){

                            direct.mkdir();
                            filePath = new File(direct, File.separator + "FIR_no_"+nameapp+".pdf");
                            System.out.println("filepath "+filePath);
                            if(filePath.isFile()==true){
                                Log.v("MSG", "File is already exist");
                                ispdfexist=filePath.isFile();
                            }else{
                                try {
                                    filePath.createNewFile();
                                    FileOutputStream fo = new FileOutputStream(filePath.getPath());
                                    fo.write(pdfAsBytes);
                                    fo.flush();
                                    fo.close();
                                } catch (Exception exc) {
                                    exc.printStackTrace();
                                }
                            }

                        } else{
                            filePath = new File(direct, File.separator + "FIR_no_"+nameapp + ".pdf");
                            System.out.println("filepath "+filePath);
                            if(filePath.isFile()==true){
                                Log.v("MSG", "File is already exist");
                                ispdfexist=filePath.isFile();
                            }else{
                                try {
                                    filePath.createNewFile();
                                    FileOutputStream fo = new FileOutputStream(filePath.getPath());
                                    fo.write(pdfAsBytes);
                                    fo.flush();
                                    fo.close();
                                } catch (Exception exc) {
                                    exc.printStackTrace();
                                }
                            }
                        }
                    }

                    alertDialog = new AlertDialog.Builder(FirInputs.this).create();
                    alertDialog.setTitle("");
                    alertDialog.setMessage("FIR in PDF has been downloaded in Device storage FIR Folder");
                    alertDialog.setIcon(R.drawable.ncrb_logo_trans_55);
                    alertDialog.setButton("OK..", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            if (filePath.exists()) {
                                Uri filepath = Uri.fromFile(filePath);
                                Intent intent = new Intent(Intent.ACTION_VIEW);
                                intent.setDataAndType(filepath, "application/pdf");
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                try {
                                    startActivity(intent);
                                } catch (Exception e) {
                                    Log.e("error", "" + e);
                                }

                            } else {
                                Log.e("else", "There is some problem");
                            }
                        }
                    });
                    alertDialog.show();
                }
            } catch (Exception e) {

                alertDialog = new AlertDialog.Builder(FirInputs.this).create();
                alertDialog.setTitle("Attention...");
                alertDialog.setMessage(e.toString());
                alertDialog.setIcon(R.drawable.ncrb_logo_trans_55);
                alertDialog.setButton("OK..", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // here you can add functions
                    }
                });
                alertDialog.show();
            }

        }// end else

    }// end show fir to user

    /**
     * @ get district and ps list
     */

    public void GetDistrictCumPSWebService() throws Exception {

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("state_cd", Constants.STATECD);
            postParams.put("m_service", Constants.mDistrictConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd  = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");

        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        this.mProgressDialog.show();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mStateConnect", postParams);

        Utils.printv("mDistrictConnect json post params " + jsonPostParams.toString());

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mDistrictConnect(jsonPostParams,
                new Callback<wspDistrictConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "connect to server district connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());


                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();


                    }// end failure

                    @Override
                    public void success(wspDistrictConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT VJ " + result.getDistrictLists());
                        Utils.printv("RESULT status " + result.getSTATUS_CODE());


                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ success");

                            singleton.wspDistrictConnect = result;

                            ArrayList<DistrictKV> districtKV = new ArrayList<>();
                            districtKV.add(new DistrictKV("0", txt_select));

                            for (wspDistrictConnect.DistrictLists list : result.getDistrictLists()) {
                                districtKV.add(new DistrictKV(list.DISTRICT_CD.toString(), list.DISTRICT.toString()));
                            }

                            if (!districtKV.isEmpty()) {
                                setDistrictSpinners(districtKV);
                            }

                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end Register web service

    /**
     * @ spinners district
     */

    public void setDistrictSpinners(ArrayList<DistrictKV> districtKV) {

        listOfDistrict = (Spinner) findViewById(R.id.spinner_district);
        listOfDistrictAdaptor = new ArrayAdapter<DistrictKV>(this, R.layout.simple_spinner_lay, districtKV);
        listOfDistrictAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfDistrict.setAdapter(listOfDistrictAdaptor);

        listOfDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfDistrict.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    DistrictKV keyValueObject = (DistrictKV) parent.getSelectedItem();
                    //singleton.district_cd = keyValueObject.getId();
                    FirInputs.this.district_code =  keyValueObject.getId();

                    setPSSetting(keyValueObject.getId());

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


    /**
     * @ spinners
     */

    public void setPSSetting(String district_cd) {

        ArrayList<PoliceStationKV> policeStationKV = new ArrayList<>();
        policeStationKV.add(new PoliceStationKV("0", txt_select));

        for (wspDistrictConnect.DistrictLists list : singleton.wspDistrictConnect.getDistrictLists()) {
            if (district_cd.toString().equals(list.DISTRICT_CD)) {
                for (wspDistrictConnect.PoliceStationLists psList : list.policeStationsDataSet.getPoliceStationLists()) {
                    policeStationKV.add(new PoliceStationKV(psList.PS_CD.toString(), psList.PS.toString()));
                }
            }
        }

        if (!policeStationKV.isEmpty()) {
            setPoliceStationSpinners(policeStationKV);
        }

    }// end setPS Setting

    /**
     * @ spinners policeStation
     */

    public void setPoliceStationSpinners(ArrayList<PoliceStationKV> policeStationKV) {

        listOfPoliceStation = (Spinner) findViewById(R.id.spinner_police_station);
        listOfPoliceStationAdaptor = new ArrayAdapter<PoliceStationKV>(this, R.layout.simple_spinner_lay, policeStationKV);
        listOfPoliceStationAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfPoliceStation.setAdapter(listOfPoliceStationAdaptor);

        listOfPoliceStation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Utils.showToastMsg(getActivity(), (String) listOfPoliceStation.getSelectedItem(), Toast.LENGTH_LONG);

                try {

                    PoliceStationKV keyValueObject = (PoliceStationKV) parent.getSelectedItem();
                    //singleton.ps_code = keyValueObject.getId();
                    FirInputs.this.police_station_code =  keyValueObject.getId();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }// end spinners


}// end main class

