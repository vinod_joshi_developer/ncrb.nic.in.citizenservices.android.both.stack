package ncrb.nic.in.citizenservices;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.NatureOfComplaint;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPNatureOfComplaintConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ComplaintSrcSecondActivity extends AppCompatActivity implements
        View.OnClickListener {

    private Button dateTimeOfIncidentFrom;
    private Button dateTimeOfIncidentTo;
    private String timeStrByTimeDialog;
    private String dateStrByDateDialog;
    private int counter = 0;
    private String selectedDateValue;

    private AppCompatEditText et_complainant_name;
    private TextInputLayout txt_ip_complainant_name;

    private String final_incident_from = "";
    private String final_incident_to = "";

    Singleton singleton = Singleton.getInstance();

    private Spinner listOfNatureOfComplaint;
    private ArrayAdapter<NatureOfComplaint> listOfNatureOfComplaintAdaptor;

    public ProgressDialog mProgressDialog;
    final String txt_select = "-- select --";
    private  int tm_get_nature_of_complaint_web_service =1;

    MCoCoRy mCoCoRy = new  MCoCoRy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_src_second);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");


        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // setting up tool bar action
        this.setToolbarAction();

        // setting up date and time values
        this.setDateAndTimeAction();

        // set and get the text editor and related values
        this.formTextEditing();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });


    }// end  on Create

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().getAttributes().windowAnimations = R.style.WindowAnimationTransition;

        try {

            if (!Utils.isNetworkAvailable(this)) {
                Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else GetNatureOfComplaintWebService();

        } catch (Exception ex) {
            Utils.printv("Exception : " + ex.getMessage());
        }


    }// end onStart

    /**
     * @ option menu
     **/
    @Override
    public void onBackPressed() {

        this.singleton.incident_from = final_incident_from;
        this.singleton.incident_to = final_incident_to;
        this.singleton.complainant_name = et_complainant_name.getText().toString();

        super.onBackPressed();

    }// end on back pressed method

    /**
     * @ tool bar action
     */
    public void setToolbarAction() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        //toolbar.setLogo(R.mipmap.ic_launcher);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }// end set toolbar action


    /**
     * @ set the date and time action
     */
    public void setDateAndTimeAction() {

        dateTimeOfIncidentFrom = (Button) findViewById(R.id.btn_incident_from);
        dateTimeOfIncidentFrom.setOnClickListener(this);

        dateTimeOfIncidentTo = (Button) findViewById(R.id.btn_incident_to);
        dateTimeOfIncidentTo.setOnClickListener(this);


    }// end set date and time action

    /**
     * @ show date dialog
     */

    @Override
    public void onClick(View v) {

        if (v == dateTimeOfIncidentFrom) {
            this.showDateDialog("INCIDENT_FROM");
        }// end if date picker chooser

        if (v == dateTimeOfIncidentTo) {
            this.showDateDialog("INCIDENT_TO");
        }// end if date picker chooser


    }// end click

    /**
     * @ show date dialog
     */

    public void showDateDialog(String incidentType) {

        dateStrByDateDialog = incidentType;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        if (dateStrByDateDialog == "INCIDENT_FROM") {

                            // @Logic : the onDateSet called twice, make sure to call date only once
                            counter++;

                            if (counter > 0) {
                                showTimeDialog(dateStrByDateDialog, dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                counter = 0;
                            }

                        }
                        if (dateStrByDateDialog == "INCIDENT_TO") {
                            // @Logic : the onDateSet called twice, make sure to call date only once
                            counter++;

                            if (counter > 0) {
                                showTimeDialog(dateStrByDateDialog, dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                counter = 0;
                            }

                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }// end show date dialog

    /**
     * @ show time dialog
     */

    public void showTimeDialog(String incidentType, String dateValue) {

        timeStrByTimeDialog = incidentType;
        selectedDateValue = dateValue;

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        // @Logic : Convert 24 Hours format into 12 hours format
                        String am_pm = "";

                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);

                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";

                        String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";

                        String selection = selectedDateValue + " " + strHrsToShow + ":" + String.format("%02d", minute) + " " + am_pm;

                        if (dateStrByDateDialog == "INCIDENT_FROM") {
                            dateTimeOfIncidentFrom.setText(selection);
                            final_incident_from = selection;
                        }
                        if (dateStrByDateDialog == "INCIDENT_TO") {
                            dateTimeOfIncidentTo.setText(selection);
                            final_incident_to = selection;
                        }

                    }// end
                }, mHour, mMinute, false);
        timePickerDialog.show();

    }// end show time dialog

    /**
     * @ Text editing related
     */

    public void formTextEditing() {


        if (singleton.incident_from != null) {
            dateTimeOfIncidentFrom.setText(singleton.incident_from);
            final_incident_from = singleton.incident_from;
        }
        if (singleton.incident_to != null) {
            dateTimeOfIncidentTo.setText(singleton.incident_to);
            final_incident_to = singleton.incident_to;
        }

        et_complainant_name = (AppCompatEditText) findViewById(R.id.et_complainant_name);
          /*@Logic: auto fill user address info, if user pressed back, save current value and display
            same value again*/
        if (singleton.wspLoginConnect.FIRST_NAME != null)
            et_complainant_name.setText(singleton.wspLoginConnect.FIRST_NAME);
        if(this.singleton.complainant_name != null)
            et_complainant_name.setText(singleton.complainant_name);
        txt_ip_complainant_name = (TextInputLayout) findViewById(R.id.til_complainant_name);
        et_complainant_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_complainant_name.getText().length() > 0) {
                    txt_ip_complainant_name.setError(null);
                    txt_ip_complainant_name.setErrorEnabled(false);
                }
            }
        });// end complainant name

    }// end form text editing

    /**
     * @ spinners
     */

    public void setNatureOfComplaintSpinner(ArrayList<NatureOfComplaint> natureOfComplaints) {

        listOfNatureOfComplaint = (Spinner) findViewById(R.id.spinner_nature_of_complaint);
        listOfNatureOfComplaintAdaptor = new ArrayAdapter<NatureOfComplaint>(this, R.layout.simple_spinner_lay, natureOfComplaints);
        listOfNatureOfComplaintAdaptor.setDropDownViewResource(R.layout.simple_spinner_dropdown_lay);
        listOfNatureOfComplaint.setAdapter(listOfNatureOfComplaintAdaptor);

        listOfNatureOfComplaint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                NatureOfComplaint natureOfComplaint = (NatureOfComplaint) parent.getSelectedItem();
                singleton.complaint_nature_cd = natureOfComplaint.getId();
                //Utils.showToastMsg(getApplicationContext(), natureOfComplaint.getName(), Toast.LENGTH_LONG);

            }// end on item selected

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (singleton.complaint_nature_cd != null) {
            autoSelectNatureOfComplaint(natureOfComplaints);
        }


    }// end spinners

    /**
     * @ auto select nature of complaint
     */
    public void autoSelectNatureOfComplaint(final ArrayList<NatureOfComplaint> natureOfComplaint) {

        Utils.printv("singleton.complaint_nature_cd " + singleton.complaint_nature_cd.toString());

        int index = 0;
        for (NatureOfComplaint noc : natureOfComplaint) {
            if (singleton.complaint_nature_cd.toString().equals(noc.getId().toString())) {
                listOfNatureOfComplaint.setSelection(index);
            }
            index++;
        }//end foreach

    }// end auto Select nature of complaint


    /**
     * @ Validation
     */

    public void validate() {

        // @Logic : convert string into date value
        String format_value = "dd-MM-yyyy hh:mm a";
        Date date_final_incident_from = Utils.convertStrToDate(this, format_value
                , final_incident_from);
        Date date_final_incident_to = Utils.convertStrToDate(this, format_value
                , final_incident_to);
        Date current_date = Utils.convertStrToDate(this, format_value
                , new SimpleDateFormat(format_value).format(new Date()));


        Utils.printv("date_final_incident_from " + date_final_incident_from);
        Utils.printv("date_final_incident_to " + date_final_incident_to);
        Utils.printv("current_date " + current_date);

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (this.listOfNatureOfComplaint.getSelectedItem().toString() == ""
                || listOfNatureOfComplaint.getSelectedItem().toString() == txt_select) {
            Utils.showToastMsg(getApplicationContext(), "Please select nature of complaint", Toast.LENGTH_LONG);
        } else if (this.final_incident_from == "") {
            Utils.showToastMsg(getApplicationContext(), "Please select Date and Time of incident from."
                    , Toast.LENGTH_LONG);
        } else if (this.final_incident_to == "") {
            Utils.showToastMsg(getApplicationContext(), "Please select Date and Time of incident to."
                    , Toast.LENGTH_LONG);
        }
        // todo please check this issue
        else if (current_date.before(date_final_incident_from)) {
            Utils.showToastMsg(getApplicationContext(), "Please select past date of incident FROM."
                    , Toast.LENGTH_LONG);
        } else if (current_date.before(date_final_incident_to)) {
            Utils.showToastMsg(getApplicationContext(), "Please select past date of incident TO."
                    , Toast.LENGTH_LONG);
        } else if (date_final_incident_from.after(date_final_incident_to)) {
            Utils.showToastMsg(getApplicationContext(), "Incident date TO must be greater than FROM."
                    , Toast.LENGTH_LONG);
        }
        else if (et_complainant_name.getText().toString().trim().equals("")) {
            txt_ip_complainant_name.setError(getString(R.string.cannot_left_empty_msg));
        } else if (et_complainant_name.getText().toString().trim().length() > 255) {
            txt_ip_complainant_name.setError(getString(R.string.number_of_chars));
        } else {


            this.singleton.incident_from = final_incident_from;
            this.singleton.incident_to = final_incident_to;
            this.singleton.complainant_name = et_complainant_name.getText().toString();

              /* Create an Intent that will start the next Activity. */
            Intent mainIntent = new Intent(ComplaintSrcSecondActivity.this
                    , ComplaintAddressActivity.class);
            ComplaintSrcSecondActivity.this.startActivity(mainIntent);
        }

    }// end validate

    /**
     * @ get nature of complaint
     */

    public void GetNatureOfComplaintWebService() throws Exception {

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            // nature of complain not need any special content
            Map postParams = new HashMap();
            postParams.put("state_cd", Constants.STATECD);
            postParams.put("m_service", Constants.mNatureOfComplaintConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        this.mProgressDialog.show();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("mStateConnect", postParams);

        apiCaller.mNatureOfComplaintConnect(jsonPostParams,
                new Callback<WSPNatureOfComplaintConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server nature of complaint.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        tm_get_nature_of_complaint_web_service++;

                        try {

                            if (!Utils.isNetworkAvailable(ComplaintSrcSecondActivity.this)) {
                                Utils.showToastMsg(ComplaintSrcSecondActivity.this, "Please check internet connection.", Toast.LENGTH_LONG);
                            } else if(tm_get_nature_of_complaint_web_service<3) GetNatureOfComplaintWebService();

                        } catch (Exception ex) {
                            Utils.printv("Exception : " + ex.getMessage());
                        }


                    }// end failure

                    @Override
                    public void success(WSPNatureOfComplaintConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {
                            Utils.printv("RESULT VJ success");

                            ArrayList<NatureOfComplaint> natureOfComplaint = new ArrayList<>();
                            natureOfComplaint.add(new NatureOfComplaint("0", txt_select));

                            for (WSPNatureOfComplaintConnect.NationalOfComplaint list : result.getNationalOfComplaint()) {
                                natureOfComplaint.add(new NatureOfComplaint(list.NOC_ID.toString(), list.NOC_NAME.toString()));
                            }

                            if (!natureOfComplaint.isEmpty()) {
                                setNatureOfComplaintSpinner(natureOfComplaint);
                            }

                        } else {
                            Utils.showToastMsg(getApplicationContext(), "Something went wrong!!!", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end Register web service

}// end main class
