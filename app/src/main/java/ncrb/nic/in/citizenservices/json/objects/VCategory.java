/**
 * 
 */
package ncrb.nic.in.citizenservices.json.objects;

import java.util.List;

/**
 * Class to represent a category in app.
 * @author
 */
public class VCategory {

	protected long id;
	protected String name;
	private long parentId;
	private int itemCount;
	private String version = "0";
	private List<VCategory> childCategories;


	public long getId() {
		return id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getParentId() {
		return parentId;
	}

	public void setParentId(long parentId) {
		this.parentId = parentId;
	}

	public int getItemCount() {
		return itemCount;
	}

	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}

	public List<VCategory> getChildCategories() {
		return childCategories;
	}

	public void setChildCategories(List<VCategory> childCategories) {
		this.childCategories = childCategories;
	}

}
