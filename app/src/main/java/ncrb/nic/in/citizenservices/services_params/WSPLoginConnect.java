package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class WSPLoginConnect {

    public String STATUS_CODE;
    public String STATUS;
    public String MESSAGE;
    public String  FIRST_NAME;
    public String  MIDDLE_NAME;
    public String  LAST_NAME;
    public String  GENDER;
    public String  DOB;
    public String  MOBILE_1;
    public String  MOBILE_2;
    public String  TELEPHONE;
    public String  EMAIL;
    public String  NATIONAL_ID_TYPE;
    public String  NATIONAL_ID_NUM;
    public String  SECURITY_QUES_CD1;
    public String  SECURITY_QUES_CD2;
    public String  SECURITY_ANSWER1;
    public String  SECURITY_ANSWER2;
    public String  YOB;
    public String  AGE;
    public String  AGE_MONTHS;
    public String  AGE_FROM_YRS;
    public String  AGE_TO_YRS;
    public String  ADDRESS_LINE_1;
    public String  ADDRESS_LINE_2;
    public String  ADDRESS_LINE_3;
    public String  VILLAGE;
    public String  TEHSIL;
    public String  COUNTRY_CD;
    public String  STATE_CD;
    public String  DISTRICT_CD;
    public String  PS_CD;
    public String  PINCODE;
    public String  IS_PERM_ADDR_SAME;
    public String  seed;

    // for java app
    public String USER_APPLICANT_NUM;
    public String USER_LOGIN_ID;

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }

}// end main class
