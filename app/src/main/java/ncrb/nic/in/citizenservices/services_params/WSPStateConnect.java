package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class WSPStateConnect {

    public String STATUS_CODE;
    public String STATUS;
    public String  seed;
    public List<StateLists> states;

    public List<StateLists> getStates() {
        return states;
    }

    public void setStates(List<StateLists> states) {
        this.states = states;
    }

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }


    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    @Keep
    public class StateLists {

        public String STATE_CD;
        public String STATE;


    }// end district list


}// end main class
