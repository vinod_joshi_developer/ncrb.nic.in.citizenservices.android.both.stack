package ncrb.nic.in.citizenservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ncrb.nic.in.citizenservices.citizen_general_service.FirInputs;
import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPStateConnect;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;

public class DashboardActivity extends AppCompatActivity {

    Singleton singleton = Singleton.getInstance();

    public ProgressDialog mProgressDialog;

    MCoCoRy mCoCoRy = new  MCoCoRy();

    /**
     * @ method onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_screen);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }// end onCreate

    /*
    * @on start
    * */
    @Override
    protected void onStart() {
        super.onStart();

        // get all the state for further usages
        try {
            if (!Utils.isNetworkAvailable(DashboardActivity.this)) {
                Utils.showToastMsg(DashboardActivity.this, "Please check internet connection.", Toast.LENGTH_LONG);
            } else DashboardActivity.this.StateListWebCaller();
        } catch (Exception ex) {
            Utils.printv("Exception " + ex.getMessage());
        }

    }// on start

    /**
     * @ method CreateComplaint
     */

    public void CreateComplaint(View v) {
        Utils.printv("create complaint");

        if(Utils.isNetworkAvailable(this)) {
            //Intent mainIntent = new Intent(DashboardActivity.this, ComplaintRegOneActivity.class);
            Intent mainIntent = new Intent(DashboardActivity.this, ComplaintSrcFirstActivity.class);
            DashboardActivity.this.startActivity(mainIntent);
        } else Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);


    }// end Create Complaint

    /**
     * @ method
     */

    public void GoToHome(View v) {

        Utils.printv("clicked GoToHome");

        Intent mainIntent = new Intent(DashboardActivity.this, FrontGridViewImageTextActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        DashboardActivity.this.startActivity(mainIntent);

    }// end Go To Home


    /**
     * @ method view fir
     */

    public void GoToViewFIRDetail(View v) {

        Utils.printv("clicked GoToViewFIRDetail");

        Intent mainIntent = new Intent(DashboardActivity.this, FirInputs.class);
        DashboardActivity.this.startActivity(mainIntent);

    }// end Go view


    /**
     * @ method CreateComplaint
     */

    public void ComplaintSearchStatus(View v) {
        Utils.printv("search status");

        if(Utils.isNetworkAvailable(this)) {
            Intent mainIntent = new Intent(DashboardActivity.this, SearchStatusActivity.class);
            DashboardActivity.this.startActivity(mainIntent);
        } else Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);

    }// end Create Complaint

    /**
     * @ option menu
     * **/
    @Override
    public void onBackPressed() {

    }// end on back pressed method

    /**
     * @ state office web caller
     */
    public void StateListWebCaller() throws Exception {

        this.mProgressDialog.show();

        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);

        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = ""; String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("state", Constants.STATECD);
            postParams.put("m_service", Constants.mStateConnect);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setClient(new OkClient(okHttpClient))
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();


        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params "+postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("LOGIN", postParams);

        Utils.printv("state " + Constants.STATECD);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mStateConnect(jsonPostParams,
                new Callback<WSPStateConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server state connection.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPStateConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            Utils.printv("RESULT states list");

                            try {

                                singleton.wspStateConnect = result;

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {

                            Utils.showToastMsg(getApplicationContext()
                                    , "State list not available.", Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end state web caller

}// end main class
