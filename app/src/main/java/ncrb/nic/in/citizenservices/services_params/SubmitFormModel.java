package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep 
public class SubmitFormModel {

    @Keep
    private int statusCode;
    @Keep
    private String status;
    @Keep
    private ResponseData responseData;

    @Keep
    public String getStatus() {
        return status;
    }

    @Keep
    public void setStatus(String status) {
        this.status = status;
    }

    @Keep
    public int getStatusCode() {
        return statusCode;
    }

    @Keep
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Keep
    public ResponseData getResponseData() {
        return responseData;
    }

    @Keep
    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    @Keep
    public class ResponseData {

        @Keep
    private String message;
        @Keep
    public Data data;

        @Keep
    public String getMessage() {
            return message;
        }

        @Keep
    public void setMessage(String message) {
            this.message = message;
        }

        @Keep
    public Data getData() {
            return data;
        }

        @Keep
    public void setData(Data data) {
            this.data = data;
        }
    }

    @Keep
    public class Data {

        @Keep
    private int application_id;

        @Keep
    public int getApplication_id() {
            return application_id;
        }
        @Keep
    public void setApplication_id(int application_id) {
            this.application_id = application_id;
        }
    }
}
