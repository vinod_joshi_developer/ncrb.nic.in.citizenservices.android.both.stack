package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class SyncableDataGSON {

    private int statusCode;
    private String status;
    private ResponseData responseData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseData responseData) {
        this.responseData = responseData;
    }

    /**
     * Response class
     */

    @Keep
    public class ResponseData {

        private String message;
        public List<District> district;
        public List<State> state;
        public List<PoliceStation> police_station;

        public List<State> getState() {
            return state;
        }

        public void setState(List<State> state) {
            this.state = state;
        }

        public List<PoliceStation> getPolice_station() {
            return police_station;
        }

        public void setPolice_station(List<PoliceStation> police_station) {
            this.police_station = police_station;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<District> getDistrict() {
            return district;
        }

        public void setDistrict(List<District> district) {
            this.district = district;
        }
    }

    /**
     * Station class
     */
    @Keep
    public class District {

        private String district_cd;
        private String idsync_histroy_mapp_tbl;
        private String creation_dt;
        private String district;
        private String tbl_name;
        private String updation_status;
        private String state_cd;

        public String getDistrict_cd() {
            return district_cd;
        }

        public void setDistrict_cd(String district_cd) {
            this.district_cd = district_cd;
        }

        public String getIdsync_histroy_mapp_tbl() {
            return idsync_histroy_mapp_tbl;
        }

        public void setIdsync_histroy_mapp_tbl(String idsync_histroy_mapp_tbl) {
            this.idsync_histroy_mapp_tbl = idsync_histroy_mapp_tbl;
        }

        public String getCreation_dt() {
            return creation_dt;
        }

        public void setCreation_dt(String creation_dt) {
            this.creation_dt = creation_dt;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getTbl_name() {
            return tbl_name;
        }

        public void setTbl_name(String tbl_name) {
            this.tbl_name = tbl_name;
        }

        public String getUpdation_status() {
            return updation_status;
        }

        public void setUpdation_status(String updation_status) {
            this.updation_status = updation_status;
        }

        public String getState_cd() {
            return state_cd;
        }

        public void setState_cd(String state_cd) {
            this.state_cd = state_cd;
        }
    }// end data


    /**
     * Station class
     */
    @Keep
    public class State {

        private String district_cd;
        private String idsync_histroy_mapp_tbl;
        private String creation_dt;
        private String district;
        private String tbl_name;
        private String updation_status;
        private String state_cd;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        private String state;

        public String getDistrict_cd() {
            return district_cd;
        }

        public void setDistrict_cd(String district_cd) {
            this.district_cd = district_cd;
        }

        public String getIdsync_histroy_mapp_tbl() {
            return idsync_histroy_mapp_tbl;
        }

        public void setIdsync_histroy_mapp_tbl(String idsync_histroy_mapp_tbl) {
            this.idsync_histroy_mapp_tbl = idsync_histroy_mapp_tbl;
        }

        public String getCreation_dt() {
            return creation_dt;
        }

        public void setCreation_dt(String creation_dt) {
            this.creation_dt = creation_dt;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getTbl_name() {
            return tbl_name;
        }

        public void setTbl_name(String tbl_name) {
            this.tbl_name = tbl_name;
        }

        public String getUpdation_status() {
            return updation_status;
        }

        public void setUpdation_status(String updation_status) {
            this.updation_status = updation_status;
        }

        public String getState_cd() {
            return state_cd;
        }

        public void setState_cd(String state_cd) {
            this.state_cd = state_cd;
        }
    }// end data


    /**
     * Station class
     */
    @Keep
    public class PoliceStation {

        private String ps;
        private String ps_code;
        private String district_cd;
        private String idsync_histroy_mapp_tbl;
        private String creation_dt;
        private String district;
        private String tbl_name;
        private String updation_status;
        private String state_cd;

        public String getPs() {
            return ps;
        }

        public void setPs(String ps) {
            this.ps = ps;
        }

        public String getPs_code() {
            return ps_code;
        }

        public void setPs_code(String ps_code) {
            this.ps_code = ps_code;
        }

        public String getDistrict_cd() {
            return district_cd;
        }

        public void setDistrict_cd(String district_cd) {
            this.district_cd = district_cd;
        }

        public String getIdsync_histroy_mapp_tbl() {
            return idsync_histroy_mapp_tbl;
        }

        public void setIdsync_histroy_mapp_tbl(String idsync_histroy_mapp_tbl) {
            this.idsync_histroy_mapp_tbl = idsync_histroy_mapp_tbl;
        }

        public String getCreation_dt() {
            return creation_dt;
        }

        public void setCreation_dt(String creation_dt) {
            this.creation_dt = creation_dt;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getTbl_name() {
            return tbl_name;
        }

        public void setTbl_name(String tbl_name) {
            this.tbl_name = tbl_name;
        }

        public String getUpdation_status() {
            return updation_status;
        }

        public void setUpdation_status(String updation_status) {
            this.updation_status = updation_status;
        }

        public String getState_cd() {
            return state_cd;
        }

        public void setState_cd(String state_cd) {
            this.state_cd = state_cd;
        }
    }// end ps


}// end main class response get near by police station
