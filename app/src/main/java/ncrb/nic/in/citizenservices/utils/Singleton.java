package ncrb.nic.in.citizenservices.utils;

import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

import ncrb.nic.in.citizenservices.services_params.WSPLoginConnect;
import ncrb.nic.in.citizenservices.services_params.WSPStateConnect;
import ncrb.nic.in.citizenservices.services_params.wspDistrictConnect;

/**
 * Created by Lenovo on 13-04-2016.
 */
public class Singleton {

    private static Singleton singleton = new Singleton( );

    public String state_cd ;
    public String district_cd 	;
    public String ps_code  ;
    public String incident_from  ;
    public String incident_to  ;
    public String complainant_name  ;
    public String remarks  ;
    public String complaint_desc  ;
    public String mobile_1 ;
    public String mobile_2 ;
    public String place_of_occurance ;

    // present address params
    public String add_house_no = "";// optional
    public String add_street = ""; // optional
    public String add_colony = "";// optional
    public String add_village;
    public String add_tehsil = "" ;// optional
    public String add_pincode;
    public String add_district_cd;
    public String add_ps_cd;
    public String add_state_cd;

    public String success_message;
    public String nationality_cd;
    public String complaint_nature_cd;
    public String office_code;
    public boolean know_your_ps;
    public boolean know_your_district;
    public String username = "";
    public String password = "";
    public WSPLoginConnect wspLoginConnect;
    public wspDistrictConnect wspDistrictConnect;
    public WSPStateConnect wspStateConnect;

    public String telephone_district_cd;


    public GoogleApiClient mGoogleApiClient;
    public List<String> ps_stations_with_km = new ArrayList<String>();
    public String nearby_state ;
    public String nearby_district ;
    public String nearby_ps ;

    // important to set different authentications, enter by user only, this will be the code of user
    // this code is already stored in db at cctns portal
    // this will be run time
    public String coco_seed_cd = "SEED";

    public String sedition_token = "";

    public String sync_date_state ;

    /**
     *
     * A private Constructor prevents any other
     * class from instantiating.
     *
     */
    private Singleton(){ }

    /* Static 'instance' method */
    public static Singleton getInstance( ) {
        return singleton;
    }

    /* Other methods protected by singleton-ness */
    protected static void demoMethod( ) {
        System.out.println("demoMethod for singleton");
    }

}// end main singleton
