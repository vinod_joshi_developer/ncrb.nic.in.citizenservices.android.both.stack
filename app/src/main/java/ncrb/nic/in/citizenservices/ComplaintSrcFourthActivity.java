package ncrb.nic.in.citizenservices;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.services_params.WSPReqSedition;
import ncrb.nic.in.citizenservices.services_params.WSPSubmitComplaintConnect;
import ncrb.nic.in.citizenservices.utils.AppLocationService;
import ncrb.nic.in.citizenservices.utils.AppPreferences;
import ncrb.nic.in.citizenservices.utils.MCoCoRy;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ComplaintSrcFourthActivity extends AppCompatActivity {

    private AppCompatEditText et_complaint_desc;
    private TextInputLayout txt_ip_complaint_desc;

    private AppCompatEditText et_remark;
    private TextInputLayout txt_ip_remark;

    Singleton singleton = Singleton.getInstance();
    public ProgressDialog mProgressDialog;
    AppLocationService appLocationService;
    HashMap<String, String> latlng = new HashMap<String, String>();
    AppPreferences appPreferences;

    //citizen complaint constants
    public static final String PERSON_TYPE_CD_COMPLAINANT = "35";
    public static final String COMPLAINT_IS_SAVE_C = "C";
    public static final String REQUISITION_TYPE_C = "C";

    MCoCoRy mCoCoRy = new  MCoCoRy();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_src_fourth);

        appPreferences = new AppPreferences(this);

        // remove keyboard on load
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        appLocationService = new AppLocationService(ComplaintSrcFourthActivity.this);

        this.setToolbarAction();

        // set the editing
        formTextEditing();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate();
            }
        });

    }// end onCreate


    @Override
    protected void onStart() {
        super.onStart();
        getWindow().getAttributes().windowAnimations = R.style.WindowAnimationTransition;

    }// end onStart

    @Override
    public void onBackPressed() {

        this.singleton.remarks = et_remark.getText().toString();
        this.singleton.complaint_desc = et_complaint_desc.getText().toString();

        super.onBackPressed();

    }// end on back pressed method

    /**
     * @ tool bar action
     */
    public void setToolbarAction() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        //toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        //toolbar.setLogo(R.mipmap.ic_launcher);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }// end set toolbar action


    /**
     * @ Text editing related
     */

    public void formTextEditing() {

        et_complaint_desc = (AppCompatEditText) findViewById(R.id.et_complainant_desc);
        if (singleton.complaint_desc != "" || singleton.complaint_desc != null) {
            et_complaint_desc.setText(singleton.complaint_desc);
        }
        txt_ip_complaint_desc = (TextInputLayout) findViewById(R.id.til_complainant_desc);
        et_complaint_desc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_complaint_desc.getText().length() > 0) {
                    txt_ip_complaint_desc.setError(null);
                    txt_ip_complaint_desc.setErrorEnabled(false);
                }
            }
        });// end complainant name

        // complainant address
        et_remark = (AppCompatEditText) findViewById(R.id.et_complainant_remark);
        if (singleton.remarks != "" || singleton.remarks != null) {
            et_remark.setText(singleton.remarks);
        }
        txt_ip_remark = (TextInputLayout) findViewById(R.id.til_complainant_remark);
        et_remark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (et_remark.getText().length() > 0) {
                    txt_ip_remark.setError(null);
                    txt_ip_remark.setErrorEnabled(false);
                }
            }
        });// end complainant name

    }// end form text editing

    /**
     * @ Validation
     */

    public void validate() {

        if (!Utils.isNetworkAvailable(this)) {
            Utils.showToastMsg(this, "Please check internet connection.", Toast.LENGTH_LONG);
        } else if (et_complaint_desc.getText().toString().trim().equals("")) {
            txt_ip_complaint_desc.setError(getString(R.string.cannot_left_empty_msg));
        } else if (et_remark.getText().toString().trim().equals("")) {
            txt_ip_remark.setError(getString(R.string.cannot_left_empty_msg));
        } else if (et_complaint_desc.toString().trim().length() > 255) {
            txt_ip_complaint_desc.setError(getString(R.string.number_of_chars));
        } else if (et_remark.toString().trim().length() > 255) {
            txt_ip_remark.setError(getString(R.string.number_of_chars));
        } else {

            this.singleton.remarks = et_remark.getText().toString();
            this.singleton.complaint_desc = et_complaint_desc.getText().toString();


            try {
                if (!Utils.isNetworkAvailable(getApplicationContext())) {
                    Utils.showToastMsg(getApplicationContext(), getString(R.string.check_net_connection)
                            , Toast.LENGTH_SHORT);
                } else {
                    GetSeditionWebService();
                }// end if else
            } catch (Exception e) {
                e.printStackTrace();
            }// end try catch

        }// end else if else validation loop

    }// end validate

    /**
     * @ register complaint
     */

    public void RegisterWebService() throws Exception {

        this.mProgressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res VISA -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();


        Map postParamsSeed = new HashMap();

        if (Constants.STACK_OF_STATE == "JAVA") {
            paramsForJavaStack(postParamsSeed);
        } else {
            paramsForMicroSoftStack(postParamsSeed);
        }

        // out of the box, make sure to send coco and seed to avoid incorrect user sedition
        postParamsSeed.put("sedition", singleton.sedition_token);

        Gson gsonObj = new Gson();
        String jsonStrToPostOnServer = gsonObj.toJson(postParamsSeed);

        Map postParamsCoco = new HashMap();
        postParamsCoco.put("username", singleton.username);
        postParamsCoco.put("password", singleton.password);
        postParamsCoco.put("sedition", singleton.sedition_token);

        String jsonStrcoco = gsonObj.toJson(postParamsCoco);


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = "";
        String coco_seed_encd = "";
        String coco_only_encd = "";

        try {

            // not particular information need to get country details
            coco_seed = jsonStrToPostOnServer;

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");
            coco_only_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), jsonStrcoco, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        Utils.printv("post params " + jsonStrToPostOnServer);

        // create a new hash which you want to send on server
        Map postParams = new HashMap();
        postParams.put("seed", coco_seed_encd);
        postParams.put("coco", coco_only_encd);

        JSONPostParams jsonPostParams = new JSONPostParams("mRegisterComplaintConnect", postParams);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);
        apiCaller.mRegisterComplaintConnect(jsonPostParams,
                new Callback<WSPSubmitComplaintConnect>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPSubmitComplaintConnect result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            singleton.success_message = result.MESSAGE;
                            /* Create an Intent that will start the next Activity. */
                            Intent mainIntent = new Intent(ComplaintSrcFourthActivity.this, SuccessScreenActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            ComplaintSrcFourthActivity.this.startActivity(mainIntent);
                            ComplaintSrcFourthActivity.this.finish();

                        } else {
                            Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                        }
                    }// end success
                });
    }// end Register web service

    /**
     * @ very important logic : call the server to get token.
     * token attached with login and validated at server end
     * token logic code and decode written on server side only
     * @
     */

    public void GetSeditionWebService() throws Exception {

        this.mProgressDialog.show();


        /**
         *
         * @logic : NCRB report on secure audit, hide the detail of user
         *
         * */

        String coco_seed = "";
        String coco_seed_encd = "";

        try {

            Map postParams = new HashMap();
            postParams.put("GetSeditionWebService", "");
            postParams.put("m_service", Constants.mReqSedition);

            // posting json on server with request params
            Gson gsonObj = new Gson();
            coco_seed = gsonObj.toJson(postParams);

            coco_seed_encd = mCoCoRy.ThreadToSecureDetail(getApplicationContext(), coco_seed, "ENCODE");


        } catch (Exception e) {
            e.printStackTrace();
        }

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        // create a new hash which you want to send on server
        Map postParams = new HashMap();

        postParams.put("seed", coco_seed_encd);

        Utils.printv("post params " + postParams);

        JSONPostParams jsonPostParams = new JSONPostParams("LOGIN", postParams);

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

        apiCaller.mReqSedition(jsonPostParams,
                new Callback<WSPReqSedition>() {
                    @Override
                    public void failure(RetrofitError arg0) {

                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
                        Utils.printv("failure " + arg0.toString());

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                    }// end failure

                    @Override
                    public void success(WSPReqSedition result, Response response) {

                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        Utils.printv("RESULT status " + result.getSTATUS_CODE());

                        if (result.getSTATUS_CODE().toString().equals("200")) {

                            Utils.printv("RESULT VJ getting offices");

                            // don't store username and password in shared preference for security reason

                            try {

                                singleton.sedition_token = result.SEDITION;

                                // make to get token and logic written on server
                                RegisterWebService();

                            } catch (Exception e) {
                                e.printStackTrace();
                                Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                            }

                        } else {
                            Utils.showToastMsg(getApplicationContext(), result.MESSAGE, Toast.LENGTH_SHORT);
                        }
                    }// end success
                });

    }// end auth web service

    /**
     *
     *
     * */

    private Map paramsForJavaStack(Map postParams) {

        Date date = new Date();
        String RecordCreatedOn = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String ComplaintYear = new SimpleDateFormat("yyyy").format(date);

        String new_format_value = "dd/MM/yyyy HH:mm";//MySQL
        String existing_format_value = "dd-MM-yyyy h:mm a";// Existing
        String date_final_incident_from = Utils.convertToDateFormat(this, new_format_value, existing_format_value
                , this.singleton.incident_from);
        String date_final_incident_to = Utils.convertToDateFormat(this, new_format_value, existing_format_value
                , this.singleton.incident_to);

        // generate unique code
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        String date_now = cal.get(Calendar.DATE) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
        String currentTime = date_now + " " + cal.get(Calendar.HOUR) + ":" + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND) + ":" + cal.get(Calendar.MILLISECOND);
        String uniqueCode = "88_" + currentTime + "_" + singleton.wspLoginConnect.USER_LOGIN_ID + "_" + "citizen";

        postParams.put("addDistrictCd", singleton.add_district_cd);
        postParams.put("addPsCd", singleton.add_ps_cd);
        postParams.put("addStateCd", singleton.add_state_cd);
        postParams.put("addressline1", singleton.add_house_no);
        postParams.put("addressline2", singleton.add_street);
        postParams.put("addressline3", singleton.add_colony);
        postParams.put("age", singleton.wspLoginConnect.AGE != "" ? singleton.wspLoginConnect.AGE : "0");
        postParams.put("ageFromYrs", singleton.wspLoginConnect.AGE_FROM_YRS != "" ? singleton.wspLoginConnect.AGE_FROM_YRS : "0");
        postParams.put("ageMonths", singleton.wspLoginConnect.AGE_MONTHS != "" ? singleton.wspLoginConnect.AGE_MONTHS : "0");
        postParams.put("ageToYrs", singleton.wspLoginConnect.AGE_TO_YRS != "" ? singleton.wspLoginConnect.AGE_TO_YRS : "0");
        postParams.put("complaintNatureCd", singleton.complaint_nature_cd);
        postParams.put("complaintRemarks", singleton.remarks);
        postParams.put("complDesc", singleton.complaint_desc);
        postParams.put("complRegDt", RecordCreatedOn);
        postParams.put("complRegNum", "1");
        postParams.put("complSrno", "1");
        postParams.put("complYear", ComplaintYear);
        postParams.put("compUniqCode", uniqueCode);
        postParams.put("countrycd", singleton.nationality_cd);
        postParams.put("dob", singleton.wspLoginConnect.DOB != "" ? singleton.wspLoginConnect.DOB : RecordCreatedOn);
        postParams.put("email", "");
        postParams.put("firstName", singleton.complainant_name);
        postParams.put("incidentFromDt", date_final_incident_from);
        postParams.put("incidentPlc", singleton.place_of_occurance);
        postParams.put("incidentToDt", date_final_incident_to);
        postParams.put("incidentType", "");
        postParams.put("informationTypeCD", "1");
        postParams.put("isDistrictKnown", singleton.know_your_district ? "Y" : "N");
        postParams.put("isOfficeKnown", singleton.know_your_ps ? "N" : "Y");
        postParams.put("isPsKnown", singleton.know_your_ps ? "Y" : "N");
        postParams.put("langCd", "99");
        postParams.put("lastName", "");
        postParams.put("middleName", "");
        postParams.put("mobile1", singleton.mobile_1);
        postParams.put("mobile2", singleton.mobile_2);
        postParams.put("nationalityCd", singleton.nationality_cd);
        postParams.put("originalRecord", "1");
        postParams.put("personCode", "1");
        postParams.put("personTypeCd", PERSON_TYPE_CD_COMPLAINANT);
        postParams.put("pinCode", singleton.add_pincode);
        postParams.put("receiptModeCd", "2"); // MDM - static entry In-person //todo need to update MDM
        postParams.put("recordCreatedBy", singleton.username);
        postParams.put("recordCreatedOn", RecordCreatedOn);
        postParams.put("recordStatus", COMPLAINT_IS_SAVE_C);
        postParams.put("relativeName", "");
        postParams.put("relativeType", "1");
        postParams.put("requisitionType", REQUISITION_TYPE_C);
        postParams.put("stateCD", Constants.STATECD);
        postParams.put("strAccusedPersonInfoXml", "");
        postParams.put("strComplainantPermanentAddressXml", "");
        postParams.put("strComplainantPresentAddressXml", "");
        postParams.put("strComplaintFilesXml", "");
        postParams.put("strComplaintFilesXmlComplaintDescription", "");
        postParams.put("strPersonNationalityCitizenXml", "");
        postParams.put("submitDistrictCd", singleton.district_cd.isEmpty() ? "0" : singleton.district_cd);
        postParams.put("submitOfficeCd", singleton.know_your_ps ? "0" : singleton.office_code);
        postParams.put("submitPsCd", singleton.know_your_ps ? singleton.ps_code : "0");
        postParams.put("tehsil", singleton.add_tehsil);
        postParams.put("telephone", "");
        postParams.put("uidNum", "");
        postParams.put("uapplicantNum", "" + singleton.wspLoginConnect.USER_APPLICANT_NUM);
        postParams.put("village", singleton.add_village);
        postParams.put("yob", singleton.wspLoginConnect.YOB != "" ? singleton.wspLoginConnect.YOB : "0");
        postParams.put("userLoginID", singleton.wspLoginConnect.USER_LOGIN_ID != "" ? singleton.wspLoginConnect.USER_LOGIN_ID : "0");
        postParams.put("userApplicantNum", singleton.wspLoginConnect.USER_APPLICANT_NUM != "" ? singleton.wspLoginConnect.USER_APPLICANT_NUM : "0");
        postParams.put("m_service", Constants.mRegisterComplaintConnect);


        return postParams;

    }// end params for java

    /**
     *
     *
     * */

    private Map paramsForMicroSoftStack(Map postParamsSeed) {

        Date date = new Date();
        String RecordCreatedOn = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String ComplaintYear = new SimpleDateFormat("yyyy").format(date);

        String new_format_value = "yyyy-MM-dd HH:mm:ss";//MySQL
        String existing_format_value = "dd-MM-yyyy h:mm a";// Existing
        String date_final_incident_from = Utils.convertToDateFormat(this, new_format_value, existing_format_value
                , this.singleton.incident_from);
        String date_final_incident_to = Utils.convertToDateFormat(this, new_format_value, existing_format_value
                , this.singleton.incident_to);

        postParamsSeed.put("PersonCode", "1");
        postParamsSeed.put("PersonTypeCD", "1");
        postParamsSeed.put("UidNum", "");
        postParamsSeed.put("FirstName", singleton.complainant_name);
        postParamsSeed.put("MiddleName", "");
        postParamsSeed.put("LastName", "");
        postParamsSeed.put("RelativeType", "1");
        postParamsSeed.put("RelativeName", "");
        postParamsSeed.put("Dob", singleton.wspLoginConnect.DOB != "" ? singleton.wspLoginConnect.DOB : RecordCreatedOn);
        postParamsSeed.put("Mobile1", singleton.mobile_1);
        postParamsSeed.put("Mobile2", singleton.mobile_2);
        postParamsSeed.put("Telephone", "");
        postParamsSeed.put("Email", "");
        postParamsSeed.put("RecordStatus", "1");
        postParamsSeed.put("RecordCreatedOn", RecordCreatedOn);
        postParamsSeed.put("RecordCreatedBy", singleton.username);
        postParamsSeed.put("OriginalRecord", "1");
        postParamsSeed.put("ComplRegNum", "1");
        postParamsSeed.put("LangCd", "99");
        postParamsSeed.put("ComplSrno", "1");
        postParamsSeed.put("ComplYear", ComplaintYear);
        postParamsSeed.put("ComplRegDt", RecordCreatedOn);
        postParamsSeed.put("RequisitionType", "");
        postParamsSeed.put("ComplaintNatureCd", singleton.complaint_nature_cd);
        postParamsSeed.put("ReceiptModeCd", "2"); // MDM - static entry In-person //todo need to update MDM
        postParamsSeed.put("ComplDesc", singleton.complaint_desc);
        postParamsSeed.put("ComplaintRemarks", singleton.remarks);
        postParamsSeed.put("IncidentPlc", singleton.place_of_occurance);
        postParamsSeed.put("IncidentType", "");

        //date_final_incident_from = "2017-02-06 10:00:00";
        //date_final_incident_to = "2017-02-07 10:00:00";

        postParamsSeed.put("IncidentFromDt", date_final_incident_from);
        postParamsSeed.put("IncidentToDt", date_final_incident_to);

        postParamsSeed.put("IsPsKnown", singleton.know_your_ps ? "Y" : "N");
        postParamsSeed.put("IsDistrictKnown", singleton.know_your_district ? "Y" : "N");
        postParamsSeed.put("IsOfficeKnown", singleton.know_your_ps ? "N" : "Y");
        postParamsSeed.put("SubmitDistrictCd", singleton.know_your_district ? singleton.district_cd : "");
        postParamsSeed.put("SubmitPsCd", singleton.know_your_ps ? singleton.ps_code : "");
        postParamsSeed.put("SubmitOfficeCd", singleton.know_your_ps ? "0" : singleton.office_code);
        postParamsSeed.put("InformationTypeCD", "1");
        postParamsSeed.put("StateCD", Constants.STATECD);
        postParamsSeed.put("strComplainantPresentAddressXml", "");
        postParamsSeed.put("strComplainantPermanentAddressXml", "");
        postParamsSeed.put("strComplaintFilesXml", "");
        postParamsSeed.put("strPersonNationalityCitizenXml", "");
        postParamsSeed.put("NationalityCd", singleton.nationality_cd);
        postParamsSeed.put("YOB", singleton.wspLoginConnect.YOB != "" ? singleton.wspLoginConnect.YOB : "0");
        postParamsSeed.put("Age", singleton.wspLoginConnect.AGE != "" ? singleton.wspLoginConnect.AGE : "0");
        postParamsSeed.put("AgeMonths", singleton.wspLoginConnect.AGE_MONTHS != "" ? singleton.wspLoginConnect.AGE_MONTHS : "0");
        postParamsSeed.put("AgeFromYrs", singleton.wspLoginConnect.AGE_FROM_YRS != "" ? singleton.wspLoginConnect.AGE_FROM_YRS : "0");
        postParamsSeed.put("AgeToYrs", singleton.wspLoginConnect.AGE_TO_YRS != "" ? singleton.wspLoginConnect.AGE_TO_YRS : "0");
        postParamsSeed.put("strAccusedPersonInfoXml", "");
        postParamsSeed.put("strComplaintFilesXmlComplaintDescription", "");
        postParamsSeed.put("COMPUNIQCODE", "");

        // present address params
        postParamsSeed.put("addressline1", singleton.add_house_no);
        postParamsSeed.put("addressline2", singleton.add_street);
        postParamsSeed.put("addressline3", singleton.add_colony);
        postParamsSeed.put("village", singleton.add_village);
        postParamsSeed.put("tehsil", singleton.add_tehsil);
        postParamsSeed.put("countrycd", singleton.nationality_cd);
        postParamsSeed.put("add_statecd", Constants.STATECD);
        postParamsSeed.put("add_districtcd", singleton.add_district_cd);
        postParamsSeed.put("add_pscd", singleton.add_ps_cd);
        postParamsSeed.put("pincode", singleton.add_pincode);
        postParamsSeed.put("m_service", Constants.mRegisterComplaintConnect);

        return postParamsSeed;

    }// end params for micro soft stack


}// end main class