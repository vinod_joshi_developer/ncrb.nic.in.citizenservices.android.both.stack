package ncrb.nic.in.citizenservices.citizen_general_service;

import ncrb.nic.in.citizenservices.R;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class HelpLineNoActivity extends AppCompatActivity {

    private List<HelpLineNoFormat> helpLineNoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HelpLineNoAdapter mAdapter;
    final int REQUEST_CODE_ASK_PERMISSIONS = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_line_no);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new HelpLineNoAdapter(helpLineNoList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                HelpLineNoFormat helpLineNo = helpLineNoList.get(position);

                Toast.makeText(getApplicationContext(), "Calling "+helpLineNo.getTitle(), Toast.LENGTH_SHORT).show();

                try {

                    if (ContextCompat.checkSelfPermission(HelpLineNoActivity.this,
                            Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                                REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + helpLineNo.getGenre()));
                        startActivity(intent);
                    }

                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(HelpLineNoActivity.this,
                            "Call failed, please try again later!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareMovieData();
    }

    //public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
            switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Call Permission Granted..Please dial again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Call permission not granted", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void prepareMovieData() {

        HelpLineNoFormat helpLineNo = new HelpLineNoFormat("Control Room", "100", "");
        helpLineNoList.add(helpLineNo);

        helpLineNo = new HelpLineNoFormat("Fire Brigade", "101", "");
        helpLineNoList.add(helpLineNo);

        helpLineNo = new HelpLineNoFormat("Ambulance", "102", "");
        helpLineNoList.add(helpLineNo);

        helpLineNo = new HelpLineNoFormat("Medical Emergency", "108", "");
        helpLineNoList.add(helpLineNo);

        helpLineNo = new HelpLineNoFormat("Women Helpline", "1090", "");
        helpLineNoList.add(helpLineNo);

        helpLineNo = new HelpLineNoFormat("Railway Helpline", "139", "");
        helpLineNoList.add(helpLineNo);

        helpLineNo = new HelpLineNoFormat("Child Helpline", "1098", "");
        helpLineNoList.add(helpLineNo);

        mAdapter.notifyDataSetChanged();
    }

}

