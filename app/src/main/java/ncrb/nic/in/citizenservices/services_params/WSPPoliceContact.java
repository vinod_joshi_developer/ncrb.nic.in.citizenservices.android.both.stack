package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

@Keep
public class WSPPoliceContact {

    public String STATUS_CODE;
    public String STATUS;
    public List<ListPSContact> listPSContact;

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public List<ListPSContact> getListPSContact() {
        return listPSContact;
    }

    public void setListPSContact(List<ListPSContact> listPSContacts) {
        this.listPSContact = listPSContacts;
    }

    @Keep
    public class ListPSContact {

        public String FIRST_NAME;
        public String LAST_NAME;
        public String MOBILE_1;
        public String MOBILE_2;
        public String TELEPHONE;
        public String EMAIL;
        public String PS;
        public String DISTRICT;
        public String PS_ID;
        public String DISTRICT_CD;
        public String STATE_CD;

    }// end  list

}// end main class
