package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class WSPDistrictOfficeConnect {

    public String STATUS_CODE;
    public String STATUS;
    public List<DistrictOffices> districtOffices;

    public List<DistrictOffices> getDistrictOffices() {
        return districtOffices;
    }

    public void setDistrictOffices(List<DistrictOffices> districtOffices) {
        this.districtOffices = districtOffices;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }


    @Keep
    public class DistrictOffices {

        public String OFFICE_CD;
        public String OFFICE_NAME;

    }// end district office names


}// end main class
