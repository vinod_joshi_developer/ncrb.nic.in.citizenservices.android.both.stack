package ncrb.nic.in.citizenservices.citizen_general_service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ncrb.nic.in.citizenservices.ComplaintAddressActivity;
import ncrb.nic.in.citizenservices.ComplaintSrcFourthActivity;
import ncrb.nic.in.citizenservices.R;
import ncrb.nic.in.citizenservices.utils.AppPreferences;
import ncrb.nic.in.citizenservices.utils.Utils;

public class SOSContactActivity extends AppCompatActivity {

    Button button;
    // set share preferences
    AppPreferences objAppPreferences;
    Context objContext;
    EditText etSOSContact1;
    EditText etSOSContact2;
    EditText etSOSContact3;
    EditText etSOSContact4;
    EditText etSOSContact5;
    EditText etSOSContactMsg;

    String sos1 = "sos1";
    String sos2 = "sos2";
    String sos3 = "sos3";
    String sos4 = "sos4";
    String sos5 = "sos5";
    String sos_msg = "sos_msg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soscontact);

        // core objects
        objContext = this;
        objAppPreferences = new AppPreferences(objContext);

        Utils.printv("stored data:  " + objAppPreferences.getUserDefaults(sos1));

        etSOSContact1=(EditText)findViewById(R.id.sos_contact_1);
        etSOSContact2=(EditText)findViewById(R.id.sos_contact_2);
        etSOSContact3=(EditText)findViewById(R.id.sos_contact_3);
        etSOSContact4=(EditText)findViewById(R.id.sos_contact_4);
        etSOSContact5=(EditText)findViewById(R.id.sos_contact_5);
        etSOSContactMsg=(EditText)findViewById(R.id.sos_contact_msg);

        // declare

        // if user already having contacts
        autoFillContacts();

        button = (Button) findViewById(R.id.sos_contact_btn);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                try {

                    objAppPreferences.setUserDefaults(sos1, etSOSContact1.getText().toString());
                    objAppPreferences.setUserDefaults(sos2, etSOSContact2.getText().toString());
                    objAppPreferences.setUserDefaults(sos3, etSOSContact3.getText().toString());
                    objAppPreferences.setUserDefaults(sos4, etSOSContact4.getText().toString());
                    objAppPreferences.setUserDefaults(sos5, etSOSContact5.getText().toString());
                    objAppPreferences.setUserDefaults(sos_msg, etSOSContactMsg.getText().toString());

                    Toast.makeText(getApplicationContext(),"Information updated successfully.",Toast.LENGTH_SHORT).show();

                    SOSContactActivity.this.onBackPressed();

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"Please update contacts and message.",Toast.LENGTH_SHORT).show();
                }


            }
        });

    }// on create

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    // fetch shared preference for contacts
    public void autoFillContacts() {

        etSOSContact1.setText(objAppPreferences.getUserDefaults(sos1));
        etSOSContact2.setText(objAppPreferences.getUserDefaults(sos2));
        etSOSContact3.setText(objAppPreferences.getUserDefaults(sos3));
        etSOSContact4.setText(objAppPreferences.getUserDefaults(sos4));
        etSOSContact5.setText(objAppPreferences.getUserDefaults(sos5));
        etSOSContactMsg.setText(objAppPreferences.getUserDefaults(sos_msg));


    }// auto fill contact

}// main class
