/**
 *
 */
package ncrb.nic.in.citizenservices.services;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import ncrb.nic.in.citizenservices.constants.DBConstants;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import ncrb.nic.in.citizenservices.json.objects.VCategory;
import ncrb.nic.in.citizenservices.utils.Utils;

/**
 * Class to handle all function of database for application.
 *
 * @author Ultron
 */
public class DatabaseHandler implements DBConstants {

    protected static final String TAG = "DatabaseHandler";
    private static DatabaseHandler handler;
    private SQLiteDatabase database;
    private SQLiteHelper helper;
    private static Context context;
    private String dataBasePath = "";

    /**
     * SQLite database helper class to init and update the database.
     */
    private class SQLiteHelper extends SQLiteOpenHelper {

        /**
         * Constructor to init helper
         */
        public SQLiteHelper(Context context, String name,
                            CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // Initialize the database with all tables.
            // Now database is bundled with the build, so no db already created.
            // createAllTables(db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.e(TAG, "in onupgrade newVersion=" + newVersion + " oldversion="
                    + oldVersion);
            // Verify old version
            if (newVersion == oldVersion) {
                // Update new version
                db.setVersion(newVersion);
            }
        }
    }// end class sqlite helper

    /**
     * returns the singleton instance of class
     *
     * @param context
     * @return
     */
    public static DatabaseHandler getInstance(Context context) {
        if (handler == null) {
            handler = new DatabaseHandler(context);
            handler.openConnection();
        }
        return handler;
    }

    /**
     * Private constructor to prevent instantiation.
     *
     * @param context
     */
    private DatabaseHandler(Context context) {
        DatabaseHandler.context = context;
        this.helper = new SQLiteHelper(context, PD_DB_NAME, null,
                PD_DB_DEF_VERSION);
    }

    /**
     * Open a writable connection
     */
    private void openConnection() {

        Log.e(TAG, "before getting database path");
        dataBasePath = context.getDatabasePath(PD_DB_NAME).toString();
        System.out.println("dataBasePath "+dataBasePath);
        Log.e(TAG, "after getting database path");

        boolean isDbExist = isDatabaseAvailable();

        if (isDbExist) {
            Log.e(TAG, "database already exist");
            opendatabase();
        } else {
            Log.e(TAG, "Database doesn't exist");
            try {
                createdatabase();
            } catch (Exception e) {
                Log.e(TAG, e + "");
            }
        }
    }

    /**
     * Creating database by copying bundled database file in app sandbox
     *
     * @throws IOException
     * @author:
     */
    private void createdatabase() throws IOException {
        helper.getReadableDatabase();// readable/writable both
        try {
            Log.e(TAG, "before copy database");
            copydatabase();
            opendatabase();
        } catch (IOException e) {
            throw new Error("Error copying database");
        }
    }

    /**
     * Copying bundled database
     *
     * @throws IOException
     * @author:
     */
    public void copydatabase() throws IOException {
        try {
            // Open your local db as the input stream
            InputStream myinput = context.getAssets().open(PD_DB_NAME);

            // Open the empty db as the output stream
            OutputStream myoutput = new FileOutputStream(dataBasePath);

            // transfer byte to inputfile to outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myinput.read(buffer)) > 0) {
                myoutput.write(buffer, 0, length);
                // Log.e(TAG, "copying database...");
            }

            // Close the streams
            myoutput.flush();
            myoutput.close();
            myinput.close();
        } catch (Exception e) {
            Log.e(TAG, e + "");
        }

    }

    /**
     * Opening database
     *
     * @throws SQLException
     * @author:
     */
    public SQLiteDatabase opendatabase() throws SQLException {
        helper.getReadableDatabase();// TODO:check
        // Open the database
        database = SQLiteDatabase.openDatabase(dataBasePath, null,
                SQLiteDatabase.OPEN_READWRITE);
        return  database;
    }

    private boolean isDatabaseAvailable() {
        boolean isDBExist = false;
        try {
            File dbfile = new File(dataBasePath);
            isDBExist = dbfile.exists();

        } catch (SQLiteException e) {
            Log.e(TAG, "Database doesn't exist, exception=" + e);
        }
        return isDBExist;
    }

    /**
     * Close the existing connection
     */
    public void closeConnection() {
        try {
            helper.close();
        } catch (Throwable th) {
        }
    }


    /**
     * Method to fetch result cursor
     *
     * @param query     query
     * @param whereArgs string args to replace ? of where clause
     * @return
     */
    public Cursor executeQuery(String query, String... whereArgs) {
        System.out.println("query "+query);
        Cursor cursor = null;
        if (query != null && !query.isEmpty()) {
            Log.d(TAG, query);
            try {
                cursor = database.rawQuery(query, whereArgs);
            } catch (Throwable th) {
                Log.e(TAG, "Query execution failed: " + th.getMessage());
            }
        }
        return cursor;
    }

    /**
     * Method to run insert update delete query on database
     *
     * @param query query
     */
    public void executeUpdate(String query) {
        if (query != null && !query.isEmpty()) {
            Log.d(TAG, query);
            try {
                database.execSQL(query);
            } catch (Throwable th) {
                Log.e(TAG, "Query execution failed: " + th.getMessage());
            }
        }
    }

    /**
     * Method to get json Object from query;
     *
     * @param query     query
     * @param whereArgs string args to replace ? of where clause
     * @return
     */
    public JSONObject getJsonObject(String query, String... whereArgs) {
        JSONObject json = new JSONObject();
        JSONArray res = new JSONArray();
        Cursor cursor = executeQuery(query, whereArgs);
        try {
            if (cursor != null) {
                String[] columns = cursor.getColumnNames();
                while (columns != null && cursor.moveToNext()) {
                    JSONObject row = new JSONObject();
                    for (int i = 0; i < columns.length; i++) {
                        Object value;
                        switch (cursor.getType(i)) {
                            case Cursor.FIELD_TYPE_INTEGER:
                                value = cursor.getInt(i);
                                break;
                            case Cursor.FIELD_TYPE_FLOAT:
                                value = cursor.getDouble(i);
                            default:
                                value = cursor.getString(i);
                        }
                        // Set key value pair
                        try {
                            row.put(columns[i], value);
                        } catch (JSONException e) {
                            Log.e(TAG, "Failed to add " + columns[i] + " -> "
                                    + value);
                        }
                    }
                    // add row in array
                    res.put(row);
                }
                // add array into json
                try {
                    json.put("Rows", res);
                } catch (JSONException e) {
                    Log.e(TAG, "Failed to Rows " + res.toString());
                }
            }
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return json;
    }

    /**
     * Method return the list of categories from query.
     *
     * @param query
     * @return
     */
    public List<VCategory> getCategoriesByQuery(String query) {
        Cursor cursor = null;
        List<VCategory> vCatList = new ArrayList<>();
        try {
            cursor = executeQuery(query);
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    VCategory vcat = Utils.getCategory(cursor);
                    // Adding categories to the list
                    vCatList.add(vcat);
                } while (cursor.moveToNext());

            } else {
                Log.e(TAG, "No Result found: " + query);
            }
        } catch (Throwable e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return vCatList;
    }// end

}// end main Database handler
