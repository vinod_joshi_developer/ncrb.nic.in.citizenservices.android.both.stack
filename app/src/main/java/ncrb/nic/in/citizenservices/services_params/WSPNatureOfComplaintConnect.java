package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class WSPNatureOfComplaintConnect {

    public String STATUS_CODE;
    public String STATUS;
    public List<NationalOfComplaint> nationalOfComplaint;

    public List<NationalOfComplaint> getNationalOfComplaint() {
        return nationalOfComplaint;
    }

    public void setNationalOfComplaint(List<NationalOfComplaint> nationalOfComplaint) {
        this.nationalOfComplaint = nationalOfComplaint;
    }


    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }

    @Keep
    public class NationalOfComplaint {

        public String NOC_ID;
        public String NOC_NAME;

    }// end State office names

}// end main class
