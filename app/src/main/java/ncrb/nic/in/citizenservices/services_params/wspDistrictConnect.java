package ncrb.nic.in.citizenservices.services_params;

import android.support.annotation.Keep;

import java.util.List;

/**
 * Created by sez1 on 24/11/15.
 */
@Keep
public class wspDistrictConnect {

    
    public String STATUS_CODE;
    
    public String STATUS;
    
    public List<DistrictLists> districtLists;

    public String getSTATUS_CODE() {
        return STATUS_CODE;
    }

    public void setSTATUS_CODE(String STATUS_CODE) {
        this.STATUS_CODE = STATUS_CODE;
    }

    public List<DistrictLists> getDistrictLists() {
        return districtLists;
    }

    public void setDistrictLists(List<DistrictLists> districtLists) {
        this.districtLists = districtLists;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    @Keep
    public class DistrictLists {

        
        public String DISTRICT_CD;
        
        public String DISTRICT;
        
        public String STATE_CD;

        
        public PoliceStationsDataSet policeStationsDataSet;

        public PoliceStationsDataSet getPoliceStationsDataSet() {
            return policeStationsDataSet;
        }

        public void setPoliceStationsDataSet(PoliceStationsDataSet policeStationsDataSet) {
            this.policeStationsDataSet = policeStationsDataSet;
        }


    }// end district list

    @Keep
    public class PoliceStationsDataSet {

        
        public List<PoliceStationLists> policeStationLists;


        public List<PoliceStationLists> getPoliceStationLists() {
            return policeStationLists;
        }

        public void setPoliceStationLists(List<PoliceStationLists> policeStationLists) {
            this.policeStationLists = policeStationLists;
        }

    }// end police station data set

    @Keep
    public class PoliceStationLists {

        
        public String STATE_CD;
        
        public String DISTRICT_CD;
        
        public String PS_CD;
        
        public String PS;

    }// end PoliceStationLists

}// end main class
