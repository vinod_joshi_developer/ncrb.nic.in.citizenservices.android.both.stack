package ncrb.nic.in.citizenservices.utils;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import ncrb.nic.in.citizenservices.constants.DBConstants;
import ncrb.nic.in.citizenservices.json.objects.Nationality;
import ncrb.nic.in.citizenservices.json.objects.NatureOfComplaint;
import ncrb.nic.in.citizenservices.services.DatabaseHandler;

/**
 * Created by Lenovo on 29-04-2016.
 */
public class QueryHandler {

    protected Context context;
    protected DatabaseHandler dbHandler;
    public String select_state = "-- Select state --";
    public String select_district = "-- Select district --";
    public String select_ps = "-- Select police station --";
    public String select_text = "-- Select --";
    public String select_nationality = "-- Select nationality--";
    public String select_compl_nature = "-- Select nature of complaint--";
    public String select_type_of_service = "-- Select type of service--";
    public String select_year = "-- Select year--";
    public QueryHandler(Context context) {
        this.context = context;
        dbHandler = DatabaseHandler.getInstance(context);
    }// end query handler

    /**
     * Method to set states in adapter
     *
     * @param
     */
    public List<String> loadMStates(String textValue) {

        Cursor cursor = null;
        List<String> state_labels = new ArrayList<String>();
        state_labels.add(this.select_state);

        String query = String.format(DBConstants.Select.QUERY_STATES, textValue + "%");
        cursor = dbHandler.executeQuery(query);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                state_labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        // Set the suggestion adapter to show the auto text
        System.out.println("states " + state_labels);

        return state_labels;

    }// end method load master states

    /**
     * Method to set states in adapter
     *
     * @param
     */
    public List<String> loadMDistrict(String state_code) {

        Cursor cursor = null;
        List<String> district_labels = new ArrayList<String>();
        district_labels.add(this.select_district);

        String query = String.format(DBConstants.Select.QUERY_DISTRICT_BY_STATE_CODE, state_code);
        cursor = dbHandler.executeQuery(query);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                district_labels.add(cursor.getString(2));// read column 2, row 0,1,2,3..etc
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        // Set the suggestion adapter to show the auto text
        System.out.println("list of district " + district_labels);

        return district_labels;

    }// end method

    /**
     * @param
     */
    public String findStateCode(String state_name) {

        Cursor cursor = null;
        String query = String.format(DBConstants.Select.Q_STATE_CODE_BY_NAME, state_name);
        cursor = dbHandler.executeQuery(query);
        cursor.moveToFirst();
        String state_code = cursor.getString(0); // read = row 0 and column 0
        // closing connection
        cursor.close();

        // Set the suggestion adapter to show the auto text
        System.out.println("state code " + state_code);

        return state_code;

    }// end method

    /**
     * Method to set states in adapter
     *
     * @param
     */
    public List<String> loadPoliceStation(String district_code) {

        List<String> ps_labels = new ArrayList<String>();
        ps_labels.add(this.select_ps);

        Cursor cursor = null;
        String query = String.format(DBConstants.Select.Q_PS_BY_DISTRICT_CD, district_code);
        cursor = dbHandler.executeQuery(query);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ps_labels.add(cursor.getString(3));// read column 2, row 0,1,2,3..etc
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        // Set the suggestion adapter to show the auto text
        System.out.println("ps labels " + ps_labels);

        return ps_labels;
    }// end method load master states

    /**
     * @param
     */
    public String findDistrictCode(String district_name, String state_code) {

        Cursor cursor = null;
        String query = String.format(DBConstants.Select.Q_DISTRICT_CD_BY_NAME, district_name, state_code);
        cursor = dbHandler.executeQuery(query);

        if (cursor.getCount() == 0) {
            return "";
        }

        String district_cd = "";
        try {
            cursor.moveToFirst();
            district_cd = cursor.getString(0); // read = row 0 and column 0
        } finally {
            // closing connection
            cursor.close();
        }

        // Set the suggestion adapter to show the auto text
        System.out.println("district code " + district_cd);

        return district_cd;

    }// end method find district code

    /**
     * @param
     */
    public String findPSCode(String ps_name, String district_code, String state_code) {

        Cursor cursor = null;
        String query = String.format(DBConstants.Select.Q_PS_CODE_BY_NAME
                , ps_name, district_code, state_code);
        cursor = dbHandler.executeQuery(query);

        if (cursor.getCount() == 0) {
            return null;
        }
        String ps_code = "";
        try {
            cursor.moveToFirst();
            ps_code = cursor.getString(0); // read = row 0 and column 0
        } finally {
            // closing connection
            cursor.close();
        }

        // Set the suggestion adapter to show the auto text
        System.out.println("ps code " + ps_code);

        return ps_code;

    }// end method find ps code


    /**
     * Method to set states in adapter
     *
     * @param
     */
    public ArrayList<NatureOfComplaint> loadNatureOfComplaint() {

        Cursor cursor = null;
        ArrayList<NatureOfComplaint> natureOfComplaints = new ArrayList<>();
        natureOfComplaints.add(new NatureOfComplaint("0", this.select_text));

        String query = String.format(DBConstants.Select.QUERY_NATURE_OF_COMPLAINT);
        cursor = dbHandler.executeQuery(query);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //System.out.println(" nature of complaint "+cursor.getString(cursor.getColumnIndex("COMPLAINT_NATURE_CD")));
                natureOfComplaints.add(new NatureOfComplaint(cursor.getString(cursor.getColumnIndex("COMPLAINT_NATURE_CD"))
                        , cursor.getString(cursor.getColumnIndex("COMPLAINT_NATURE"))));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        // Set the suggestion adapter to show the auto text
        System.out.println("nature of complaints " + natureOfComplaints);

        return natureOfComplaints;

    }// end method load master states

    /**
     * Method to set states in adapter
     *
     * @param
     */
    public ArrayList<Nationality> loadNationality() {

        Cursor cursor = null;
        ArrayList<Nationality> nationality = new ArrayList<>();
        nationality.add(new Nationality("0", this.select_text));

        String query = String.format(DBConstants.Select.QUERY_NATIONALITY);
        cursor = dbHandler.executeQuery(query);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //System.out.println(" nature of complaint "+cursor.getString(cursor.getColumnIndex("COMPLAINT_NATURE_CD")));
                nationality.add(new Nationality(cursor.getString(cursor.getColumnIndex("NATIONALITY_CD"))
                        , cursor.getString(cursor.getColumnIndex("NATIONALITY"))));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        // Set the suggestion adapter to show the auto text
        System.out.println("nationality " + nationality);

        return nationality;

    }// end method load master states

}// end Query Handler
