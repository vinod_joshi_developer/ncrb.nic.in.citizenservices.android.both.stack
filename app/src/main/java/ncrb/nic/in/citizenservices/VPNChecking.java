package ncrb.nic.in.citizenservices;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import ncrb.nic.in.citizenservices.constants.Constants;
import ncrb.nic.in.citizenservices.json.objects.JSONPostParams;
import ncrb.nic.in.citizenservices.services.ApiCaller;
import ncrb.nic.in.citizenservices.utils.Utils;
import retrofit.RestAdapter;

public class VPNChecking extends Activity {

    public ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vpnchecking);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait...");

        try {
            GetAuthDetailWebService();
        } catch (Exception ex) {
            Utils.printv("Exception " + ex.getMessage());
        }

    }

    /**
     * @ get auth service
     */

    public void GetAuthDetailWebService() throws Exception {

        this.mProgressDialog.show();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.API_BASE_URL).setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String msg) {
                        Log.i("Res Complaint -", msg);
                    }
                }).setLogLevel(RestAdapter.LogLevel.FULL).build();

        JSONPostParams jsonPostParams = new JSONPostParams("LOGIN", "", "");

        ApiCaller apiCaller = restAdapter.create(ApiCaller.class);

//        apiCaller.mVPNChecking(jsonPostParams,
//                new Callback<List<WSPvpn>>() {
//                    @Override
//                    public void failure(RetrofitError arg0) {
//
//                        Utils.showToastMsg(getApplicationContext(), "Can not connect to server.", Toast.LENGTH_SHORT);
//                        Utils.printv("failure " + arg0.toString());
//
//                        if (mProgressDialog != null && mProgressDialog.isShowing())
//                            mProgressDialog.dismiss();
//
//                    }// end failure
//
//                    @Override
//                    public void success(List<WSPvpn> result, Response response) {
//
//                        if (mProgressDialog != null && mProgressDialog.isShowing())
//                            mProgressDialog.dismiss();
//
//                        for (int i=0; i<result.size(); i++) {
//                            System.out.println(result.get(i));
//                        }
//
//
//                        Utils.showAlert(VPNChecking.this, "VPN connected total results "+result.size());
//
//
//                    }// end success
//                });


    }// end auth web service


}// end main class
