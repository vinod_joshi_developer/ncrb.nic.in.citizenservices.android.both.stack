package ncrb.nic.in.citizenservices;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import ncrb.nic.in.citizenservices.citizen_general_service.CitizenTipActivity;
import ncrb.nic.in.citizenservices.citizen_general_service.HelpLineNoActivity;
import ncrb.nic.in.citizenservices.citizen_general_service.LoadMapFindPSActivity;
import ncrb.nic.in.citizenservices.citizen_general_service.SOSHelpActivity;
import ncrb.nic.in.citizenservices.citizen_general_service.TelePhoneDirActivity;
import ncrb.nic.in.citizenservices.constants.Constants;

public class FrontGridViewImageTextActivity extends AppCompatActivity {

    GridView androidGridView;

    // todo for later - "Tip for Police" , "Nearby Police Station",
    String[] gridViewString = {
             "Emergency Help", "Find Police Station", "Complaint Registration"
            , "FIR Detail", "Helpline No.", "Police Telephone Dir"
            ,"Theft vehicle(India)" ,  "Talash/Missing Persons"
            , "Citizen Tip"

    } ;
    // todo for later - R.drawable.citizen_tip, R.drawable.about_us
    int[] gridViewImageId = {
             R.drawable.sos,R.drawable.google_map, R.drawable.citizen_portal
            ,R.drawable.fir_police, R.drawable.alert,R.drawable.phone_dir
            ,R.drawable.car_confiscated,R.drawable.missing_person
            , R.drawable.snoopy
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_grid_view_image);

        CustomFrontGridViewActivity adapterViewAndroid = new CustomFrontGridViewActivity(FrontGridViewImageTextActivity.this, gridViewString, gridViewImageId);
        androidGridView=(GridView)findViewById(R.id.grid_view_image_text);
        androidGridView.setAdapter(adapterViewAndroid);
        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
                //Toast.makeText(FrontGridViewImageTextActivity.this, "Service Name: " + gridViewString[+i], Toast.LENGTH_LONG).show();

                openRelatedActivity(gridViewString[+i]);
            }
        });

    }// end on create

    public void openRelatedActivity(String serviceName) {

        if(serviceName.equals("Emergency Help")) {

//          Utils.showAlert(FrontGridViewImageTextActivity.this, "Please wait.. i'm coming...");
            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, SOSHelpActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);
        }

        if(serviceName.equals("Complaint Registration")) {

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, ComplaintLoginActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);
        }

        if(serviceName.equals("Helpline No.")) {

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, HelpLineNoActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);
        }


        if(serviceName.equals("Theft vehicle(India)")) {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.VAHAN_SAMANVAY));
            startActivity(browserIntent);
        }

        if(serviceName.equals("Talash/Missing Persons")) {

            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TALASH));
            startActivity(browserIntent);
        }

        if(serviceName.equals("Telephone Dir")) {

            //Utils.showAlert(FrontGridViewImageTextActivity.this, "Please wait.. i'm coming...");

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, TelePhoneDirActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);

        }

        if(serviceName.equals("FIR Detail")) {

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, ComplaintLoginActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);

        }

        if(serviceName.equals("Find Police Station")) {
            //Utils.showAlert(FrontGridViewImageTextActivity.this, "Please wait.. i'm coming...");

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, LoadMapFindPSActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);

        }

        if(serviceName.equals("Citizen Tip")) {

            //Utils.showAlert(FrontGridViewImageTextActivity.this, "Please wait.. i'm coming...");

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, CitizenTipActivity.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);

        }

        if(serviceName.equals("VPN")) {

            //Utils.showAlert(FrontGridViewImageTextActivity.this, "Please wait.. i'm coming...");

            Intent mainIntent = new Intent(FrontGridViewImageTextActivity.this, VPNChecking.class);
            FrontGridViewImageTextActivity.this.startActivity(mainIntent);

        }


    }// end openRelated Activity

    /**
     * @ option menu
     * **/
    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit app?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }// end on back pressed method

}// end main class