package ncrb.nic.in.citizenservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import ncrb.nic.in.citizenservices.utils.Singleton;
import ncrb.nic.in.citizenservices.utils.Utils;

public class SuccessScreenActivity extends AppCompatActivity {

    TextView success_message;
    Singleton singleton = Singleton.getInstance();

    /**
     * @ option menu
     * **/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_screen);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        success_message = (TextView) findViewById(R.id.success_message);
        success_message.setText(this.singleton.success_message);

    }// end on create

    /**
     * @ option menu
     * **/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_success_screen_activity, menu);
        return true;

    }// end on create options menu

    /**
     * @ option menu
     * **/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_close:
                this.onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }// end on Options ItemSelected

    /**
     * @ option menu
     * **/
    @Override
    public void onBackPressed() {

        Utils.printv("clicked GoToHome");

        Intent mainIntent = new Intent(SuccessScreenActivity.this, FrontGridViewImageTextActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        SuccessScreenActivity.this.startActivity(mainIntent);

    }// end on back pressed method

}// end main class